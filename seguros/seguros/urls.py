"""seguros URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url, include
from django.contrib import admin
from compania import urls as companias_urls
from trabajo import urls as trabajos_urls
# from i18n import urls as i18n_urls
from productor import urls as productores_urls

admin.autodiscover()
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^companias/', include(companias_urls)),
    url(r'^trabajos/', include(trabajos_urls)),
    url(r'^productores/', include(productores_urls)),
    # url(r'^i18n/', include(i18n_urls)),
    url(r'^select2/', include('django_select2.urls')),
]
