"""
Django settings for seguros project.

Generated by 'django-admin startproject' using Django 1.11.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

import os
import sys

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, os.path.join(BASE_DIR, "../"))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'k7m6!@1fut(*9^4tf75wa%8^i_66l2!h2*alf51-%nkwlt)&-q'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = [
    'suit',
    'polymorphic',
    'django.contrib.contenttypes',
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Additional apps
    'django.core.management',
    # Third-party apps
    'djcelery',
    'django_extensions',
    # 'i18n',
    'nested_inline',
    'django_select2',
    # Local apps
    'compania',
    'productor',
    'trabajo',
    'django.contrib.admin',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'seguros.urls'

TEMPLATES_ROOT = os.path.join(BASE_DIR, 'templates')
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [TEMPLATES_ROOT,
                 os.path.join(TEMPLATES_ROOT, 'productor'),
                 os.path.join(TEMPLATES_ROOT, 'compania'),
                 os.path.join(TEMPLATES_ROOT, 'trabajo'),
                 ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'seguros.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'es-AR'

TIME_ZONE = 'America/Argentina/Cordoba'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'statics')
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'media'),
)

# CELERY SETTINGS
BROKER_URL = 'redis://localhost:6379/0'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERYD_HIJACK_ROOT_LOGGER = False


sys.path.insert(0, "/home/fernando/Documents/projects/project_lis/recolistor/")


DATE_INPUT_FORMATS = (
    '%d/%m/%Y', '%d/%m/%y',  # '25/10/2006', '25/10/06'
)

DATE_FORMAT = 'j F Y'
TIME_FORMAT = 'H:i'
DATETIME_FORMAT = 'j F Y H:i'
YEAR_MONTH_FORMAT = 'F Y'
MONTH_DAY_FORMAT = 'j F'
SHORT_DATE_FORMAT = 'j N Y'
SHORT_DATETIME_FORMAT = 'j N Y H:i'
FIRST_DAY_OF_WEEK = 1

# Loggin
LOGIN_URL = '/admin/login'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'

LOGGING = {'version': 1,
           'disable_existing_loggers': False,
           'handlers': {'console': {'class': 'logging.StreamHandler', },
                        'file': {'class': 'logging.FileHandler',
                                 'filename': '/tmp/debug.log',
                                 'formatter': 'verbose'
                                 },
                        },
           'formatters': {'verbose': {'format': '%(asctime)s %(levelname)s '
                                      'module=%(module)s, '
                                      'process_id=%(process)d, %(message)s'
                                      }
                          },
           'loggers': {'celery': {'handlers': ['file'],
                                  'level': 'INFO',
                                  'propagate': True
                                  },
                       }
           }

SUIT_CONFIG = {
    # header
    'ADMIN_NAME': 'Sistemas Lis',
    'HEADER_DATE_FORMAT': 'l, j. F Y',
    'HEADER_TIME_FORMAT': 'H:i',

    # forms
    'SHOW_REQUIRED_ASTERISK': True,  # Default True
    'CONFIRM_UNSAVED_CHANGES': True,  # Default True

    # menu
    'SEARCH_URL': '/admin/productores/productor/',
    'MENU_OPEN_FIRST_CHILD': True,  # Default True
    'MENU': (
        'auth',
        '-',
        {'app': 'compania', 'icon': 'icon-folder-close',
         'models': ('compania',)},
        {'app': 'productor',
         'icon': 'icon-briefcase',
         'models': ('productor', 'clave', 'provincia', 'ciudad')},
        {'app': 'trabajo',
         'icon': 'icon-th-list',
         'models': ('trabajo', 'cobro', 'tarea', 'listadeprecios')},
        {'label': 'Dashboard',
         'icon': 'icon-calendar',
         'url': '/trabajos/dashboard/'},
        {'label': 'Impresiones',
         'icon': 'icon-book',
         'url': '/productores/exportar/'},
        {'label': 'Sistema Nuevo',
         'icon': 'icon-chevron-right',
         'url': '/productores/'},
    ),

    # misc
    'LIST_PER_PAGE': 50
}

# Mail de Notificaciones Vlic
MAIL_NOTIFICACIONES_LIS_CONF = {
    'host': 'smtp.gmail.com',
    'port': 587,
    'username': 'notificaciones@vlic.tv',
    'password': 'bwkkarCal4O0Gbsi9K5P',
    'user_tls': True
}

SENDGRID_API_KEY = "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"
