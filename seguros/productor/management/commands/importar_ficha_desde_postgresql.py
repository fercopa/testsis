# -*- coding: utf-8 -*-
from django.contrib.auth.models import UserManager, User

from django.core.management.base import BaseCommand, CommandError
from productores.models import *
from productores.management.commands.importar_productores_codigos_li import parseDate
from companias.management.commands.importar_companias import parseSN
from trabajos.models import *

from django.core.exceptions import ValidationError
import csv
import datetime
import sys
import psycopg2

class Command(BaseCommand):

    def consultar_PG(self, cadena_sql):
        servidor = 'localhost'
        basedatos = 'sistemaslis_database'
        # usuario = 'postgres'
        # clave = 'recolistor'
        usuario = 'brokerdigital'
        clave = 'mcfly2030'
        data = []

        cad_sql = ''
        cad_sql = cadena_sql

        string_conn = "host='{0}' dbname='{1}' user='{2}' password='{3}' ".format(
            servidor, basedatos, usuario, clave
            )

        try:
            conn = psycopg2.connect(string_conn)
            cur = conn.cursor()
            estado = True
        except psycopg2.Error as e:
            estado = False
            print(e)

        if estado:
            cur.execute(cad_sql)
            data = cur.fetchall()
        return data

    def buscar_ficha(self, contacto_id=''):
        buscar_contacto = ' where c.id = {0}'.format(contacto_id) if contacto_id else ''
        string = """
        Select c.id, translate(upper(trim(c.nombre)),'áéíóúÁÉÍÓÚäëïöüÄËÏÖÜ','aeiouAEIOUaeiouAEIOU')  as nombre,
             translate(upper(trim(c.apellido)),'áéíóúÁÉÍÓÚäëïöüÄËÏÖÜ','aeiouAEIOUaeiouAEIOU') as apellido,
             c.email, 
             c.observaciones, c.domicilio_id, c.telefonos,
             cp.nombre as provincia, cl.nombre as localidad, cl.provincia_id as provincia_id,
             cd.piso, cd.departamento,
             cd.calle_id, cc.nombre as nombre_calle, cd.nro,
             cd.barrio_id, cb.nombre as nombre_barrio,
             cd.otros_datos,
             cr.nombre as responsable,
            p.* from entidades_productor p
            left join contactos_contacto c ON c.id = p.contacto_id
            left join contactos_domicilio cd ON c.domicilio_id = cd.id
            left join contactos_provincia cp ON cp.id = cd.provincia_id
            left join contactos_localidad cl ON cl.id = cd.localidad_id
            left join contactos_calle cc ON cc.id = cd.calle_id
            left join contactos_barrio cb ON cb.id = cd.barrio_id
            left join contactos_responsable cr ON cr.id = c.responsable_id
            {0} order by c.apellido
            """.format(buscar_contacto)
            # where c.actividad_id = 15 Se saca esta opcion porque la ficha tiene no esa del otod actualizada

        return self.consultar_PG(string)

    def buscar_licencias_productor(self, productor_id):
        tipo_de_producto = 1
        string = """select distinct vm.producto_id, produc.nombre, vm.cantidad  
                    from ventas_movimiento vm
                    left join productos_producto produc ON produc.id = vm.producto_id
                    where para_id={0} and produc.tipo_id = {1} order by vm.producto_id""".format(
                    productor_id, tipo_de_producto
                    )
        return self.consultar_PG(string)

    
    def buscar_telefonos_productor(self, contacto_id):
        string =  """select ctc.tipo, ctc.nro 
                    from contactos_telefono_de_contacto ctc
                    where contacto_id ={0}""".format(contacto_id)
        return self.consultar_PG(string)

    def obtener_o_crear_responsable(self, new_responsable):
        responsable = User.objects.filter(username__iexact=new_responsable)
        if not responsable:
            try:
                response = User.objects.create_user(username=new_responsable, password=new_responsable)
            except:
                response = ''
        else:
            response = responsable[0]
        return response

    def obtener_o_crear_provincia(self, nombre):
        if not Provincia.objects.filter(nombre__iexact=nombre):
            response = ''
            response = Provincia.objects.create(
                nombre=nombre,
                )
            response.save()
            # print('No Existe se crea...{0}'.format(nombre))
        else:
            try:
                response = Provincia.objects.get(nombre__iexact=nombre)
                # print('Ya existe , se actualiza...{0}'.format(nombre))
            except:
                print('Ocurrio un error con esta provincia', nombre, sys.exc_info()[0])
                response = ''
        return response

    def obtener_o_crear_ciudad(self, ciudad, provincia):
        if not Ciudad.objects.filter(nombre__iexact=ciudad):
            response = ''
            response = Ciudad.objects.create(
                nombre=ciudad,
                provincia=provincia
                )
            response.save()
            # print('No Existe se crea...{0}'.format(ciudad))
        else:
            try:
                response = Ciudad.objects.get(nombre__iexact=ciudad)
                # print('Ya existe , se actualiza...{0}'.format(ciudad))
            except:
                print('Ocurrio un error con esta Ciudad', ciudad, sys.exc_info()[0])
                response = ''
        return response

    def obtener_o_crear_productor(self, nombre_prod, new_matricula):

        def crear_productor():
            objCreado = ''
            objCreado = Productor.objects.create(
                nombre=nombre_prod,
                matricula=new_matricula,
                )
            print('NO EXISTE, SE CREA', nombre_prod, new_matricula)
            return objCreado

        response = ''
        if new_matricula and nombre_prod:
            if not Productor.objects.filter(Q(matricula=new_matricula) | Q(nombre__iexact=nombre_prod)):
                response = crear_productor()
            else:
                response = Productor.objects.filter(Q(matricula=new_matricula) | Q(nombre__iexact=nombre_prod))[0]
                print('YA EXISTE, SE ACTUALIZA', nombre_prod, new_matricula)

        elif not new_matricula and nombre_prod:
            if not Productor.objects.filter(nombre__iexact=nombre_prod):
                response = crear_productor()
            else:
                response = Productor.objects.filter(nombre__iexact=nombre_prod)[0]
                print('YA EXISTE, SE ACTUALIZA', nombre_prod, new_matricula)

        elif new_matricula and not nombre_prod:
            if not Productor.objects.filter(matricula=new_matricula):
                response = crear_productor()
            else:
                response = Productor.objects.filter(matricula=new_matricula)[0]
                print('YA EXISTE, SE ACTUALIZA', nombre_prod, new_matricula)


        # if not Productor.objects.filter(Q(matricula=new_matricula) | Q(nombre__iexact=nombre_prod)).filter(
        #     matricula__isnull=False):
        #     response = ''
        #     response = Productor.objects.create(
        #         nombre=nombre_prod,
        #         matricula=new_matricula,
        #         )
        #     response.save()
        #     # print('No Existe se crea...{0}'.format(nombre_prod))
        # else:
        #     try:
        #         response = Productor.objects.get(Q(matricula=new_matricula) | Q(nombre__iexact=nombre_prod))
        #         # print('Ya existe , se actualiza...{0}'.format(nombre_prod))
        #     except:
        #         print('Ocurrio un error con este productor', nombre_prod, new_matricula, sys.exc_info()[0])
        #         response = ''
        return response

    def buscar_impresos_productor(self, productor_id, opera_cobra):

        if opera_cobra.upper() == 'O':
            tipo_impreso = 'li.operaciones_id'
        else:
            tipo_impreso = 'li.cobranzas_id'
        
        string = """select li.fecha, lr.fecha as fecha_renglon, lr.libro, lr.hoja, lr.orden
                    from libros_impresion li
                    left join libros_renglon lr on lr.id = {1}
                    where productor_id = {0} order by li.fecha desc limit 2""".format(productor_id, tipo_impreso)

        return self.consultar_PG(string)

    def generar_impresos_operaciones_cobranzas(self, productor_instancia, impresos_para_productor, tipo_impreso):
        if impresos_para_productor:
            # print('Generando impresos para:', productor_instancia, tipo_impreso)
            pass
            
        for trabajos in impresos_para_productor:
            fecha_solicitud = trabajos[0]  # datetime.datetime.strptime("2000-01-01", '%Y-%m-%d').date()
            if trabajos[1]:
                fecha_str = trabajos[1].strftime('%Y-%m-%d')
                fecha_renglon = datetime.datetime.strptime(fecha_str, '%Y-%m-%d')
            else:
                fecha_renglon = None

            libro = trabajos[2] if trabajos[2] else 0 
            hoja = trabajos[3] if trabajos[3] else 0 
            orden = trabajos[4] if trabajos[4] else 0

            #new_fecha_solicitud_format = datetime.datetime.strptime("2000-01-01", '%Y-%m-%d').date()
            nuevo_trabajo = TrabajoImpreso.objects.create(
                productor=productor_instancia,
                fecha=fecha_solicitud
                )
        
            generar_impreso = GeneracionImpresos.objects.create(
                trabajo=nuevo_trabajo,
                fecha=fecha_solicitud,
                tipo=tipo_impreso,
                ultimo_libro=libro,
                ultima_hoja=hoja,
                ultimo_nro_orden=orden,
                hasta=fecha_renglon,
                #terminado=True
                )
            generar_impreso.save()

    def generar_impresos_para_productor(self, productor_instancia, productorid):       
        impresos_operaciones_para_productor = self.buscar_impresos_productor(productorid, 'O')
        impresos_cobranzas_para_productor = self.buscar_impresos_productor(productorid, 'C')

        if impresos_operaciones_para_productor:
            self.generar_impresos_operaciones_cobranzas(productor_instancia, impresos_operaciones_para_productor, 'O')
        
        if impresos_cobranzas_para_productor:
            self.generar_impresos_operaciones_cobranzas(productor_instancia, impresos_cobranzas_para_productor, 'C')

    def add_arguments(self, parser):
        parser.add_argument('contacto_id', nargs='+', type=str)

    def handle(self, *args, **options):
        contacto = options.get('contacto_id')
        if contacto:
            contacto = contacto[0]
        else:
            contacto = ''
        registros = self.buscar_ficha(contacto)

        for productor in registros:
            contacto_id = productor[0]
            productor_id = productor[19]
            nombre = productor[1].strip() if productor[1] else ''
            apellido = productor[2].strip() if productor[2] else ''
            nombres = '{0}{1} {2}'.format(
                apellido.upper() if apellido else '',
                ',' if nombre else '',
                nombre.upper() if nombre else ''
                )

            email = productor[3]
            observaciones = productor[4]
            provincia = productor[7]
            localidad = productor[8]

            # TODO: agregar un campo para guardar la direccion
            piso = '{0}'.format('Piso '+ productor[10] if productor[10] else '')
            departamento = '{0}'.format(' Dpto '+ productor[11] if productor[11] else '')
            nombre_calle = '{0}'.format(' Calle ' + productor[13] if productor[13] else '')
            nro = '{0}'.format(' Nro ' + str(productor[14]) if productor[14] else '')
            nombre_barrio = '{0}'.format(' Barrio ' + productor[16] if productor[16] else '')
            otros_datos = '{0}'.format(' Mas Inf. ' + productor[17] if productor[17] else '')
            direccion = nombre_barrio + nombre_calle + nro + departamento + piso + otros_datos
            # print(direccion)

            encargado = productor[18]
            licencia_sistema_id = productor[19]
            matricula = productor[21]
            fecha_de_alta = productor[22]  # No se si se usa
            articulo_19 = True if productor[25] else False
            ultimo_libro_operaciones = productor[29] if productor[29] else 0
            ultimo_libro_cobranzas = productor[30] if productor[30] else 0

            libros_digitales = False
            preferencial = False
            cuit = ''
            clave_fiscal = ''
            link_al_ssn = "http://www.ssn.gov.ar/storage/registros/productores/productoresactivos.asp?matricula={0}&socpro=".format(matricula if matricula else '')
            # companias_actualizadas_al_dia = models.DateField('Listado de Cias Actualizadas al Dia', null=True)
            
            if nombres:
                # print(nombres, productor_id)
                obj_productor = self.obtener_o_crear_productor(nombres.strip(), matricula)
                if not obj_productor:
                    # print(nombres, productor_id)
                    continue

                obj_productor.email = email
                obj_productor.observaciones = observaciones
                telefonos = ','.join([' '.join(f) for f in self.buscar_telefonos_productor(productor[0])])

                if direccion:
                    obj_productor.direccion = direccion

                if telefonos:
                    obj_productor.telefono = telefonos

                if provincia:
                    self.obj_provincia = self.obtener_o_crear_provincia(provincia)
                    obj_productor.provincia = self.obj_provincia

                if localidad and provincia:
                    self.obj_localidad = self.obtener_o_crear_ciudad(localidad, self.obj_provincia)
                    obj_productor.localidad = self.obj_localidad

                if encargado:
                    self.obj_encargado = self.obtener_o_crear_responsable(encargado)
                    obj_productor.encargado = self.obj_encargado

                licencias = self.buscar_licencias_productor(contacto_id)
                
                tiene_licencia = False
                cantidad_de_lic = 0
                cantidad_de_maq = 0

                if licencias:
                    # print('POSEE LICENCIA')
                    tiene_licencia = True
                    cantidad_de_lic = 1
                    
                    for f in licencias:
                        if "Máquina extra" in f:
                            cantidad_de_maq = f[2]
 
                obj_productor.posee_licencia = tiene_licencia
                obj_productor.licencia = cantidad_de_lic
                obj_productor.licencias_extra = cantidad_de_maq

                obj_productor.matricula = matricula
                obj_productor.articulo_19 = articulo_19
                obj_productor.ultimo_libro_operaciones = ultimo_libro_operaciones
                obj_productor.ultimo_libro_cobranzas = ultimo_libro_cobranzas
                obj_productor.link_al_ssn = link_al_ssn

                obj_productor.save()

                self.generar_impresos_para_productor(obj_productor, productor_id)

# Exportar la base de la Ficha
# pg_dump -h localhost -U brokerdigital -W sistemaslis_database > respaldo_14_feb_2017.sql

# Traermela a mi equipo:
# scp foxcarlos@sistemaslis.com.ar:/home/foxcarlos/respaldo_14_feb_2017.sql /home/foxcarlos/Descargas/

# Borrar la base de datos desde PGADMIN:

# crearla:
# createdb -h localhost -U postgres -W sistemaslis_database

# Restaurarla:
# psql -h localhost -U postgres -W sistemaslis_database < respaldo_14_feb_2017.sql

# jecutar el Escript importar_ficha_desde_postgewsql
# python manage.py importar_ficha_desde_postgresql ''

# Pasos para Migrar:
# 1 Script a Ejecutar
# Antes pasar todo a mayusculas desde Excel
# python manage.py importar_productores_codigos_li /home/foxcarlos/Descargas/modelo_nueva_planilla_Codigos_LI.csv

# 2 script a ejecutar:
# python manage.py importar_productores_historicos_LI /home/foxcarlos/Descargas/modelo_nueva_planilla_Historico_L.csv


