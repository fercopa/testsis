# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from productores.models import *
from trabajos.models import *

class Command(BaseCommand):

    def handle(self, *args, **options):
		from productores.management.commands.importar_ficha_desde_postgresql  import Command
		c = Command()
		lista_de_productores_obtenidos_desde_la_ficha = c.buscar_ficha()

		for productor in lista_de_productores_obtenidos_desde_la_ficha:
			nombre = productor[1]
			apellido = productor[2].strip().upper()
			matricula = productor[21]
			if not nombre:
				if apellido:
					nombres = '{0}'.format(apellido.strip())
			else:
				if not apellido:
					nombres = '{0}'.format(nombre.strip())
				else:
					nombres = '{0}, {1}'.format(apellido.strip(), nombre.strip())
			# print(nombres)
			if not Productor.objects.filter(nombre=nombres):
				print('NO EXISTE EL PRODUCTOR', nombres, matricula)

    def buscar_impresiones_desde_la_ficha(self):
        # Ver impresiones por Productor de PostgreSQL
	sqlstring = """select c.id,
                select c.id,
                upper(trim(c.nombre)) as nombre, upper(trim(c.apellido)) as apellido,
                li.productor_id, li.operaciones_id, li.fecha as fecha_ope, lr.fecha as fecha_renglon_ope,
                lr.libro as libro_ope, lr.hoja as hoja_ope, lr.orden as orden_ope,
                li.cobranzas_id, lrc.fecha as fecha_renglon_cob,
                lrc.libro as libro_cob, lrc.hoja as hoja_cob, lrc.orden as orden_cob
                from libros_impresion li
                left join libros_renglon lr on lr.id = li.operaciones_id
                left join libros_renglon lrc on lrc.id = li.cobranzas_id
                left join entidades_productor p on p.id = li.productor_id
                left join contactos_contacto c ON c.id = p.contacto_id
                order by apellido, li.fecha desc """

# Ver impresiones por Productor de Sqlite desde la GUI de SqliteBrowser
# select productores_productor.nombre, trabajos_trabajo.productor_id,
# trabajos_trabajo.id as trabajo_id, trabajos_trabajo.fecha,
# trabajos_generacionimpresos.trabajo_id,
# trabajos_generacionimpresos.tipo,
# trabajos_generacionimpresos.hasta,
# trabajos_generacionimpresos.ultima_hoja,
# trabajos_generacionimpresos.ultimo_libro,
# trabajos_generacionimpresos.ultimo_nro_orden from trabajos_trabajo  
# left join trabajos_generacionimpresos 
# on  trabajos_generacionimpresos.trabajo_id = trabajos_trabajo.id
# left join productores_productor 
# on productores_productor.id = trabajos_trabajo.productor_id 
# where trabajos_trabajo.productor_id = 4
