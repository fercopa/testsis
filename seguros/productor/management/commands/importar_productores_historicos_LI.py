# -*- coding: utf-8 -*-
from django.contrib.auth.models import UserManager, User
from django.db.models import Q

from django.core.management.base import BaseCommand, CommandError
from productores.models import *
from productores.management.commands.importar_productores_codigos_li import parseDate
from companias.management.commands.importar_companias import parseSN
from trabajos.models import *

from django.core.exceptions import ValidationError
import csv
import datetime
from datetime import datetime

import sys


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('csv_filename', nargs='+', type=str)

    def obtener_o_crear_responsable(self, new_responsable):

        responsable = User.objects.filter(username__iexact=new_responsable)
        if not responsable:
            try:
                response = User.objects.create_user(username=new_responsable, password=new_responsable)
            except:
                response = ''
        else:
            response = responsable[0]
        return response

    def obtener_o_crear_productor(self, nombre_prod, new_matricula):

        if not Productor.objects.filter(Q(matricula=new_matricula) | Q(nombre__iexact=nombre_prod)):
            response = Productor.objects.create(
                nombre=nombre_prod,
                matricula=new_matricula,
                observaciones='Se creo desde el Modelo Codigos_LI'
                )
            response.save()
            print('No Existe se crea...{0}'.format(nombre_prod))
        else:
            try:
                response = Productor.objects.get(Q(matricula=new_matricula) | Q(nombre__iexact=nombre_prod))
                print('Ya existe , se actualiza...{0}'.format(nombre_prod))
            except:
                print('Ocurrio un error con este productor', nombre_prod, new_matricula, sys.exc_info()[0])
                response = ''
        return response

    def generar_impresos(self, productor_db, row):
        
        from productores.management.commands.importar_ficha_desde_postgresql import Command
        comando = Command()

        new_fecha_solicitud = parseDate(row[2])
        if not new_fecha_solicitud:
            new_fecha_solicitud = datetime.strptime(parseDate(row[24]), "%Y-%m-%d").date() 

        # Datos de Operaciones
        new_gi_fecha_hasta_op = datetime.strptime(parseDate(row[24]), "%Y-%m-%d").date()
        new_gi_ultimo_n_libro_op = row[25]
        new_gi_ultimo_n_hoja_op = row[26]
        new_gi_ultimo_n_orden = row[27]

        datos_impresos_operaciones = [(new_fecha_solicitud, new_gi_fecha_hasta_op, new_gi_ultimo_n_libro_op,
                                new_gi_ultimo_n_hoja_op, new_gi_ultimo_n_orden
                                )]
        
        comando.generar_impresos_operaciones_cobranzas(
            productor_db,
            datos_impresos_operaciones,
            'O',
            )
        
        # Datos de Cobranzas
        try:
            new_gi_fecha_hasta_cobranzas = datetime.strptime(parseDate(row[28]), "%Y-%m-%d").date()
        except:
            new_gi_fecha_hasta_cobranzas = None

        new_gi_ultimo_n_libro_cyr = row[29]
        new_gi_ultimo_n_hoja_cyr = row[30]

        datos_impresos_cobranzas = [(new_fecha_solicitud, new_gi_fecha_hasta_cobranzas, new_gi_ultimo_n_libro_cyr,
                                new_gi_ultimo_n_hoja_cyr, '0')]

        comando.generar_impresos_operaciones_cobranzas(
            productor_db,
            datos_impresos_cobranzas,
            'C',
            )

    #handle ejecuta el codigo correspondiente al argumento
    def handle(self, *args, **options):
        for csv_filename in options['csv_filename']:
            with open(csv_filename, 'rb') as csvfile:
                codigos_impresos_reader = csv.reader(csvfile, delimiter=',')  # delimiter='\t', quotechar='|')
                first_row = True
                for row in codigos_impresos_reader:
                    #No nos interesa el primer renglón, tiene nombres de columnas
                    if first_row:
                        first_row = False
                        continue

                    ### Nombramos todos los elementos del csv sabiendo el orden de antemano
                    nombre_prod = row[0].strip()
                    print('Procesando...{0}'.format(nombre_prod))
                    new_matricula = row[1]

                    new_fecha_solicitud = parseDate(row[2])
                    try:
                        new_fecha_solicitud_format = datetime.datetime.strptime(new_fecha_solicitud, '%Y-%m-%d').date()
                    except:
                        pass

                    new_observaciones = row[4] + '( SE CREO O ACTUALIZO CON EL SCRIPT DESDE EL MODELO CODIGOS_LI)'
                    new_responsable = row[5]
                    
                    new_cliente_preferencial = parseSN(row[31])
                    # #Datos de GeneracionImpresos
                    # new_gi_fecha_hasta_op = parseDate(row[24])

                    # new_gi_ultimo_n_libro_op = row[22]
                    # new_gi_ultimo_n_hoja_op = row[23]
                    # new_gi_ultimo_n_orden = row[24]
                    # new_gi_fecha_hasta_cobranzas = parseDate(row[25])
                    # new_gi_ultimo_n_libro_cyr = row[26]
                    # new_gi_ultimo_n_hoja_cyr = row[27]

                    # #Encabezados
                    # new_op = row[17]
                    # if new_op == '-':
                    #     new_op = ''
                    # new_cobro = row[18]
                    # new_timbrado = row[19]
                    # new_pago_timbrado = row[20]

                    productor_db = self.obtener_o_crear_productor(nombre_prod, new_matricula)

                    if productor_db:
                        productor_db.matricula = new_matricula
                        productor_db.observaciones = new_observaciones

                        responsable = self.obtener_o_crear_responsable(new_responsable)
                        if responsable:
                            productor_db.encargado = responsable
                        
                        productor_db.preferencial = parseSN(new_cliente_preferencial)
                        productor_db.save()

                        # Aqui generar impresos
                        # Armar la lista
                        if not new_fecha_solicitud:  
                            # Si el archivo excel no tiene fecha_solicitud se toma fecha operaciones
                            try:
                                new_fecha_solicitud = parseDate(row[24])
                                if not new_fecha_solicitud:
                                    continue
                            except:
                                # Si tampoco tiene fecha hasta no se crea nada
                                continue

                        # Si el archivo excel no tiene fecha_solicitud se toma fecha Hasta operaciones
                        if not parseDate(row[24]):
                            continue

                        # Por Ahora no se generan Impresos
                        # self.generar_impresos(productor_db, row)
                        




                    

                    