# -*- coding: utf-8 -*-
from django.contrib.auth.models import UserManager, User

from django.core.management.base import BaseCommand, CommandError
from productores.models import *
from productores.management.commands.importar_productores_codigos_li import parseDate
from companias.management.commands.importar_companias import parseSN
from trabajos.models import *

from django.core.exceptions import ValidationError
import csv


class Command(BaseCommand):

    #def add_arguments(self, parser):
        #parser.add_argument('csv_filename', nargs='+', type=str)

    #handle ejecuta el codigo correspondiente al argumento
    def handle(self, *args, **options):

        while Tarea.objects.count():
            Tarea.objects.all()[0].delete()
