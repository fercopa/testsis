# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from productores.models import *
from companias.management.commands.importar_companias import parseSN
from trabajos.models import *
import datetime
import csv

class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('csv_filename', nargs='+', type=str)

    #handle ejecuta el codigo correspondiente al argumento
    def handle(self, *args, **options):
                csvfile = open('datos_p_sistema.csv', 'rb')
                datos_ficha_reader = csvfile.readlines()
                print (datos_ficha_reader)
                for row in datos_ficha_reader:
                    row = row.split(',')

                    nombre_prod = row[1] + " " + row[0]
                    productor_db =  Productor.objects.get(nombre=nombre_prod)
                    try:
                        new_trabajo = TrabajoImpreso.objects.all().filter(productor=productor_db)[0]
                    except:
                        print "fallo crear trabajo" + nombre
                        continue
                    try:
                        generacion_o = GeneracionImpresos.objects.create(trabajo=new_trabajo,tipo="O",ultimo_libro=row[11], ultima_hoja=row[12], ultimo_nro_orden=row[13], hasta=datetime.datetime.strptime(row[14],"%d %m %Y"))
                        generacion_o.save()
                    except:
                        print "generacion op" + nombre + ": fallida"
                    try:
                        generacion_c = GeneracionImpresos.objects.create(trabajo=new_trabajo,tipo="C",ultimo_libro=row[15],ultima_hoja=row[16], hasta=datetime.datetime.strptime(row[17],"%d %m %Y"))
                        generacion_c.save()
                    except:
                        print "generacion cob" + nombre + ": fallida"
