# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from productores.models import *
from companias.management.commands.importar_companias import parseSN
from trabajos.models import *
import datetime
import csv
from django.utils import timezone

class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('csv_filename', nargs='+', type=str)

    #handle ejecuta el codigo correspondiente al argumento
    def handle(self, *args, **options):
        for csv_filename in options['csv_filename']:
            with open(csv_filename, 'rb') as csvfile:
                codigos_li_reader = csv.reader(csvfile, delimiter=',')  # delimiter='\t', quotechar='|')
                first_row = True
                i = 0
                for row in codigos_li_reader:
                    print str(i) + " of " + row[0] + ' - ' + row[1]
                    #No nos interesa el primer renglón, tiene nombres de columnas
                    if first_row:
                        first_row = False
                        continue

                    #Nombramos los elementos del csv sabiendo el orden de antemano
                    nombre_prod = row[0].strip()
                    compania = row[1].strip()
                    new_identificador = row[3]
                    new_usuario = row[4]
                    new_password = row[5]
                    fecha_hasta_op = parseDate(row[6])
                    fecha_hasta_cob = parseDate(row[7])
                    #¿Existe ya el productor en la base?
                    if Productor.objects.filter(nombre__iexact=nombre_prod).count() == 0:
                        #No existe el productor, hay que crearlo
                        productor_db = Productor.objects.create(nombre=nombre_prod)
                        productor_db.save()
                        print('No Existe se crea...{0}'.format(nombre_prod))
                    else:
                        #Ya existe el productor en nuestra base de datos, vamos a obtenerlo
                        productor_db = Productor.objects.get(nombre__iexact=nombre_prod)
                        print('Ya existe , se obtiene...{0}'.format(nombre_prod))

                    #Existe la compania en la base? Si no, vamos a crearla
                    if Compania.objects.filter(nombre__iexact=compania).count() == 0:
                        compania_db = Compania.objects.create(nombre=compania)
                        compania_db.save()
                    else:
                        compania_db = Compania.objects.get(nombre__iexact=compania)

                    #Existe ProductorEnCompania para este par?
                    if ProductorEnCompania.objects.filter(productor=productor_db, compania=compania_db).count() == 0:
                        #No existe el productor en compania, vamos a crearlo
                        prod_en_compania_db = ProductorEnCompania.objects.create(productor=productor_db, compania=compania_db)
                        prod_en_compania_db.save()

                        #creamos la Clave, ya teniendo el PEC y el Productor
                        clave = Clave.objects.create(productor_en_compania=prod_en_compania_db, identificador=new_identificador, usuario=new_usuario, password=new_password)
                        clave.save()


                    else:
                        prod_en_compania_db = ProductorEnCompania.objects.get(productor=productor_db, compania=compania_db)
                        prod_en_compania_db.save()

                        clave = Clave.objects.get(productor_en_compania=prod_en_compania_db)
                        clave.usuario = new_usuario
                        clave.password = new_password
                        clave.identificador = new_identificador
                        clave.save()

                    if fecha_hasta_op != '':
                        # Comentario Temporal 06 Feb 2017
                        # No es posible crear una tarea sin tener un trabajo asociado coo estaba elcodigo anterior
                        # Se deberia tener una instancia de trabajo creada
                        # Creamos el trabajo

                        new_trabajo = TrabajoImpreso.objects.create(productor=productor_db, fecha=fecha_hasta_op)

                        #creamos la tarea de descarga para Fecha Hasta Op
                        descarga = Descarga.objects.create(trabajo=new_trabajo, productor_en_compania=prod_en_compania_db, hasta=fecha_hasta_op)
                        descarga.save()

                        #creamos el recupero para la tarea de descarga
                        recupero = Recupero.objects.create(trabajo=new_trabajo, productor_en_compania=prod_en_compania_db)
                        recupero.save()
                    
                    i+=1

def parseDate(date):
    date_data = date.split('/')
    if len(date_data) != 3:
        return ''
    day = date_data[0]
    if len(day) == 1:
        day = '0' + day
    month = date_data[1]
    if len(month) == 1:
        month = '0' + month
    year = date_data[2]
    if len(year) == 2:
        year = '20' + year
    if int(month) > 12:
        month, day = day, month
    result = year + '-' + month + '-' + day
    try:
        datetime.datetime.strptime(result, '%Y-%m-%d')
    except ValueError:
        result = ''
    return result
