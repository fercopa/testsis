from django.core.management.base import BaseCommand, CommandError
from productores.models import *
import unicodedata
import sys

class Command(BaseCommand):

    def handle(self, *args, **options):
		productores = Productor.objects.all()
		for productor in productores:
			try:
				nombre_c = ''.join((c for c in unicodedata.normalize('NFD',unicode(productor.nombre)) if unicodedata.category(c) != 'Mn'))
				productor.nombre = nombre_c
				productor.save()
				# print('Cambiando: {0} por --> {1}'.format(productor.nombre, nombre_c))
			except:
				print('Ocurrio un error con este productor', productor.nombre, productor.matricula, sys.exc_info()[0])
