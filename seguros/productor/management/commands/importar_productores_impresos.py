# -*- coding: utf-8 -*-
from django.contrib.auth.models import UserManager, User

from django.core.management.base import BaseCommand, CommandError
from productores.models import *
from productores.management.commands.importar_productores_codigos_li import parseDate
from companias.management.commands.importar_companias import parseSN
from trabajos.models import *

from django.core.exceptions import ValidationError
import csv
import datetime
import sys


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('csv_filename', nargs='+', type=str)

    def obtener_o_crear_responsable(self, new_responsable):

        responsable = User.objects.filter(username__iexact=new_responsable)
        if not responsable:
            try:
                response = User.objects.create_user(username=new_responsable, password=new_responsable)
            except:
                response = ''
        else:
            response = responsable[0]
        return response

    def obtener_o_crear_productor(self, nombre_prod, new_matricula):

        if not Productor.objects.filter(Q(matricula=new_matricula) | Q(nombre__iexact=nombre_prod)):
            response = Productor.objects.create(
                nombre=nombre_prod,
                matricula=new_matricula,
                # observaciones=new_observaciones
                )
            response.save()
            print('No Existe se crea...{0}'.format(nombre_prod))
        else:
            try:
                response = Productor.objects.get(Q(matricula=new_matricula) | Q(nombre__iexact=nombre_prod))
                print('Ya existe , se actualiza...{0}'.format(nombre_prod))
            except:
                print('Ocurrio un error con este productor', nombre_prod, new_matricula, sys.exc_info()[0])
                response = ''
        return response

    #handle ejecuta el codigo correspondiente al argumento
    def handle(self, *args, **options):
        for csv_filename in options['csv_filename']:
            with open(csv_filename, 'rb') as csvfile:
                codigos_impresos_reader = csv.reader(csvfile, delimiter=',')  # delimiter='\t', quotechar='|')
                first_row = True
                for row in codigos_impresos_reader:
                    #No nos interesa el primer renglón, tiene nombres de columnas
                    if first_row:
                        first_row = False
                        continue

                    ### Nombramos todos los elementos del csv sabiendo el orden de antemano
                    nombre_prod = row[0].strip()
                    new_matricula = row[1]

                    if not new_matricula:
                        continue

                    new_fecha_solicitud = parseDate(row[2])
                    try:
                        new_fecha_solicitud_format = datetime.datetime.strptime(new_fecha_solicitud, '%Y-%m-%d').date()
                    except:
                        pass
                    new_observaciones = row[3]
                    new_responsable = row[4]
                    new_cliente_preferencial = parseSN(row[31])

                    #Datos de GeneracionImpresos
                    new_gi_fecha_hasta_op = parseDate(row[21])

                    new_gi_ultimo_n_libro_op = row[22]
                    new_gi_ultimo_n_hoja_op = row[23]
                    new_gi_ultimo_n_orden = row[24]
                    new_gi_fecha_hasta_cobranzas = parseDate(row[25])
                    new_gi_ultimo_n_libro_cyr = row[26]
                    new_gi_ultimo_n_hoja_cyr = row[27]

                    #Encabezados
                    new_op = row[17]
                    if new_op == '-':
                        new_op = ''
                    new_cobro = row[18]
                    new_timbrado = row[19]
                    new_pago_timbrado = row[20]

                    productor_db = self.obtener_o_crear_productor(nombre_prod, new_matricula)

                    # Si no hay productor salta a la siguiente y no hace nada
                    if not productor_db:
                        continue
                    else:
                        productor_db.matricula = new_matricula
                        productor_db.observaciones = new_observaciones

                        responsable = self.obtener_o_crear_responsable(new_responsable)
                        if responsable:
                            productor_db.encargado = responsable
                        
                        productor_db.preferencial = parseSN(new_cliente_preferencial)
                        productor_db.save()

                    #creamos la instancia de Trabajo necesaria para la de GeneracionImpresos
                    try:
                        new_trabajo = TrabajoImpreso.objects.create(productor=productor_db, fecha=new_fecha_solicitud)
                    except:
                        new_fecha_solicitud_format = datetime.datetime.strptime("2000-01-01", '%Y-%m-%d').date()

                        new_trabajo = TrabajoImpreso.objects.create(productor=productor_db, fecha=new_fecha_solicitud_format)

                    #Antes de crear los campos, chequeamos que no sean nulos para evitar problemas de parseo
                    if new_op != '':
                        new_trabajo.cantidad_operaciones = int(new_op)
                    if new_cobro != '':
                        new_trabajo.cantidad_cobranzas = int(new_cobro)
                    if new_timbrado != '':
                        new_trabajo.importe_timbrado = int(new_timbrado)
                    if new_pago_timbrado != '':
                        new_trabajo.cobrado = int(new_pago_timbrado)
                    new_trabajo.save()

                    #creamos la instancia de GeneracionImpresos, con los datos de las columnas amarillas
                    generacion = GeneracionImpresos.objects.create(trabajo=new_trabajo)

                    #Chequeamos los datos para no intentar parsear campos nulos
                    if new_gi_fecha_hasta_op != '':
                        generacion.hasta = datetime.datetime.strptime(new_gi_fecha_hasta_op, '%Y-%m-%d').date()

                    if new_gi_ultimo_n_libro_op != '':
                        generacion.ultimo_libro = int(new_gi_ultimo_n_libro_op)
                    if new_gi_ultimo_n_hoja_op != '':
                        generacion.ultima_hoja = int(new_gi_ultimo_n_hoja_op)
                    if new_gi_ultimo_n_orden != '':
                        generacion.ultimo_nro_orden = int(new_gi_ultimo_n_orden)

                    generacion.save()

'''                 La razón por la cual todo este código está comentado, es que no estoy seguro si la planilla
                    Impresos tiene información que relacione directamente los modelos productor y compania.
                    Esto es, no hay referencias directas a las companias con lo cual no se puede (¿no se debe?)
                    crear una instancia de ProductorEnCompania como se hace con Codigos_LI. Por lo tanto, es muy
                    importante recordar el orden de ejecución de los script, PRIMERO EJECUTAR EL IMPORTAR DE
                    CODIGOS LI Y DESPUES EL DE IMPRESOS.
                    De esta forma, todas las posibles instancias de ProductorEnCompania quedan inicializadas
                    antes de correr este script.
                    Por lo que yo entendí, en realidad impresos hace referencia más bien a trabajos y tareas,
                    no tanto a Productores y Claves propiamente dichas.
                    Queda pendiente importar las fechas (que entiendo que corresponden al campo fecha del modelo
                    Clave), que sin tener un ProductorEnCompania para filtrar, no hay un candidato claro que
                    funcione de "primary key".

                    #No existe la compania en la base? vamos a crearla
                    if Compania.objects.filter(nombre=name).count() == 0:
                        compania_db = Compania.objects.create(nombre=compania)
                        compania_db.save()
                    else:
                        compania_db = Compania.objects.get(nombre=compania)
                    if ProductorEnCompania.objects.filter(compania=compania_db, productor=productor_db).count() == 0:
                        #creamos el Productor En Compania (PEC)

                        prod_en_compania = ProductorEnCompania.objects.create(productor=productor_db, compania=compania_db)
                        prod_en_compania.save()

                        #creamos la Clave, ya teniendo el PEC y el Productor creados
                        clave = Clave.objects.create(productor_en_compania=prod_en_compania, identificador=new_identificador, usuario=new_usuario, password=new_password, fecha=new_fecha_solicitud)
                        clave.save()

                    else:
                    prod_en_compania = ProductorEnCompania.objects.get(productor=productor_db, compania=compania_db)
                    clave = Clave.objects.get(productor_en_compania=prod_en_compania)
                    productor_db.matricula = new_matricula
                    productor_db.preferencial = parseSN(new_cliente_preferencial)
                    productor_db.save()
                    clave.fecha = new_fecha_solicitud
                    clave.save()

                    #creamos la instancia de Trabajo necesaria para la de GeneracionImpresos
                    new_trabajo = Trabajo.objects.create(productor=productor_db, cantidad_operaciones=new_op, cantidad_cobranzas=new_cobro, importe_timbrado=new_timbrado, cobrado=new_pago_timbrado)
                    new_trabajo.save()

                    #creamos la instancia de GeneracionImpresos, con los datos de las columnas amarillas
                    generacion = GeneracionImpresos.objects.create(trabajo = new_trabajo, hasta=new_gi_fecha_hasta_op, ultimo_libro=new_gi_ultimo_n_libro_op, ultima_hoja=new_gi_ultimo_n_hoja_op, ultimo_nro_orden=new_gi_ultimo_n_orden)
                    generacion.save()'''
