# -*- coding: utf-8 -*-
from django.contrib.auth.models import UserManager, User
from django.db.models import Q

from django.core.management.base import BaseCommand, CommandError
from productores.models import *
from productores.management.commands.importar_productores_codigos_li import parseDate
from companias.management.commands.importar_companias import parseSN
from trabajos.models import *

from django.core.exceptions import ValidationError
import csv
import datetime
from datetime import datetime

import sys


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('csv_filename', nargs='+', type=str)

    def obtener_o_crear_productor(self, nombre_prod, new_matricula):

        if not Productor.objects.filter(Q(matricula=new_matricula) | Q(nombre__iexact=nombre_prod)):
            response = Productor.objects.create(
                nombre=nombre_prod,
                matricula=new_matricula,
                observaciones='Se creo desde el script importar_nombre_y_ficha'
                )
            response.save()
            print('NO EXISTE SE CREA...{0}'.format(nombre_prod))
        else:
            try:
                response = Productor.objects.get(Q(matricula=new_matricula) | Q(nombre__iexact=nombre_prod))
                print('Ya existe , se actualiza...{0}'.format(nombre_prod))
            except:
                print('Ocurrio un error con este productor', nombre_prod, new_matricula, sys.exc_info()[0])
                response = ''
        return response

    #handle ejecuta el codigo correspondiente al argumento
    def handle(self, *args, **options):
        for csv_filename in options['csv_filename']:
            with open(csv_filename, 'rb') as csvfile:
                codigos_impresos_reader = csv.reader(csvfile, delimiter=',')  # delimiter='\t', quotechar='|')
                first_row = True
                for row in codigos_impresos_reader:
                    #No nos interesa el primer renglón, tiene nombres de columnas
                    if first_row:
                        first_row = False
                        continue

                    nombre_prod = row[0].strip()
                    print('Procesando...{0}'.format(nombre_prod))
                    new_matricula = row[1]

                    productor_db = self.obtener_o_crear_productor(nombre_prod, new_matricula)

                    if productor_db:
                        productor_db.matricula = new_matricula
                        productor_db.save()
