from .. import forms
import pytest
pytestmark = pytest.mark.django_db


class TestProductorForms:
    def test_productor_search_empty(self):
        data = {}
        form = forms.ProductorSearch(data=data)
        assert form.is_valid()

    def test_productor_search_with_data(self):
        data = {'productor': 'Hola', }
        form = forms.ProductorSearch(data=data)
        assert form.is_valid()
