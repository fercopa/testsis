import datetime
import pytz
from django.test import TestCase
from .models import Productor


def tz_parse(date):
    p = datetime.datetime.strptime(date, "%Y-%m-%d")
    tz = pytz.timezone("UTC")
    return tz.localize(p)


class ProductorModelTest(TestCase):
    def test_string_representation(self):
        pass

    def test_hojas(self):
        pass

    def test_ultimo_libro_en_uso(self):
        pass

    def test_ultimo_libro_en_uso_o(self):
        pass

    def test_ultimo_libro_en_uso_c(self):
        pass

    def test_hojas_disponibles_o(self):
        pass

    def test_hojas_disponibles_c(self):
        pass

    def test_ultima_hoja(self):
        pass

    def test_generacion_hasta(self):
        pass

    def test_en_stock_actual_anterior(self):
        pass

    def test_buscar_libro_actual_anterior(self):
        pass

    def test_hoja_y_actual_anterior(self):
        pass

    def test_orden_y_actual_anterior(self):
        pass

    def test_generacion_hasta_link(self):
        pass

    def test_fecha_generacion(self):
        pass

    def test_impresion_actual_anterior(self):
        pass


class ProductorEnCompaniaModelTest(TestCase):
    def test_clave_vigente(self):
        pass


class CodigoModelTest(TestCase):
    def test_vigentes(self):
        pass


class ClaveModelTest(TestCase):
    def test_vigencia(self):
        pass
