import datetime
from datetime import timedelta
from productor.models import JURIDICA, FISICA, OPERACION, COBRANZA
from mixer.backend.django import mixer
import pytest
pytestmark = pytest.mark.django_db


class TestLicence:
    def test_model(self):
        licence = mixer.blend('productor.Licence')
        assert licence.pk == 1


class TestProvincia:
    def test_model(self):
        obj = mixer.blend('productor.Provincia')
        assert obj.pk == 1

    def test_get_nombre(self):
        expected = 'Jujuy'
        provincia = mixer.blend('productor.Provincia', codigo=6)
        nombre = provincia.get_nombre()
        assert nombre == expected

    def test_get_nombre_with_bad_code(self):
        expected = ''
        provincia = mixer.blend('productor.Provincia', codigo=43)
        nombre = provincia.get_nombre()
        assert nombre == expected

    def test_get_nombre_corto(self):
        expected = 'JUY'
        provincia = mixer.blend('productor.Provincia', codigo=6)
        nombre_corto = provincia.get_nombre_corto()
        assert nombre_corto == expected

    def test_get_nombre_corto_with_bad_code(self):
        expected = ''
        provincia = mixer.blend('productor.Provincia', codigo=43)
        nombre_corto = provincia.get_nombre_corto()
        assert nombre_corto == expected

    def test_get_nombre_abreviado(self):
        expected = 'JY'
        provincia = mixer.blend('productor.Provincia', codigo=6)
        abreviatura = provincia.get_nombre_abreviado()
        assert abreviatura == expected

    def test_get_nombre_abreviado_with_bad_code(self):
        expected = ''
        provincia = mixer.blend('productor.Provincia', codigo=43)
        nombre_abreviado = provincia.get_nombre_abreviado()
        assert nombre_abreviado == expected


class TestLocalidad:
    def test_model(self):
        obj = mixer.blend('productor.Localidad')
        assert obj.pk == 1

    def test_get_absolute_url(self):
        localidad = mixer.blend('productor.Localidad')
        url = localidad.get_absolute_url()
        assert str(localidad.pk) in url

    def test_get_provinica_name(sellf):
        name_expected = 'Jujuy'
        provincia = mixer.blend('productor.Provincia', codigo=6)
        localidad = mixer.blend('productor.Localidad', provincia=provincia)
        provincia_name = localidad.get_provincia_name()
        assert name_expected == provincia_name


class TestProfile:
    def test_model(self):
        profile = mixer.blend('productor.Profile')
        assert profile.pk == 1

    def test_get_localidad_name(self):
        name_expected = 'Rio Seco'
        profile = mixer.blend('productor.Profile',
                              localidad__nombre=name_expected)
        localidad_name = profile.get_localidad_name()
        assert localidad_name == name_expected

    def test_get_provincia_name(self):
        name_expected = 'Jujuy'
        profile = mixer.blend('productor.Profile',
                              localidad__provincia__codigo=6)
        provincia_name = profile.get_provincia_name()
        assert provincia_name == name_expected

    def test_get_full_name(self):
        expected = 'COPA, FERNANDO ESTEBAN'
        profile = mixer.blend('productor.Profile', apellido='Copa',
                              nombres='Fernando Esteban')
        full_name = profile.get_full_name()
        assert full_name == expected


class TestProductor:
    def test_model(self):
        productor = mixer.blend('productor.Productor')
        assert productor.pk == 1

    def test_get_link_ssn_juridica(self):
        productor = mixer.blend('productor.Productor', tipo_persona=JURIDICA,
                                matricula=914)
        url = productor.get_link_to_ssn()
        assert 'SOC' in url

    def test_get_link_ssn_fisica(self):
        productor = mixer.blend('productor.Productor', tipo_persona=FISICA,
                                matricula=50965)
        url = productor.get_link_to_ssn()
        assert 'PAS' in url

    def test_get_absolute_url(self):
        productor = mixer.blend('productor.Productor')
        url = productor.get_absolute_url()
        assert str(productor.pk) in url

    def test_amount_companies(self):
        productor = mixer.blend('productor.Productor')
        mixer.cycle(5).blend('productor.ProductorEnCompania',
                             productor=productor)
        res = productor.get_amount_companies()
        assert res == 5

    def test_get_full_address(self):
        provincia = mixer.blend('productor.Provincia', codigo=3)
        localidad = mixer.blend('productor.Localidad', nombre='Carlos Paz',
                                provincia=provincia)
        direccion = 'Av. Colón 3894 B* San Ignacio'
        profile = mixer.blend('productor.Profile', direccion=direccion,
                              localidad=localidad)
        productor = mixer.blend('productor.Productor', profile=profile)
        expected_value = ', '.join([direccion, localidad.nombre,
                                    provincia.get_nombre()])
        full_address = productor.get_full_address()
        assert full_address == expected_value

    def test_get_full_name(self):
        profile = mixer.blend('productor.Profile', apellido='Agatha',
                              nombres='Christie Noemi')
        productor = mixer.blend('productor.Productor', profile=profile)
        full_name = productor.get_full_name()
        assert full_name.upper() == 'Agatha, Christie Noemi'.upper()

    def test_get_cuit(self):
        cuit_expected = 20453545853
        profile = mixer.blend('productor.Profile', cuit=20453545853)
        productor = mixer.blend('productor.Productor', profile=profile)
        cuit = productor.get_cuit()
        assert cuit == cuit_expected

    def test_get_localidad_name(self):
        name_expected = 'San Fernando de Catamarca'
        localidad = mixer.blend('productor.Localidad', nombre=name_expected)
        profile = mixer.blend('productor.Profile', localidad=localidad)
        productor = mixer.blend('productor.Productor', profile=profile)
        localidad_name = productor.get_localidad_name()
        assert localidad_name == name_expected

    def test_get_provincia_name(self):
        name_expected = 'La Pampa'
        provincia = mixer.blend('productor.Provincia', codigo=21)
        localidad = mixer.blend('productor.Localidad', provincia=provincia)
        profile = mixer.blend('productor.Profile', localidad=localidad)
        productor = mixer.blend('productor.Productor', profile=profile)
        provinica_name = productor.get_provincia_name()
        assert provinica_name == name_expected

    def test_get_direccion(self):
        expected = 'Av. Velez Sarfield 233 B* Alta Córdoba'
        profile = mixer.blend('productor.Profile', direccion=expected)
        productor = mixer.blend('productor.Productor', profile=profile)
        direccion = productor.get_direccion()
        assert direccion == expected

    def test_get_email(self):
        expected = 'admin@gmail.com'
        profile = mixer.blend('productor.Profile', email=expected)
        productor = mixer.blend('productor.Productor', profile=profile)
        email = productor.get_email()
        assert email == expected

    def test_get_last_active_work(self):
        productor = mixer.blend('productor.Productor')
        year = 2014
        desde = datetime.date(year, 12, 23)
        hasta = datetime.date(year+1, 12, 23)
        for i in range(1, 6):
            mixer.blend('trabajo.TrabajoDigital', productor=productor,
                        terminado=True, fecha_desde=desde, fecha_hasta=hasta)
            desde = datetime.date(year+i, 12, 23)
            hasta = datetime.date(year+1+i, 12, 23)
        work = mixer.blend('trabajo.TrabajoDigital', productor=productor,
                           terminado=False, fecha_desde=desde,
                           fecha_hasta=hasta)
        current_work = productor.get_last_active_work()
        assert current_work.pk == work.pk
        current_work.terminado = False
        current_work.save()
        assert current_work.pk == work.pk

    def test_get_active_companies(self):
        expected_value = 9
        productor = mixer.blend('productor.Productor')
        for i in range(3):
            cia = mixer.blend('compania.Compania', active=False)
            mixer.blend('productor.ProductorEnCompania', productor=productor,
                        compania=cia)
        for j in range(expected_value):
            cia = mixer.blend('compania.Compania', active=True)
            mixer.blend('productor.ProductorEnCompania', productor=productor,
                        compania=cia)
        cias = productor.get_active_companies()
        assert len(cias) == expected_value

    def test_get_productor_en_companias_without_companies(self):
        productor = mixer.blend('productor.Productor')
        companias = productor.get_productor_en_companias()
        assert len(companias) == 0

    def test_get_productor_en_companias_with_some_companies(self):
        productor = mixer.blend('productor.Productor')
        mixer.cycle(5).blend('productor.ProductorEnCompania',
                             productor=productor)
        pecs = productor.get_productor_en_companias()
        assert len(pecs) == 5

    def test_create_digital_work(self):
        desde = datetime.date(2016, 1, 21)
        hasta = datetime.date(2018, 1, 21)
        productor = mixer.blend('productor.Productor')
        productor.create_digital_work(desde, hasta)
        assert productor.exist_pending_works() is True

    def test_create_several_digital_work(self):
        desde = datetime.date(2016, 1, 21)
        hasta = datetime.date(2018, 1, 21)
        productor = mixer.blend('productor.Productor')
        productor.create_digital_work(desde, hasta)
        productor.create_digital_work(desde, hasta)
        works = productor.get_active_works()
        assert len(works) == 1


class TestProductorEnCompania:
    def test_model(self):
        pes = mixer.blend('productor.ProductorEnCompania')
        assert pes.pk == 1

    def test_get_usuario_as_string(self):
        expected = 'ADMIN'
        pes = mixer.blend('productor.ProductorEnCompania')
        mixer.blend('productor.Clave', productor_en_compania=pes,
                    usuario=expected)
        usuario = pes.get_usuario_as_string()
        assert usuario == expected

    def test_get_password_as_string(self):
        expected = 'Admin1234'
        pes = mixer.blend('productor.ProductorEnCompania')
        mixer.blend('productor.Clave', productor_en_compania=pes,
                    password=expected)
        password = pes.get_password_as_string()
        assert password == expected

    def test_get_identificador_as_string(self):
        expected = '12345678'
        pes = mixer.blend('productor.ProductorEnCompania')
        mixer.blend('productor.Clave', productor_en_compania=pes,
                    identificador=expected)
        identificador = pes.get_identificador_as_string()
        assert identificador == expected

    def test_get_clave_without_clave(self):
        pec = mixer.blend('productor.ProductorEnCompania')
        clave = pec.get_clave()
        assert clave is None

    def test_get_clave_with_clave(self):
        pec = mixer.blend('productor.ProductorEnCompania')
        mixer.blend('productor.Clave', productor_en_compania=pec)
        clave = pec.get_clave()
        assert clave is not None

    def test_get_portal_web(self):
        expected = 'https://www.google.com.ar'
        pec = mixer.blend('productor.ProductorEnCompania',
                          compania__portal_web=expected)
        portal_web = pec.get_portal_web()
        assert expected == portal_web

    def test_get_portal_web_alternativo(self):
        expected = 'https://www.google.com'
        pec = mixer.blend('productor.ProductorEnCompania',
                          compania__portal_web_alternativo=expected)
        portal_web = pec.get_portal_web_alternativo()
        assert portal_web == expected

    def test_is_not_active_the_company(self):
        pec = mixer.blend('productor.ProductorEnCompania',
                          compania__active=False)
        assert not pec.is_active_the_company()

    def test_is_active_the_company(self):
        pec = mixer.blend('productor.ProductorEnCompania',
                          compania__active=True)
        assert pec.is_active_the_company()


class TestRubricaDigital:
    def test_model(self):
        rubrica = mixer.blend('productor.RubricaDigital')
        assert rubrica.pk == 1

    def test_get_last_cobranza_book(self):
        last_date = datetime.date(2017, 8, 31)
        rubrica = mixer.blend('productor.RubricaDigital')
        book_expected = mixer.blend('productor.LibroDigital', rubrica=rubrica,
                                    tipo=COBRANZA, fecha_hasta=last_date)
        for i in range(2):
            mixer.blend('productor.LibroDigital', rubrica=rubrica,
                        tipo=COBRANZA,
                        fecha_hasta=datetime.date(2016+i, 3+i, 20))
        last_cob_book = rubrica.get_last_cobranza_book()
        assert last_cob_book.pk == book_expected.pk

    def test_get_last_operacion_book(self):
        last_date = datetime.date.today()
        rubrica = mixer.blend('productor.RubricaDigital')
        book_expected = mixer.blend('productor.LibroDigital', rubrica=rubrica,
                                    tipo=OPERACION, fecha_hasta=last_date)
        for i in range(2):
            mixer.blend('productor.LibroDigital', rubrica=rubrica,
                        tipo=OPERACION,
                        fecha_hasta=datetime.date(2017+i, 5+i, 21))
        last_ope_book = rubrica.get_last_operacion_book()
        assert last_ope_book.pk == book_expected.pk

    def test_get_time_since_the_last_rubrica(self):
        time_expected = (3, 4, 12)
        days = 3*360 + 4*30 + 12  # 30 days = 1 month
        last_work = datetime.date.today() - timedelta(days=days)
        rubrica = mixer.blend('productor.RubricaDigital')
        mixer.blend('productor.LibroDigital', fecha_hasta=last_work,
                    rubrica=rubrica)
        time_past = rubrica.get_time_since_the_last_rubrica()
        assert time_past == time_expected

    def test_get_presupuesto(self):
        book = mixer.blend('productor.RubricaDigital', amount_records=3000)
        total = book.get_presupuesto()
        assert total > 0

    def test_get_credits_available(self):
        rubrica = mixer.blend('productor.RubricaDigital', credits=15000)
        mixer.blend('productor.LibroDigital', amount_records=3000,
                    tipo=OPERACION, rubrica=rubrica)
        mixer.blend('productor.LibroDigital', amount_records=3000,
                    tipo=COBRANZA, rubrica=rubrica)
        creditos = rubrica.get_credits_available()
        assert creditos == 9000


class TestLibro:
    def test_model(self):
        book = mixer.blend('productor.Libro')
        assert book.pk == 1
