from productor.templatetags.productor_tags import markdown_to_html
import pytest
pytestmark = pytest.mark.django_db


class TestTemplateTags:
    def test_markdown_to_html(self):
        md = "# Titulo"
        html = markdown_to_html(md)
        assert '<h1>' in html

    def test_markdown_to_html_with_no_string(self):
        md = 12
        html = markdown_to_html(md)
        assert html == ''
