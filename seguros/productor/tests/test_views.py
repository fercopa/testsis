from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory
from mixer.backend.django import mixer
from .. import views
from .. import models
import pytest
pytestmark = pytest.mark.django_db


def anonymous_get(view, *args):
    req = RequestFactory().get('/')
    req.user = AnonymousUser()
    pk = args[0] if args else None
    return view(req, pk=pk)


def superuser_get(view, *args):
    user = mixer.blend('auth.User', is_superuser=True)
    req = RequestFactory().get('/')
    req.user = user
    pk = args[0] if args else None
    return view(req, pk=pk)


class TestHomeView:
    def test_anonymous_get(self):
        resp = anonymous_get(views.HomeView.as_view())
        assert 'login' in resp.url

    def test_superuser_get(self):
        resp = superuser_get(views.HomeView.as_view())
        assert resp.status_code == 200

    def test_add_all_provincias(self):
        user = mixer.blend('auth.User')
        req = RequestFactory().post('/')
        req.user = user
        req.POST = 1
        resp = views.add_all_provincias(req)
        amount_provincias = models.Provincia.objects.all().count()
        assert resp.status_code == 302
        assert amount_provincias == 24


class TestProductorList:
    def test_anonymous_get(self):
        resp = anonymous_get(views.ProductorList.as_view())
        assert 'login' in resp.url

    def test_superuser_get(self):
        resp = superuser_get(views.ProductorList.as_view())
        assert resp.status_code == 200


class TestProductorDetail:
    def test_anonymous_get(self):
        productor = mixer.blend('productor.Productor')
        args = productor.pk
        resp = anonymous_get(views.ProductorDetail.as_view(), args)
        assert 'login' in resp.url

    def test_superuser_get(self):
        productor = mixer.blend('productor.Productor')
        args = productor.pk
        resp = superuser_get(views.ProductorDetail.as_view(), args)
        assert resp.status_code == 200


class TestProductoUpdate:
    def test_anonymous_get(self):
        productor = mixer.blend('productor.Productor')
        resp = anonymous_get(views.ProductorUpdate.as_view(), productor.pk)
        assert 'login' in resp.url

    def test_superuser_get(self):
        productor = mixer.blend('productor.Productor')
        resp = superuser_get(views.ProductorUpdate.as_view(), productor.pk)
        assert resp.status_code == 200


class TestProductorCreate:
    def test_anonymous_get(self):
        resp = anonymous_get(views.ProductorCreate.as_view())
        assert 'login' in resp.url

    def test_superuser_get(self):
        resp = superuser_get(views.ProductorCreate.as_view())
        assert resp.status_code == 200


class TestProductorEnCompaniaList:
    def test_anonymous_get(self):
        resp = anonymous_get(views.ProductorEnCiaList.as_view())
        assert 'login' in resp.url

    def test_superuser_get(self):
        resp = superuser_get(views.ProductorEnCiaList.as_view())
        assert resp.status_code == 200


class TestProductorEnCompaniaDetail:
    def test_anonymous_get(self):
        productor = mixer.blend('productor.ProductorEnCompania')
        args = productor.pk
        resp = anonymous_get(views.ProductorEnCiaDetail.as_view(), args)
        assert 'login' in resp.url

    def test_superuser_get(self):
        productor = mixer.blend('productor.ProductorEnCompania')
        args = productor.pk
        resp = superuser_get(views.ProductorEnCiaDetail.as_view(), args)
        assert resp.status_code == 200


class TestProductorEnCompaniaUpdate:
    def test_anonymous_get(self):
        pec = mixer.blend('productor.ProductorEnCompania')
        resp = anonymous_get(views.ProductorEnCiaUpdate.as_view(), pec.pk)
        assert 'login' in resp.url

    def test_superuser_get(self):
        pec = mixer.blend('productor.ProductorEnCompania')
        resp = superuser_get(views.ProductorEnCiaUpdate.as_view(), pec.pk)
        assert resp.status_code == 200


class TestProductorSearchToJson:
    def test_anonymous_search_to_json(self):
        req = RequestFactory().get('/')
        req.user = AnonymousUser()
        resp = views.productor_search_to_json(req)
        assert 'login' in resp.url

    def test_superuser_search_to_json(self):
        user = mixer.blend('auth.User')
        req = RequestFactory().get('/')
        req.user = user
        resp = views.productor_search_to_json(req)
        assert resp.status_code == 200


class TestProductorSearch:
    def test_anonymous_productor_search(self):
        req = RequestFactory().get('/')
        req.user = AnonymousUser()
        resp = views.productor_search(req)
        assert 'login' in resp.url

    def test_superuser_productor_search(self):
        user = mixer.blend('auth.User')
        req = RequestFactory().get('/')
        req.user = user
        resp = views.productor_search(req)
        assert resp.status_code == 200
