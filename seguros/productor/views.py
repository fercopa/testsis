# -*- coding: utf-8 -*-
# pylint: disable-all
import json
from django.shortcuts import render, redirect
from django.http import HttpResponse

from django.views.generic.base import View
from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView, CreateView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from .variables import PROVINCIAS
from . import models


@method_decorator(login_required, 'dispatch')
class CreateProfile(CreateView):
    model = models.Profile
    fields = '__all__'
    template_name = 'productor_create_profile.html'
    success_url = '/productores/'


@method_decorator(login_required, 'dispatch')
class HomeView(TemplateView):
    template_name = 'productor_home.html'


@method_decorator(login_required, 'dispatch')
class ProductorList(ListView):
    model = models.Productor
    template_name = 'productor_list.html'


@method_decorator(login_required, 'dispatch')
class ProductorDetail(DetailView):
    model = models.Productor
    template_name = 'productor_detail.html'


@method_decorator(login_required, 'dispatch')
class ProductorUpdate(UpdateView):
    model = models.Productor
    fields = '__all__'
    template_name = 'productor_update.html'


@method_decorator(login_required, 'dispatch')
class ProductorCreate(CreateView):
    model = models.Productor
    fields = '__all__'
    template_name = 'productor_update.html'


@method_decorator(login_required, 'dispatch')
class ProductorEnCiaList(ListView):
    model = models.ProductorEnCompania
    template_name = 'productor_en_cia_list.html'


@method_decorator(login_required, 'dispatch')
class ProductorEnCiaDetail(DetailView):
    model = models.ProductorEnCompania
    template_name = 'productor_en_cia_detail.html'


@method_decorator(login_required, 'dispatch')
class ProductorEnCiaUpdate(UpdateView):
    model = models.ProductorEnCompania
    fields = '__all__'
    template_name = 'productor_en_cia_update.html'


@method_decorator(login_required, 'dispatch')
class LocalidadCreate(CreateView):
    model = models.Localidad
    fields = '__all__'
    template_name = 'localidad_update.html'


@method_decorator(login_required, 'dispatch')
class ProvinciaList(ListView):
    model = models.Provincia
    template_name = 'provincia_list.html'


@method_decorator(login_required, 'dispatch')
class LocalidadDetail(DetailView):
    model = models.Localidad
    template_name = 'localidad_detail.html'


@method_decorator(login_required, 'dispatch')
class CreateLicencia(CreateView):
    model = models.Licence
    fields = '__all__'
    template_name = 'create_licencia.html'


@login_required
def add_all_provincias(request):
    if request.POST:
        provincia = models.Provincia
        for code, data in PROVINCIAS.items():
            try:
                provincia.objects.get(codigo=code)
            except provincia.DoesNotExist:
                provincia.objects.create(codigo=code)
    return redirect('provincia-list')


def dashboard(request):
    pass


class CrearImpresion(View):
    pass


@login_required
def productor_search_to_json(request):
    q = request.GET.get('q', '')
    profile = models.Profile.objects.filter(nombres__contains=q)
    # Retuturn the value null for materializecss autocomplete
    res = [{'label': p.get_full_name(), 'value': None} for p in profile]
    data = json.dumps(res)
    mimetype = 'application/json'
    return HttpResponse(data, content_type=mimetype)


@login_required
def productor_search(request):
    q = request.GET.get('q', '')
    profiles = models.Profile.objects.filter(nombres__icontains=q)
    return render(request, 'productor_results.html', {'profiles': profiles})
