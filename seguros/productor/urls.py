from django.conf.urls import url
from productor import views

urlpatterns = [
    url(r'^$', views.HomeView.as_view(), name='productor-home'),
    url(r'^profile/add/$', views.CreateProfile.as_view(),
        name='productor-create-profile'),
    url(r'^add/$', views.ProductorCreate.as_view(), name='productor-create'),
    url(r'^(?P<pk>\d+)/edit/$', views.ProductorUpdate.as_view(),
        name='productor-update'),
    url(r'^all/$', views.ProductorList.as_view(), name='productor-list'),
    url(r'^search/$', views.productor_search, name='productor-search'),
    url(r'^search/json/$', views.productor_search_to_json,
        name='productor-search-to-json'),
    url(r'^(?P<pk>\d+)/detail/$', views.ProductorDetail.as_view(),
        name='productor-detail'),
    url(r'^localidad/add/$', views.LocalidadCreate.as_view(),
        name='localidad-create'),
    url(r'^provincia/all/$', views.ProvinciaList.as_view(),
        name='provincia-list'),
    url(r'^add-all-provincias/$', views.add_all_provincias,
        name='add-all-provincias'),
    url(r'^licencia/add/$', views.CreateLicencia.as_view(),
        name='create-licencia'),
    url(r'^localidad/(?P<pk>\d+)/detail/$', views.LocalidadDetail.as_view(),
        name='localidad-detail'),
]
