from django.template import Library
import markdown

register = Library()


def markdown_to_html(text):
    try:
        md = markdown.markdown(text)
        html = md.replace('<ul>', '<ul class="browser-default">')
    except AttributeError:
        html = ''
    return html


register.filter('marked', markdown_to_html)
