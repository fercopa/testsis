from django import forms


class TrabajoImpresoInlineForm(forms.ModelForm):
    pass


class TrabajoDigitalInlineForm(forms.ModelForm):
    pass


class TerminadosInlineForm(forms.ModelForm):
    pass


class NuevaImpresionForm(forms.Form):
    fecha = forms.DateField(required=False, label='Fecha Generacion')
    fechah = forms.DateField(required=False, label='Fecha Hasta')
    libro = forms.CharField(required=False, label='Libro Ope')
    hoja = forms.CharField(required=False, label='Hoja Ope.')
    orden = forms.CharField(required=False, label='Orden Ope.')

    fecha_c = forms.DateField(required=False, label='Fecha Generacion')
    fechah_c = forms.DateField(required=False, label='Fecha Hasta')
    libro_c = forms.CharField(required=False, label='Libro Cob')
    hoja_c = forms.CharField(required=False, label='Hoja Cob.')


class ProductorSearch(forms.Form):
    productor = forms.CharField(required=False, label='Productor',
                                widget=forms.TextInput(attrs={'id': 'search'}))
