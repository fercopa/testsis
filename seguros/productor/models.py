import datetime
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse_lazy

from compania.models import Compania
from trabajo.models import TrabajoDigital, TrabajoImpreso, Trabajo
from productor.variables import (
        OPERACION, COBRANZA, JURIDICA, FISICA, TIPO_PERSONA, SSN_URL,
        PROVINCIAS,
)


def get_provincia_choices():
    choices = [(k, v['nombre']) for k, v in PROVINCIAS.items()]
    return choices


class Licence(models.Model):
    licencia = models.IntegerField()
    licencias_extra = models.IntegerField(blank=True, default=0)
    licencia_preferencial = models.BooleanField(default=False)

    def __str__(self):
        return "Licencia: %s" % self.licence


class Provincia(models.Model):
    codigo = models.IntegerField(choices=get_provincia_choices())

    def __str__(self):
        return self.get_nombre()

    def get_nombre(self):
        try:
            provincia = PROVINCIAS[self.codigo]
            nombre = provincia['nombre']
        except Exception:
            nombre = ''
        return nombre

    def get_nombre_corto(self):
        try:
            provincia = PROVINCIAS[self.codigo]
            nombre_corto = provincia['nombre_corto']
        except Exception:
            nombre_corto = ''
        return nombre_corto

    def get_nombre_abreviado(self):
        try:
            provincia = PROVINCIAS[self.codigo]
            abreviatura = provincia['abreviacion']
        except Exception:
            abreviatura = ''
        return abreviatura

    class Meta:
        ordering = ["codigo"]


class Localidad(models.Model):
    nombre = models.CharField(max_length=100)
    provincia = models.ForeignKey(Provincia, null=True, blank=True)
    descripcion = models.CharField(max_length=240, blank=True)

    def __str__(self):
        return self.nombre

    def get_provincia_name(self):
        provincia = self.provincia
        return provincia.get_nombre()

    def get_absolute_url(self):
        return reverse_lazy('localidad-detail', args=[self.pk, ])

    class Meta:
        verbose_name_plural = "Localidades"
        ordering = ["nombre"]


class Profile(models.Model):
    apellido = models.CharField(max_length=50)
    nombres = models.CharField(max_length=150)
    direccion = models.CharField(max_length=150, blank=True)
    localidad = models.ForeignKey(Localidad, null=True, blank=True)
    telefono = models.CharField(max_length=50, blank=True)
    email = models.EmailField(max_length=50, null=True, blank=True)
    cuit = models.IntegerField(blank=True)

    def __str__(self):
        return self.get_full_name()

    def get_full_name(self):
        return ', '.join((self.apellido.upper(), self.nombres.upper()))

    def get_localidad_name(self):
        localidad = self.localidad
        return localidad.nombre

    def get_provincia_name(self):
        localidad = self.localidad
        return localidad.get_provincia_name()


class Productor(models.Model):
    profile = models.OneToOneField(Profile, on_delete=models.CASCADE)
    licence = models.OneToOneField(Licence, null=True, blank=True)
    tipo_persona = models.IntegerField(default=FISICA, choices=TIPO_PERSONA)
    clave_fiscal = models.CharField(max_length=20, null=True, blank=True)
    matricula = models.IntegerField(unique=True)
    articulo_19 = models.BooleanField(default=False)
    rubrica_digital = models.BooleanField(default=False)
    preferencial = models.BooleanField(default=False)
    trabajo_mensual = models.BooleanField(default=False)

    def __str__(self):
        return self.get_full_name()

    class Meta:
        verbose_name_plural = "Productores"

    def create_digital_work(self, desde, hasta):
        if not self.exist_pending_works():
            work = TrabajoDigital.objects.create(
                    productor=self, fecha_desde=desde, fecha_hasta=hasta)
            work.proponer()

    def get_link_to_ssn(self):
        url = ''
        mat = str(self.matricula)
        if self.tipo_persona == FISICA:
            url = SSN_URL + 'matricula=%s' % mat + "&socpro="
            url += "PAS"
        elif self.tipo_persona == JURIDICA:
            url = SSN_URL + 'socpro=SOC' + "&matricula=%s" % mat
            url += '&apellidorazonsocial=&Submit=Buscar'
        return url

    def get_absolute_url(self):
        return reverse_lazy('productor-detail', args=[str(self.pk), ])

    def get_amount_companies(self):
        res = ProductorEnCompania.objects.filter(productor=self)
        return len(res)

    def get_cuit(self):
        profile = self.profile
        return profile.cuit

    def get_direccion(self):
        profile = self.profile
        return profile.direccion

    def get_localidad_name(self):
        profile = self.profile
        return profile.get_localidad_name()

    def get_provincia_name(self):
        profile = self.profile
        return profile.get_provincia_name()

    def get_full_address(self):
        direccion = self.get_direccion()
        localidad = self.get_localidad_name()
        provincia = self.get_provincia_name()
        full_address = ', '.join([direccion, localidad, provincia])
        return full_address

    def get_full_name(self):
        profile = self.profile
        last_name = profile.apellido
        names = profile.nombres
        full_name = ', '.join([last_name, names])
        return full_name

    def get_email(self):
        profile = self.profile
        return profile.email

    def get_last_active_work(self):
        works = Trabajo.objects.filter(productor=self,
                                       terminado=False).order_by('-fecha')
        return works[0] if works else None

    def get_active_companies(self):
        cias_actives = ProductorEnCompania.objects.filter(
                productor=self, compania__active=True)
        return cias_actives

    def exist_pending_works(self):
        works_digital = TrabajoDigital.objects.filter(productor=self,
                                                      terminado=False)
        works_impreso = TrabajoImpreso.objects.filter(productor=self,
                                                      terminado=False)
        return True if works_digital or works_impreso else False

    def get_active_works(self):
        works = Trabajo.objects.filter(productor=self, terminado=False)
        return works

    def get_productor_en_companias(self):
        pecs = ProductorEnCompania.objects.filter(productor=self)
        return pecs

    def get_active_work(self):
        return Trabajo.objects.get(productor=self, terminado=False)

    def hojas_disponibles(self, tipo):
        pass

    # Las siguientes dos funciones fueron definidas solo para poder usar en el
    # HTML template su valor como "atributo"

    def ultimo_libro_en_uso(self, tipo):
        pass

    def ultimo_libro_en_uso_o(self):
        pass

    def ultimo_libro_en_uso_c(self):
        pass

    def hojas_disponibles_c(self):
        pass

    def hojas_disponibles_o(self):
        pass

    def ultima_hoja(self, tipo):
        pass

    def ultimo_nro_orden(self, tipo):
        pass

    def generacion_hasta(self, tipo):
        pass

    # Carlos Garcia
    def en_stock_actual_anterior(self, tipo, orden_de_impresion):
        pass

    def buscar_libro_actual_y_anterior(self, tipo, orden_de_impresion):
        pass

    def hoja_actual_y_anterior(self, tipo, orden_de_impresion):
        pass

    def orden_actual_y_anterior(self, tipo, orden_de_impresion):
        pass

    def generacion_hasta_link(self, tipo, orden_de_impresion):
        pass

    def fecha_generacion(self, tipo, orden_de_impresion):
        pass

    def impresion_actual_anterior(self, tipo, orden_de_impresion):
        pass

    def companias_vigentes(self, fecha):
        pass

    def cuotas_pendiente(self):
        pass

    def ultimo_cobro(self):
        pass


class Rubrica(models.Model):
    productor = models.ForeignKey(Productor)
    saldo = models.FloatField(default=0.0)

    def get_presupuesto(self):
        return 300

    def get_time_since_the_last_rubrica(self):
        last_book = self.get_last_operacion_book()
        today = datetime.date.today()
        y, m, d = self._get_time_past(last_book.fecha_hasta, today)
        return (y, m, d)

    def _get_time_past(self, before, current):
        days = current.toordinal() - before.toordinal()
        y, days = self._get_amount_years(days)
        m, days = self._get_amount_months(days)
        return y, m, days

    def _get_amount_years(self, days):
        d = days
        y = 0
        while d >= 360:
            y += 1
            d = d - 360
        return y, d

    def _get_amount_months(self, days):
        d = days
        m = 0
        while d >= 30:
            m += 1
            d = d - 30
        return m, d


class RubricaDigital(Rubrica):
    credits = models.IntegerField(default=0)

    def get_last_operacion_book(self):
        books = LibroDigital.objects.filter(rubrica=self, tipo=OPERACION)
        book = books.order_by('-fecha_hasta')[0] if books else None
        return book

    def get_last_cobranza_book(self):
        books = LibroDigital.objects.filter(rubrica=self, tipo=COBRANZA)
        book = books.order_by('-fecha_hasta')[0] if books else None
        return book

    def get_credits_available(self):
        subtotal = self.get_total_records()
        total = self.credits - subtotal
        return total

    def get_total_records(self):
        res = 0
        books = LibroDigital.objects.filter(rubrica=self)
        for book in books:
            res += book.amount_records
        return res


class RubricaImpreso(Rubrica):
    last_sheet = models.IntegerField(default=0)


class Libro(models.Model):
    rubrica = models.ForeignKey(Rubrica)
    fecha_generado = models.DateField(null=True, blank=True)
    fecha_hasta = models.DateField(null=True, blank=True)
    tipo = models.IntegerField(default=OPERACION)
    amount_records = models.IntegerField(default=0)


class LibroImreso(Libro):
    ultimo_libro_operaciones = models.PositiveIntegerField(default=1)
    ultimo_libro_cobranzas = models.PositiveIntegerField(default=1)


class LibroDigital(Libro):
    pass


class ProductorEnCompania(models.Model):
    productor = models.ForeignKey(Productor)
    compania = models.ForeignKey(Compania)

    def __str__(self):
        return u"%s en %s" % (self.productor, self.compania)

    def get_company_name(self):
        cia = self.compania
        return cia.get_nombre()

    def is_active_the_company(self):
        cia = self.compania
        return cia.active

    def get_clave(self):
        try:
            return Clave.objects.get(productor_en_compania=self)
        except Clave.DoesNotExist:
            return None

    def get_portal_web(self):
        cia = self.compania
        return cia.portal_web

    def get_portal_web_alternativo(self):
        cia = self.compania
        return cia.portal_web_alternativo

    def get_usuario_as_string(self):
        clave = self.get_clave()
        return clave.usuario if clave else ''

    def get_password_as_string(self):
        clave = self.get_clave()
        return clave.password if clave else ''

    def get_identificador_as_string(self):
        clave = self.get_clave()
        return clave.identificador if clave else ''

    class Meta:
        ordering = ['productor', 'compania']


class Clave(models.Model):
    fecha = models.DateField(default=timezone.now)
    user = models.ForeignKey(User, null=True, blank=True)
    productor_en_compania = models.OneToOneField(ProductorEnCompania)
    usuario = models.CharField(max_length=100)
    password = models.CharField(max_length=100, null=True, blank=True)
    identificador = models.CharField(max_length=100, null=True, blank=True)
    vigente = models.BooleanField(default=True)

    def __str__(self):
        return '-'.join((self.usuario, self.password, self.identificador))
