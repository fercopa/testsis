PROVINCIAS = {
    0: {'nombre': 'Ciudad Autónoma de Buenos Aires',
        'nombre_corto': 'CABA', 'abreviacion': 'CABA'},
    1: {'nombre': 'Buenos Aires', 'nombre_corto': 'BS. AS.',
        'abreviacion': 'PBA'},
    2: {'nombre': 'Catamarca', 'nombre_corto': 'CAT', 'abreviacion': 'CA'},
    3: {'nombre': 'Córdoba', 'nombre_corto': 'CBA', 'abreviacion': 'CB'},
    4: {'nombre': 'Corrientes', 'nombre_corto': 'CTE', 'abreviacion': 'CR'},
    5: {'nombre': 'Entre Ríos', 'nombre_corto': 'E. RÍOS',
        'abreviacion': 'ER'},
    6: {'nombre': 'Jujuy', 'nombre_corto': 'JUY', 'abreviacion': 'JY'},
    7: {'nombre': 'Mendoza', 'nombre_corto': 'MZA', 'abreviacion': 'MZ'},
    8: {'nombre': 'La Rioja', 'nombre_corto': 'L. RJA', 'abreviacion': 'LR'},
    9: {'nombre': 'Salta', 'nombre_corto': 'SAL', 'abreviacion': 'SA'},
    10: {'nombre': 'San Juan', 'nombre_corto': 'S. JUAN', 'abreviacion': 'SJ'},
    11: {'nombre': 'San Luis', 'nombre_corto': 'S. LUIS', 'abreviacion': 'SL'},
    12: {'nombre': 'Santa Fé', 'nombre_corto': 'STA. FE', 'abreviacion': 'SF'},
    13: {'nombre': 'Santiago del Estero', 'nombre_corto': 'SGO',
         'abreviacion': 'SE'},
    14: {'nombre': 'Tucumán', 'nombre_corto': 'TUC', 'abreviacion': 'TU'},
    16: {'nombre': 'Chaco', 'nombre_corto': 'CHA', 'abreviacion': 'CH'},
    17: {'nombre': 'Chubut', 'nombre_corto': 'CHUB', 'abreviacion': 'CT'},
    18: {'nombre': 'Formosa', 'nombre_corto': 'FOR', 'abreviacion': 'FO'},
    19: {'nombre': 'Misiones', 'nombre_corto': 'MIS', 'abreviacion': 'MI'},
    20: {'nombre': 'Neuquén', 'nombre_corto': 'NQN', 'abreviacion': 'NQ'},
    21: {'nombre': 'La Pampa', 'nombre_corto': 'L. PAM', 'abreviacion': 'LP'},
    22: {'nombre': 'Río Negro', 'nombre_corto': 'R. NEG', 'abreviacion': 'RN'},
    23: {'nombre': 'Santa Cruz', 'nombre_corto': 'S. CRUZ',
         'abreviacion': 'SC'},
    24: {'nombre': 'Tierra del Fuego', 'nombre_corto': 'T. FGO',
         'abreviacion': 'TF'},
}

OPERACION = 0
COBRANZA = 1
JURIDICA = 0
FISICA = 1
TIPO_PERSONA = ((FISICA, 'Física'), (JURIDICA, 'Jurídica'))
HOJAS_POR_LIBRO = 100
SSN_URL = "http://www.ssn.gob.ar/storage/registros/productores/"\
           + "productoresactivos.asp?"
