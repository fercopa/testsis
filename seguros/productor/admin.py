# # -*- coding: utf-8 -*-
# # pylint: disable-all
# import logging
# from datetime import datetime, timedelta
# from django.urls import reverse

from django.contrib import admin
from . import models
# from django.db import models
# from django.forms import TextInput, Textarea
# from django.utils.safestring import mark_safe
# from django.utils.html import format_html

# from nested_inline.admin import (NestedStackedInline, NestedTabularInline,
                                 # NestedModelAdmin)
# from suit.widgets import (AutosizedTextarea, SuitDateWidget, SuitTimeWidget,
                          # SuitSplitDateTimeWidget, LinkedSelect)

# from .models import Productor, ProductorEnCompania, Clave, Provincia, Localidad
# from trabajo.models import Trabajo, TrabajoDigital, TrabajoImpreso
# from .forms import (ProductoresForm, TerminadosInlineForm,
                    # TrabajoImpresoInlineForm, TrabajoDigitalInlineForm,
                    # CodigoInlineForm)

admin.site.register(models.Provincia)
admin.site.register(models.Localidad)
admin.site.register(models.Productor)
admin.site.register(models.Profile)

# class ClaveInlineAdmin(admin.TabularInline):
    # fields = ('usuario', 'password', 'identificador', 'vigente')
    # model = Clave
    # extra = 0
    # fk_name = 'productor_en_compania'


# class ClaveAdmin(admin.ModelAdmin):
    # fields = ('productor_en_compania', 'usuario', 'password', 'identificador',
              # 'vigente')
    # list_display = ('productor_en_compania', 'usuario', 'password',
                    # 'identificador', 'vigente')
    # search_fields = ('productor_en_compania__compania__nombre',
                     # 'productor_en_compania__productor__nombre',)
    # list_filter = ('vigente', 'fecha')


# class CodigoInlineAdmin(admin.TabularInline):
    # def portal_productores(self, pec):
        # buscar_portal = pec.get_portal_productores()
        # if buscar_portal:
            # url = buscar_portal
            # mensaje = 'Link'
        # else:
            # cia = pec.compania
            # url = "/admin/companias/compania/{0}/change/".format(cia.pk)
            # mensaje = 'Agregar compañía'
        # tag_a = '<a target="_blank" href="{0}">{1}</a>'.format(url, mensaje)
        # return mark_safe(tag_a)

    # cols = ('compania', 'portal_productores', 'clave', 'descarga',)
    # fieldsets = (
        # (None, {'fields': cols}),
    # )
    # readonly_fields = ['clave', 'descarga', 'portal_productores']
    # model = ProductorEnCompania
    # extra = 0
    # fk_name = 'productor'

    # formfield_overrides = {models.DateField: {'widget': SuitDateWidget}, }
    # suit_classes = 'suit-tab suit-tab-codigos'

    # def clave(self, pec):
        # clave_obj = pec.get_clave()
        # clave = clave_obj.show() if clave_obj else ''
        # nueva_url = reverse('admin:productor_clave_add')
        # actualizar_url = reverse('admin:productor_clave_change',
                                 # args=(clave_obj.pk, ))
        # nueva = """<a href="%s">Nueva clave</a>""" % nueva_url
        # actualizar = """<a href="%s">Actualizar</a>""" % actualizar_url
        # return mark_safe("{} {} {}".format(clave, nueva, actualizar))
    # clave.allow_tag = False

    # def descarga(self, obj):
        # if obj.id:
            # descarga = obj.descargada_hasta()
            # if descarga:
                # descarga = descarga.strftime('%Y/%m/%d')
                # return mark_safe("""%s <a target="_blank" href="/admin/trabajos/tarea/add/?ct_id=18&productor_en_compania=%d">Marcar como recibido</a>""" % (descarga, obj.id))
            # else:
                # descarga = ''
                # return mark_safe("""%s <a target="_blank" href="/admin/trabajos/tarea/add/?ct_id=18&productor_en_compania=%d">Marcar como recibido</a>""" % (descarga, obj.id))
        # else:
            # return mark_safe("-")
    # descarga.short_description = 'Archivos recibidos/descargados'

    # def recupera(self, obj):
        # if obj.id:
            # descarga = ""
            # return mark_safe("""%s <a target="_blank" href="/admin/trabajos/tarea/add/?ct_id=19&productor_en_compania=%d">Recupera</a>""" % (descarga, obj.id))
        # else:
            # return mark_safe("")


# class TrabajoInlineAdmin(admin.TabularInline):
    # model = Trabajo

    # def has_add_permission(self, request):
        # return False


# class TrabajoDigitalInlineAdmin(TrabajoInlineAdmin):
    # form = TrabajoDigitalInlineForm
    # fields = ['fecha', 'user',]
    # model = TrabajoDigital

    # class Media(object):
        # css = {'all': ('no-more-warnings.css', )}


    # # readonly_fields = ['importe_recupero', 'importe_contenidos',
    # #                   'importe_timbrado', 'total', 'cobrado',
    # #                   'cobrar', 'saldo']
    # extra = 0
    # formfield_overrides = {
        # models.DateField: {'widget': SuitDateWidget},
        # models.TextField: {'widget': admin.widgets.AdminTextInputWidget},
    # }
    # verbose_name_plural = "Trabajos Pendientes Digitaales"

    # def get_queryset(self, request):
        # qs = super(TrabajoDigitalInlineAdmin, self).get_queryset(request)
        # qs = qs.filter(terminado=False)
        # return qs
    # suit_classes = 'suit-tab suit-tab-pendientes-digitales'

    # def cobrar(self, obj):
        # if obj.id:
            # return mark_safe("""<a target="_blank" href="/admin/trabajos/cobro/add/?trabajo=%d">Cobrar</a>""" % obj.id)
        # else:
            # return mark_safe("")

    # def has_add_permission(self, request):
        # return False


# class TrabajoImpresoInlineAdmin(TrabajoInlineAdmin):
    # form = TrabajoImpresoInlineForm
    # fields = ['fecha', 'importe_recupero',
              # 'importe_encabezados', 'importe_contenidos', 'importe_timbrado',
              # 'total', 'cobrado', 'cobrar', 'saldo', 'pendientes']
    # model = TrabajoImpreso

    # class Media(object):
        # css = {'all': ('no-more-warnings.css', )}


    # readonly_fields = ['importe_recupero', 'importe_encabezados',
                       # 'importe_contenidos', 'importe_timbrado', 'total',
                       # 'cobrado', 'cobrar', 'saldo', 'pendientes']
    # extra = 0
    # logging.info(dir(admin.widgets))
    # formfield_overrides = {
        # models.DateField: {'widget': SuitDateWidget},
        # models.TextField: {'widget': admin.widgets.AdminTextInputWidget},
    # }

    # verbose_name_plural = "Trabajos Pendientes Impresos"

    # def get_queryset(self, request):
        # qs = super(TrabajoImpresoInlineAdmin, self).get_queryset(request)
        # qs = qs.filter(terminado=False)
        # return qs

    # suit_classes = 'suit-tab suit-tab-pendientes-impresos'

    # def cobrar(self, obj):
        # if obj.id and obj.saldo() > 0:
            # url = """<a target="_blank" href="/admin/trabajos/cobro/add/?trabajo=%d">Cobrar</a>""" % obj.id
            # return mark_safe(url)
        # else:
            # return mark_safe("")

    # def pendientes(self, obj):
        # if obj.id:
            # return mark_safe(obj.pendiente_mejorado())
        # else:
            # return mark_safe("")

    # def has_add_permission(self, request):
        # return False


# class TerminadosInlineAdmin(TrabajoInlineAdmin):
    # form = TerminadosInlineForm
    # # fields = ['fecha', ]

    # verbose_name_plural = "Trabajos Terminados"

    # class Media(object):
        # css = {'all': ('no-more-warnings.css', )}

    # def get_queryset(self, request):
        # qs = super(TrabajoInlineAdmin, self).get_queryset(request)
        # qs = qs.filter(terminado=True)
        # return qs
    # suit_classes = 'suit-tab suit-tab-terminados'

    # def has_add_permission(self, request):
        # return False


# class ProductorAdmin(admin.ModelAdmin):
    # model = Productor
    # form = ProductoresForm

    # class Media(object):
        # css = {'all': ('no-more-warnings.css', )}

    # """def hojas_disponibles_operaciones(self, obj=None):
        # return obj.hojas_disponibles_o()

    # def hojas_disponibles_cobranzas(self, obj=None):
        # return obj.hojas_disponibles_c()
    # """

    # # Fecha Generacion
    # def fecha_generacion(self, obj, tipo, posic):
        # fecha_gene_ope = obj.fecha_generacion(tipo, posic)
        # if len(fecha_gene_ope) <= 2:
            # fecha_gene_ope = 'Sin fecha'
        # return fecha_gene_ope

    # def fecha_generacion_op_actual(self, obj):
        # fecha_gene_ope_actual = self.fecha_generacion(obj, 'O', 0)
        # return fecha_gene_ope_actual

    # def fecha_generacion_op_anterior(self, obj):
        # fecha_gene_ope_anterior = self.fecha_generacion(obj, 'O', 1)
        # return fecha_gene_ope_anterior

    # def fecha_generacion_co_actual(self, obj):
        # fecha_gene_cob_actual = self.fecha_generacion(obj, 'C', 0)
        # return fecha_gene_cob_actual

    # def fecha_generacion_co_anterior(self, obj):
        # fecha_gene_cob_anterior = self.fecha_generacion(obj, 'C', 1)
        # return fecha_gene_cob_anterior

    # # Hasta
    # def generacion_hasta(self, obj, tipo, posic):
        # fecha_gen_hast_oper = obj.generacion_hasta_link(tipo, posic)

        # fecha = fecha_gen_hast_oper.get('fecha')
        # id = fecha_gen_hast_oper.get('id')

        # if not fecha:
            # fecha = 'Sin fecha'
        # return mark_safe("""<a target='_blank' href='{0}'>{1}</a>""".format('/admin/trabajos/tarea/'+str(id)+'/change/', fecha))

    # def hasta_generacion_op_actual(self, obj):
        # hasta_generacion_actual = self.generacion_hasta(obj, 'O', 0)
        # return hasta_generacion_actual

    # def hasta_generacion_co_actual(self, obj):
        # hasta_generacion_actual = self.generacion_hasta(obj, 'C', 0)
        # return hasta_generacion_actual

    # def hasta_generacion_op_anterior(self, obj):
        # hasta_generacion_actual = self.generacion_hasta(obj, 'O', 1)
        # return hasta_generacion_actual

    # def hasta_generacion_co_anterior(self, obj):
        # hasta_generacion_actual = self.generacion_hasta(obj, 'C', 1)
        # return hasta_generacion_actual

    # # Libro
    # def libro_en_uso(self, obj, tipo, posic):
        # return obj.buscar_libro_actual_y_anterior(tipo, posic)

    # def libro_en_uso_op_actual(self, obj):
        # return self.libro_en_uso(obj, 'O', 0)

    # def libro_en_uso_co_actual(self, obj):
        # return self.libro_en_uso(obj, 'C', 0)

    # def libro_en_uso_op_anterior(self, obj):
        # return self.libro_en_uso(obj, 'O', 1)

    # def libro_en_uso_co_anterior(self, obj):
        # return self.libro_en_uso(obj, 'C', 1)

    # # Hoja
    # def ultima_hoja(self, obj, tipo, posic):
        # return obj.hoja_actual_y_anterior(tipo, posic)

    # def ultima_hoja_op_actual(self, obj):
        # return self.ultima_hoja(obj, 'O', 0)

    # def ultima_hoja_co_actual(self, obj):
        # return self.ultima_hoja(obj, 'C', 0)

    # def ultima_hoja_op_anterior(self, obj):
        # return self.ultima_hoja(obj, 'O', 1)

    # def ultima_hoja_co_anterior(self, obj):
        # return self.ultima_hoja(obj, 'C', 1)

    # # Orden
    # def ultimo_orden(self, obj, tipo, posic):
        # return obj.orden_actual_y_anterior(tipo, posic)

    # def orden_op_actual(self, obj):
        # return self.ultimo_orden(obj, 'O', 0)

    # def orden_co_actual(self, obj):
        # return self.ultimo_orden(obj, 'C', 0)

    # def orden_op_anterior(self, obj):
        # return self.ultimo_orden(obj, 'O', 1)

    # def orden_co_anterior(self, obj):
        # return self.ultimo_orden(obj, 'C', 1)

    # # Stock Hojas
    # def stock_hojas(self, obj, tipo, posic):
        # return obj.en_stock_actual_anterior(tipo, posic)

    # def stock_op_actual(self, obj):
        # return self.stock_hojas(obj, 'O', 0)

    # def stock_co_actual(self, obj):
        # return self.stock_hojas(obj, 'C', 0)

    # def stock_op_anterior(self, obj):
        # return self.stock_hojas(obj, 'O', 1)

    # def stock_co_anterior(self, obj):
        # return self.stock_hojas(obj, 'C', 1)

    # def crear_nueva_impresion(self, obj):
        # if obj.id:
            # return mark_safe("""<a href="/productores/trabajo/{0}/">Crear nueva Impresion</a>""".format(obj.id))
        # else:
            # return mark_safe("")
    # crear_nueva_impresion.allow_tag = False

    # def nuevo_trabajo(self, obj):
        # if obj.id:
            # if obj.libros_digitales:
                # url = """<a target="_blank" href="/admin/trabajos/trabajo/add/?ct_id=15&productor={0}">Crear trabajo digital</a>""".format(obj.id)
            # else:
                # url = """<a target="_blank" href="/admin/trabajos/trabajo/add/?ct_id=16&productor={0}">Crear trabajo impreso</a>""".format(obj.id)
            # return mark_safe(url)
        # else:
            # return mark_safe("")

    # def trabajos_abiertos(self, obj):
        # if obj.id:
            # url = """<a target="_blank" href="/admin/trabajos/trabajo/?terminado__exact=0&productor__id__exact={0}">Ver Listado</a>""".format(obj.id)
            # return mark_safe(url)
        # else:
            # return mark_safe("")

    # fecha_generacion_co_actual.short_description = 'Cob. Fecha Generado'  # "CyR. fecha Imp."
    # fecha_generacion_co_anterior.short_description = 'Cob. Fecha Generado'  # "CyR. fecha Imp."
    # fecha_generacion_op_actual.short_description = 'Ope. Fecha Generado'  # "Ope. fecha de Imp."
    # fecha_generacion_op_anterior.short_description = 'Ope. Fecha Generado'  # "Ope. fecha de Imp."

    # hasta_generacion_op_actual.short_description = "Hasta"
    # hasta_generacion_co_actual.short_description = "Hasta"
    # hasta_generacion_op_anterior.short_description = "Hasta"
    # hasta_generacion_co_anterior.short_description = "Hasta"

    # libro_en_uso_op_actual.short_description = "Libro"
    # libro_en_uso_co_actual.short_description = "Libro"
    # libro_en_uso_op_anterior.short_description = "Libro"
    # libro_en_uso_co_anterior.short_description = "Libro"

    # ultima_hoja_op_actual.short_description = "Hoja"
    # ultima_hoja_co_actual.short_description = "Hoja"
    # ultima_hoja_op_anterior.short_description = "Hoja"
    # ultima_hoja_co_anterior.short_description = "Hoja"

    # orden_op_actual.short_description = "Orden"
    # orden_co_actual.short_description = "Orden"
    # orden_op_anterior.short_description = "Orden"
    # orden_co_anterior.short_description = "Orden"

    # stock_op_actual.short_description = "Stock"
    # stock_co_actual.short_description = "Stock"

    # crear_nueva_impresion.short_description = "Operaciones y Cobranzas"
    # nuevo_trabajo.short_description = "Nuevo Impreso"


    # def ver_ocultar_campo(self, field):
        # model = self.model()

        # if model.libros_digitales:
            # if field == 'articulo_19':
                # campo = field
            # else:
                # campo = "{0}_hidden".format(field)
        # else:
            # if field == 'articulo_19':
                # campo = "{0}_hidden".format(field)
            # else:
                # campo = field
        # return campo

    # def get_fieldsets(self, request, obj=None):
        # self.exclude = []

        # fieldsets = (
            # ('Principal', {
                # 'classes': ('suit-tab', 'suit-tab-general',),
                # 'fields': (
                    # 'nombre', ('tipo_persona'),
                    # # ('preferencial', 'libros_digitales'),
                    # # ('companias_actualizadas_al_dia', 'encargado'),
                    # ('matricula', 'show_link_al_ssn', self.ver_ocultar_campo('articulo_19')),
                    # #(self.ver_ocultar_campo('ultimo_libro_operaciones'), self.ver_ocultar_campo('ultimo_libro_cobranzas')),
                    # # ('provincia'), ('localidad'),
                # ),
            # }),
        # )

        # if True and obj:
            # if obj.libros_digitales:
                # fieldsets += (('Datos de productor - DIGITAL', {
                    # 'classes': ('suit-tab', 'suit-tab-general',),
                    # 'fields': (
                        # # ('mensual_digitales'),
                        # # 'cuit', 'clave_fiscal',
                        # # 'creditos_disponibles',
                        # ),
                    # }),
                             # )

                # fieldsets += (('Ultima Generación', {
                    # 'classes': ('suit-tab', 'suit-tab-general',),
                    # 'fields': (
                        # ('fecha_generacion_op_actual',
                         # 'hasta_generacion_op_actual',
                        # ),
                        # #
                        # ('fecha_generacion_co_actual',
                         # 'hasta_generacion_co_actual',
                        # ),
                        # ('crear_nueva_impresion')
                        # )
                    # }),
                             # )

            # else:
                # fieldsets += (('Ultimos Libros', {
                    # 'classes': ('suit-tab', 'suit-tab-general',),
                    # 'fields': (
                        # (self.ver_ocultar_campo('ultimo_libro_operaciones'),),
                        # (self.ver_ocultar_campo('ultimo_libro_cobranzas'),),
                        # ('crear_nueva_impresion')
                        # ),
                    # }),
                             # )

                # fieldsets += (('Impresión Actual', {
                    # 'classes': ('suit-tab', 'suit-tab-general',),
                    # 'fields': (
                        # ('fecha_generacion_op_actual',
                         # 'hasta_generacion_op_actual',
                         # 'libro_en_uso_op_actual',
                         # 'ultima_hoja_op_actual',
                         # 'orden_op_actual',
                         # 'stock_op_actual',),
                        # #
                        # ('fecha_generacion_co_actual',
                         # 'hasta_generacion_co_actual',
                         # 'libro_en_uso_co_actual',
                         # 'ultima_hoja_co_actual',
                         # 'orden_co_actual',
                         # 'stock_co_actual'),
                        # ),
                    # }),
                             # )

                # fieldsets += (('Impresión Anterior', {
                    # 'classes': ('suit-tab', 'suit-tab-general',),
                    # 'fields': (
                        # ('fecha_generacion_op_anterior',
                         # 'hasta_generacion_op_anterior',
                         # 'libro_en_uso_op_anterior',
                         # 'ultima_hoja_op_anterior',
                         # 'orden_op_anterior',
                        # ),
                        # #
                        # ('fecha_generacion_co_anterior',
                         # 'hasta_generacion_co_anterior',
                         # 'libro_en_uso_co_anterior',
                         # 'ultima_hoja_co_anterior',
                         # 'orden_co_anterior',
                        # ),
                        # ),
                    # }),
                             # )

        # #except:
            # #Si ya esta creado le agrego los campos correspondientes
            # #sino, solo se rellena con los datos principales
        # #    pass
        # fieldsets += (
            # ('Datos personales', {
                # 'classes': ('collapse', 'suit-tab', 'suit-tab-general',),
                # 'fields': (
                    # # ('telefono', 'email'), 'observaciones', 'como_se_entrega', 'direccion',
                    # ),
            # }),
        # )

        # fieldsets += (
            # ('Posee Licencia', {
                # 'classes': ('suit-tab', 'suit-tab-general',),
                # 'fields': (
                    # # ('posee_licencia'),
                    # ),
            # }),
        # )

        # try:
            # if obj.posee_licencia:
                # fieldsets += (
                    # ('Datos de la Licencia', {
                        # 'classes': ('collapse', 'suit-tab', 'suit-tab-general',),
                        # 'fields': (
                            # # ('licencia', 'licencias_extra'),
                            # # ('libros_digitales', 'licencia_vencimiento'),
                            # ),
                    # }),
                # )
            # else:
                # pass
        # except:
            # fieldsets += (
                # ('Datos de la Licencia', {
                    # 'classes': ('collapse', 'suit-tab', 'suit-tab-general',),
                    # 'fields': (
                        # ('licencia', 'licencias_extra'),
                        # # ('licencia_preferencial', 'licencia_personalizada'),
                        # # ('libros_digitales', 'licencia_vencimiento'),
                        # ),
                # }),
            # )

        # fieldsets += (
                # ('Trabajos', {
                    # 'classes': ('suit-tab', 'suit-tab-general',),
                    # 'fields': (
                        # ('nuevo_trabajo', 'trabajos_abiertos'),
                        # ),
                # }),
            # )

        # return fieldsets

    # readonly_fields = ('hasta_generacion_op_actual',
                       # 'hasta_generacion_co_actual',
                       # 'hasta_generacion_op_anterior',
                       # 'hasta_generacion_co_anterior',
                       # 'fecha_generacion_op_actual',
                       # 'fecha_generacion_op_anterior',
                       # 'fecha_generacion_co_actual',
                       # 'fecha_generacion_co_anterior',
                       # 'libro_en_uso_op_actual',
                       # 'libro_en_uso_co_actual',
                       # 'libro_en_uso_op_anterior',
                       # 'libro_en_uso_co_anterior',
                       # 'ultima_hoja_op_actual',
                       # 'ultima_hoja_co_actual',
                       # 'ultima_hoja_op_anterior',
                       # 'ultima_hoja_co_anterior',
                       # 'orden_op_actual',
                       # 'orden_co_actual',
                       # 'orden_op_anterior',
                       # 'orden_co_anterior',
                       # 'stock_op_actual',
                       # 'stock_co_actual',
                       # 'show_link_al_ssn',
                       # 'crear_nueva_impresion',
                       # 'nuevo_trabajo',
                       # 'trabajos_abiertos',
                       # 'cuota_pendiente',
                      # )

    # # list_display = ('nombre', 'matricula', )
    # search_fields = ('nombre', 'matricula')
    # # list_filter = ('libros_digitales', 'mensual_digitales', 'preferencial', 'matricula')
    # inlines = [CodigoInlineAdmin, TrabajoImpresoInlineAdmin,
               # TrabajoDigitalInlineAdmin, TerminadosInlineAdmin, ]
    # formfield_overrides = {
        # models.TextField: {'widget': AutosizedTextarea(attrs={'rows': 3, 'cols': 300})},
    # }

    # suit_form_tabs = (('general', 'General'),
                      # ('codigos', 'Compañías'),
                      # ('pendientes-impresos', 'Trabajos Pendientes Impresos'),
                      # ('pendientes-digitales', 'Trabajos Pendientes Digitales'),
                      # ('terminados', 'Trabajos Terminados')
                     # )

    # def cuota_pendiente(self, obj):
        # if not obj.cuotas_pendiente() or obj.cuotas_pendiente()<=0:
            # return '-'
        # else:
            # return obj.cuotas_pendiente()
    # cuota_pendiente.short_description = 'Cuotas pendiente'

    # def show_link_al_ssn(self, obj):
        # if obj.tipo_persona == 'J':
            # url = "http://ssn.gov.ar/storage/registros/productores/productoresactivosfiltro.asp"
            # mensaje = 'Sociedades'
        # else:
            # url = obj.get_link_ssn()
            # if url:
                # mensaje = obj.matricula
            # else:
                # mensaje = 'Indique una matricula para el productor'
        # return mark_safe("""<a target='_blank' href='{0}'>{1}</a>""".format(url, mensaje))

    # show_link_al_ssn.allow_tags = True
    # show_link_al_ssn.short_description = "Link al SSN"


# class ProvinciaAdmin(admin.ModelAdmin):
    # model = Provincia


# class LocalidadAdmin(admin.ModelAdmin):
    # model = Localidad


# '''class LicenciaAdmin(admin.ModelAdmin):
    # model = Licencia'''

# admin.site.register(Productor, ProductorAdmin)
# admin.site.register(Clave, ClaveAdmin)
# admin.site.register(Provincia, ProvinciaAdmin)
# admin.site.register(Localidad, LocalidadAdmin)
# #admin.site.register(Licencia, LicenciaAdmin)
