from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


class ListaDePrecios(models.Model):
    fecha = models.DateTimeField('Fecha de los precios', default=timezone.now)
    hoja_operaciones = models.IntegerField(default=0,
                                           help_text="Solo Trabajos Impresos")
    hoja_cobranzas = models.IntegerField(default=0,
                                         help_text="Solo Trabajos Impresos")
    encabezados = models.IntegerField(default=0,
                                      help_text="Solo Trabajos Impresos")
    # precios digitales
    creditos = models.IntegerField(default=0,
                                   help_text="Solo Trabajos Digitales")
    # precios comunes a las 2 clases
    contenidos = models.IntegerField(default=0,
                                     help_text="Para ambos tipos de trabajos")
    timbrado = models.IntegerField(default=0,
                                   help_text="Para ambos tipos de trabajos")
    recupero = models.FloatField(default=0.0,
                                 help_text="Para ambos tipos de trabajo")

    def __str__(self):
        return ("Precios al dia %s" % self.fecha.strftime("%d/%m/%Y"))

    class Meta:
        ordering = ['-fecha']


class Observacion(models.Model):
    fecha = models.DateTimeField(default=timezone.now)
    detalle = models.TextField()

    def __str__(self):
        return self.fecha


class Trabajo(models.Model):
    user = models.ForeignKey(User, null=True, blank=True)
    productor = models.ForeignKey('productor.Productor')
    observacion = models.ForeignKey(Observacion, null=True, blank=True)
    fecha = models.DateTimeField('Fecha solicitud', default=timezone.now)
    terminado = models.BooleanField(default=False)
    cobrado = models.FloatField(default=0)
    descuento = models.FloatField(default=0)
    fecha_desde = models.DateField()
    fecha_hasta = models.DateField()

    def save(self, *args, **kwargs):
        if self.fecha_desde < self.fecha_hasta:
            super(Trabajo, self).save(*args, **kwargs)
        else:
            return

    def start_work(self):
        if not self.terminado:
            self.create_descarga_tasks()
            Presupuesto.objects.create(trabajo=self)
            Cobro.objects.create(trabajo=self)

    def proponer(self):
        if not self.exist_propuesta():
            Propuesta.objects.create(trabajo=self)

    def exist_propuesta(self):
        propuestas = Propuesta.objects.filter(trabajo=self)
        return True if propuestas else False

    def accept_propuesta(self):
        propuesta = self.get_active_propuesta()
        propuesta.accept()

    def get_finalized_propuesta(self):
        return Propuesta.objects.get(trabajo=self, finalized=True)

    def get_active_propuesta(self):
        return Propuesta.objects.get(trabajo=self, finalized=False)

    def create_descarga_tasks(self):
        if not self.terminado:
            productor = self.productor
            pecs = productor.get_active_companies()
            for pec in pecs:
                Descarga.objects.create(productor_en_compania=pec,
                                        trabajo=self)

    def get_amount_active_companies(self):
        productor = self.productor
        cias = productor.get_active_companies()
        return len(cias)

    def get_amount_descargas(self):
        descargas = self.get_descarga_tasks()
        return len(descargas)

    def get_descarga_tasks(self):
        descargas = Descarga.objects.filter(trabajo=self)
        return descargas

    def get_amount_cobros(self):
        cobros = self.get_cobros()
        return len(cobros)

    def get_cobros(self):
        cobros = Cobro.objects.filter(trabajo=self)
        return cobros

    def finalizar(self):
        if self.is_complete():
            self.terminado = True
            self.save()

    def is_complete(self):
        tasks = self.get_active_tasks()
        return True if len(tasks) == 0 else False

    def get_active_tasks(self):
        tasks = Tarea.objects.filter(trabajo=self, finalized=False)
        return tasks

    def saldo_pendiente(self):
        pass

    def saldo(self):
        pass

    def calcular_cobrado(self):
        pass

    def recuperos_pendientes(self):
        pass

    def generacion_pendiente(self, tipo):
        """ tipo = 'O' o 'C'
        'O' para Operaciones
        'C' para Cobranzas
        """
        pass

    def tareas_pendientes(self):
        pass

    def descargas_pendientes(self, solo_descargables=False):
        pass

    def presupuesto_pendientes(self):
        pass

    def companias_faltantes(self):
        pass

    def reclamar(self, dias=0):
        pass

    def reclamo_pendiente(self):
        pass

    def archivos_faltantes(self):
        pass

    def claves_faltantes(self):
        pass

    def estimacion_pendiente(self):
        pass

    def listadeprecios_relacionada(self):
        pass

    def cantidad_companias_relacionadas_para_descargar(self):
        pass

    def proxima_tarea(self):
        pass

    def pendiente_mejorado(self):
        pass


class TrabajoDigital(Trabajo):
    cantidad_operaciones = models.IntegerField(default=0)
    cantidad_cobranzas = models.IntegerField(default=0)
    importe_recupero = models.FloatField(default=0.0)
    importe_contenidos = models.FloatField(default=0.0)
    importe_timbrado = models.FloatField(default=0.0)

    def cobrar(self):
        pass

    def operaciones_por_generar(self):
        pass

    def cobranzas_por_generar(self):
        pass

    def recursos_disponibles(self, tipo):
        pass

    def cia_descargable(self, cia):
        pass

    def cias_descargables(self, cias):
        pass

    def crear_estimacion(self):
        pass

    def total(self):
        pass

    def calcular_presupuesto(self):
        pass


class TrabajoImpreso(Trabajo):
    cantidad_hojas_operaciones = models.IntegerField(default=0)
    cantidad_hojas_cobranzas = models.IntegerField(default=0)
    importe_recupero = models.FloatField(default=0.0)
    importe_encabezados = models.FloatField(default=0.0)
    importe_contenidos = models.FloatField(default=0.0)
    importe_timbrado = models.FloatField(default=0.0)

    def operaciones_por_generar(self):
        pass

    def cobranzas_por_generar(self):
        pass

    def recursos_disponibles(self, tipo):
        pass

    def cia_descargable(self, cia):
        pass

    def crear_estimacion(self):
        pass

    def cias_descargables(self, cias):
        pass

    def total(self):
        pass

    def calcular_presupuesto(self):
        pass


class Tarea(models.Model):
    fecha = models.DateTimeField(default=timezone.now, editable=False)
    user = models.ForeignKey(User, null=True, blank=True, editable=False)
    trabajo = models.ForeignKey(Trabajo)
    finalized = models.BooleanField(default=False)
    postergar_dias = models.IntegerField(default=0)

    class Meta:
        ordering = ['fecha']


class Descarga(Tarea):
    productor_en_compania = models.ForeignKey('productor.ProductorEnCompania')

    def __str__(self):
        pec = self.productor_en_compania
        return "Descarga de %s" % pec.get_company_name()

    def get_company_name(self):
        pec = self.productor_en_compania
        return pec.get_company_name()

    def finalizar(self):
        Recupero.objects.create(descarga=self)
        self.finalized = True
        self.save()


class Propuesta(Tarea):
    accepted = models.BooleanField(default=False)

    def accept(self):
        work = self.trabajo
        work.start_work()
        self.accepted = True
        self.finalized = True
        self.save()

    def reject(self):
        work = self.trabajo
        work.finalizar()
        self.accepted = False
        self.finalized = True
        self.save()


class Reclamo(Tarea):
    pass


class Recupero(models.Model):
    fecha = models.DateTimeField(default=timezone.now, editable=False)
    descarga = models.OneToOneField(Descarga)
    user = models.ForeignKey(User, null=True, blank=True, editable=False)
    finalized = models.BooleanField(default=False)

    def __str__(self):
        descarga = self.descarga
        cia = descarga.get_company_name()
        return "Recupero de %s" % cia


class Presupuesto(Tarea):
    pass


class Control(Tarea):
    pass


class SubidaLD(Tarea):
    pass


class RetiroPDF(Tarea):
    pass


class EnvioPDF(Tarea):
    pass


class Cobro(models.Model):
    fecha = models.DateTimeField(default=timezone.now)
    user = models.ForeignKey(User, null=True, blank=True, editable=False)
    trabajo = models.ForeignKey(Trabajo)
    importe = models.FloatField(default=0)
    comprobante = models.CharField(max_length=20, blank=True)


class EstimacionImpresos(Tarea):
    cantidad_hojas_operaciones = models.IntegerField(default=0)
    cantidad_hojas_cobranzas = models.IntegerField(default=0)

    def intentos_anteriores(self):
        pass


class EstimacionDigitales(Tarea):
    cantidad_operaciones = models.IntegerField(default=0)
    cantidad_cobranzas = models.IntegerField(default=0)

    def intentos_anteriores(self):
        pass


class GeneracionImpresos(Tarea):
    tipo = models.CharField(max_length=1,
                            choices=(('O', 'Operaciones'),
                                     ('C', 'Cobranzas'))
                            )
    hasta = models.DateField(null=True, blank=True)
    ultimo_libro = models.IntegerField(null=True, blank=True)
    ultima_hoja = models.IntegerField(null=True, blank=True)
    ultimo_nro_orden = models.IntegerField(null=True,
                                           blank=True,
                                           help_text="Sólo para operaciones")

    def __str__(self):
        return u'{0}'.format(self.trabajo)

    class Meta:
        ordering = ['-hasta']

    def intentos_anteriores(self):
        pass


class GeneracionDigitales(Tarea):
    tipo = models.CharField(max_length=1,
                            choices=(('O', 'Operaciones'),
                                     ('C', 'Cobranzas'))
                            )
    hasta = models.DateField(null=True, blank=True)
    cantidad_renglones = models.IntegerField(null=True, blank=True)

    class Meta:
        ordering = ['-hasta']

    def intentos_anteriores(self):
        pass


class PresupuestoDigitales(Tarea):
    cantidad_operaciones = models.IntegerField(null=True, blank=True)
    cantidad_cobranzas = models.IntegerField(null=True, blank=True)
    importe_recupero = models.FloatField(null=True, blank=True)
    importe_contenidos = models.FloatField(null=True, blank=True)
    importe_timbrado = models.FloatField(null=True, blank=True)
    descuento = models.FloatField(null=True, blank=True)

    def intentos_anteriores(self):
        pass

    class Meta:
        verbose_name = 'Presupuesto Digital'
        verbose_name_plural = 'Presupuestos Digitales'


class PresupuestoImpresos(Tarea):
    cantidad_hojas_operaciones = models.IntegerField(null=True, blank=True)
    cantidad_hojas_cobranzas = models.IntegerField(null=True, blank=True)
    importe_recupero = models.FloatField(null=True, blank=True)
    importe_encabezados = models.FloatField(null=True, blank=True)
    importe_contenidos = models.FloatField(null=True, blank=True)
    importe_timbrado = models.FloatField(null=True, blank=True)
    descuento = models.FloatField(null=True, blank=True)

    def intentos_anteriores(self):
        pass

    class Meta:
        verbose_name = 'Presupuesto Impreso'
        verbose_name_plural = 'Presupuestos Impresos'
