# # -*- coding: utf-8 -*-
# # pylint: disable-all

# import datetime
# import pytz

# # django imports
# from django.utils import timezone
# from django.utils.dateparse import parse_datetime
# from django.test import TestCase
# from django.core.exceptions import ValidationError

# # projects imports
# # TIEMPO_POSTERGACION,
# from .models import Trabajo, Tarea, GeneracionImpresos, GeneracionDigitales,  TrabajoDigital, TrabajoImpreso, ListaDePrecios
# from productor.models import Productor
# from compania.models import Compania


# def tz_parse(date):
    # p = datetime.datetime.strptime(date, "%Y-%m-%d")
    # tz = pytz.timezone("UTC")
    # return tz.localize(p)


# """class TareaModelTest(TestCase):

    # def test_status(self):
        # t = Tarea(status="ERR")
        # with self.assertRaises(ValidationError):
            # # debe tener observaciones
            # t.full_clean()
        # t = Tarea(status="POS")
        # with self.assertRaises(ValidationError):
            # # debe tener observaciones
            # t.full_clean()
# """

# class TrabajoModelTest(TestCase):

    # def setUp(self):
        # self.p1 = Productor.objects.create(nombre="a", preferencial=True, matricula="1")
        # self.p2 = Productor.objects.create(nombre="b", matricula="2")
        # self.p3 = Productor.objects.create(nombre="c", matricula="3")

    # def test_inspeccion(self):
        # t = self.p1.trabajos.create(con_inspeccion=True)
        # with self.assertRaises(ValidationError):
            # t.full_clean()

# # Test para trabajos en general (no importa si son impresos o digitales)
    # def test_orden_de_trabajos(self):
        # t3 = Trabajo.objects.create(productor_id=self.p3.id)
        # t2 = Trabajo.objects.create(productor_id=self.p3.id, con_inspeccion=True, fecha_inspeccion=datetime.date.today())
        # t1 = Trabajo.objects.create(productor_id=self.p3.id, con_inspeccion=True, fecha_inspeccion=datetime.date.today() - datetime.timedelta(days=1))
        # # prioridad a los que tienen inspeccion
        # self.assertEqual(Trabajo.objects.all()[0], t1)
        # self.assertEqual(Trabajo.objects.all()[1], t2)
        # self.assertEqual(Trabajo.objects.all()[2], t3)

    # def test_orden_de_trabajos2(self):
        # f = timezone.now()
        # t1 = Trabajo.objects.create(productor_id=self.p2.id, con_inspeccion=True, fecha_inspeccion=f)
        # t2 = Trabajo.objects.create(productor_id=self.p1.id, con_inspeccion=True, fecha_inspeccion=f)
        # t3 = Trabajo.objects.create(productor_id=self.p2.id)
        # t4 = Trabajo.objects.create(productor_id=self.p1.id)
        # # prioridad a los preferenciales
        # self.assertEqual(Trabajo.objects.all()[0], t2)
        # self.assertEqual(Trabajo.objects.all()[1], t1)
        # self.assertEqual(Trabajo.objects.all()[2], t4)
        # self.assertEqual(Trabajo.objects.all()[3], t3)

    # def test_orden_de_trabajos3(self):
        # f = timezone.now()
        # t1 = self.p1.trabajos.create()
        # t1.cobros.create(importe=22)
        # t2 = self.p1.trabajos.create()
        # t2.cobros.create(importe=44)
        # t3 = Trabajo.objects.create(productor_id=self.p2.id, cobrado=0, fecha=timezone.now())
        # t4 = Trabajo.objects.create(productor_id=self.p2.id, cobrado=0, fecha=timezone.now() - datetime.timedelta(days=1))
        # # prioridad a los que pagaron, y sino por fecha de pedido
        # self.assertEqual(Trabajo.objects.all()[0], t2)
        # self.assertEqual(Trabajo.objects.all()[1], t1)
        # self.assertEqual(Trabajo.objects.all()[2], t4)
        # self.assertEqual(Trabajo.objects.all()[3], t3)

    # #Revisar cuando implemente el calculo de presupuesto para los digitales
    # def test_flow_completo_modified(self):
        # precios = ListaDePrecios.objects.create(fecha=tz_parse("2015-11-01"), hoja_operaciones=10, hoja_cobranzas=10, encabezados=11, contenidos=10, timbrado=10, recupero=10)
        # precios.save()
        # prod = Productor.objects.create(nombre="Productores Asociados", tipo_persona="J", libros_digitales=True)
        # prod.save()
        # t = TrabajoDigital.objects.create(productor=prod, fecha=tz_parse("2015-12-01"))
        # t.save()

        # # faltan las companias
        # #self.assertEqual(2, prod.ultimo_libro_cobranzas)
        # self.assertTrue(t.companias_faltantes())
        # # no puedo presupuestar
        # # self.assertEqual(t.presupuesto_pendientes(), None)

        # # agregar companias
        # c1 = Compania.objects.create(nombre="A", descargable_digital=True)
        # c2 = Compania.objects.create(nombre="B", descargable_digital=True)
        # c3 = Compania.objects.create(nombre="C", descargable_digital=False)
        # c4 = Compania.objects.create(nombre="D", descargable_digital=True)
        # cod1 = prod.companias.create(compania=c1)
        # cod2 = prod.companias.create(compania=c2, desde="2016-01-01")
        # cod3 = prod.companias.create(compania=c3, hasta="2016-01-01")
        # cod4 = prod.companias.create(compania=c4, desde="2016-01-01", hasta="2016-01-31")

        # # no faltan las companias
        # self.assertFalse(t.companias_faltantes())
        # # La fecha de actuzalización de companias esta desactualizada.
        # t.productor.companias_actualizadas_al_dia = datetime.date.today() + datetime.timedelta(days=-1)
        # self.assertTrue(t.productor.companias_desactualizadas())

        # t.productor.companias_actualizadas_al_dia = datetime.date.today()
        # #Actualizo las companias
        # self.assertFalse(t.productor.companias_desactualizadas())

        # # viene la clave
        # cod1.claves.create(usuario="a", password="b")
        # cod2.claves.create(usuario="a", password="b")
        # cod3.claves.create(usuario="a", password="b")

        # cod1.save()
        # cod2.save()
        # cod3.save()

        # # al guardar y tener companias actualizadas, se calcula el monto del recupero..
        # t.save()

        # # solo hay que completar companias que
        # # hayan comenzado antes del 2015-12-01 (cod1 y cod3)

        # # si es preferencial ya puedo empezar
        # prod.preferencial = True
        # prod.save()
        # # self.assertEqual(t.saldo_pendiente(), 0)

        # # si es comun, debo cobrar el recupero primero
        # prod.preferencial = False
        # prod.save()
        # # self.assertEqual(t.saldo_pendiente(), t.importe_recupero)

        # # cobro los recuperos
        # t.cobros.create(importe=5)
        # # self.assertEqual(t.saldo_pendiente(), 15)
        # t.cobros.create(importe=5)
        # # self.assertEqual(t.saldo_pendiente(), 10)

        # # self.assertEqual(len(t.descargas_pendientes()), 4)
        # # una de las companias no tiene clave y la otra no se puede descargar
        # self.assertEqual(len(t.descargas_pendientes(solo_descargables=True)), 2)
        # self.assertEqual(len(t.claves_faltantes()), 1)
        # self.assertEqual(len(t.archivos_faltantes()), 1)

        # # La tarea Descarga necesita un trabajo (Carlos Garcia)
        # cod1.descargas.create(trabajo=t, fecha=tz_parse("2016-02-01"), desde="2015-01-01", hasta="2015-12-01")
        # self.assertTrue(cod1.falta_recuperar())

        # # Son 3 porque de las 4 una no es descargable
        # self.assertEqual(len(t.descargas_pendientes()), 3)

        # # Estaba 1 yo le coloque 4 porque son 4 compañias en propductor (Carlos Garcia)
        # self.assertEqual(len(t.recuperos_pendientes()), 4)

        # cod1.recuperos.create(trabajo=t, fecha=tz_parse("2016-03-01"))
        # self.assertFalse(cod1.falta_recuperar())

        # self.assertFalse(t.generacion_pendiente("O"))

        # self.assertEqual(len(t.descargas_pendientes()), 3)

        # cod3.descargas.create(trabajo=t, desde="2015-01-01", hasta="2015-12-01")

        # self.assertEqual(len(t.recuperos_pendientes()), 3)
        # cod3.recuperos.create(trabajo=t)

        # # Estaban en 0 Carlos Garcia
        # self.assertEqual(len(t.descargas_pendientes()), 2)
        # self.assertEqual(len(t.recuperos_pendientes()), 2)

        # #Agrego la cantidad de operaciones, y de cobranzas.
        # t.cantidad_operaciones = 200
        # t.cantidad_cobranzas = 200
        # t.save()

        # # pero todavía no está la generación aprobada
        # self.assertFalse(t.generacion_pendiente("O"))


        # # Se crea la clave faltante
        # cod4.claves.create(usuario="a", password="b")
        # cod4.save()

        # cod2.descargas.create(trabajo=t, fecha=tz_parse("2016-02-01"), desde="2015-01-01", hasta="2015-12-01")
        # cod2.recuperos.create(trabajo=t, fecha=tz_parse("2016-03-01"))

        # cod4.descargas.create(trabajo=t, fecha=tz_parse("2016-02-01"), desde="2015-01-01", hasta="2015-12-01")
        # cod4.recuperos.create(trabajo=t, fecha=tz_parse("2016-03-01"))

        # # presupuestos pendientes
        # self.assertEqual(t.presupuesto_pendientes().trabajo.id, 1)

        # prod.creditos_disponibles = 300
        # prod.save()

        # # ya se puede "imprimir" las operaciones

        # self.assertTrue(t.generacion_pendiente("O"))
        # # pero todavía no sabemos cuántas cobranzas son
        # self.assertFalse(t.generacion_pendiente("C"))

        # # no alcanza 1 libro de cobranzas
        # self.assertFalse(t.generacion_pendiente("C"))

        # prod.creditos_disponibles += 100
        # prod.save()
        # # imprime más hojas
        # # ahora sí alcanza y sobra

        # # Estaba True se coloca False
        # self.assertTrue(t.generacion_pendiente("C"))

        # # genero los libros de operaciones
        # GeneracionDigitales.objects.create(trabajo=t, tipo='O', hasta=tz_parse("2015-12-02"))
        # # ya no hace falta generar
        # self.assertFalse(t.generacion_pendiente("O"))

        # # genero los libros de cobranzas
        # GeneracionDigitales.objects.create(trabajo=t, tipo='C', hasta=tz_parse("2015-11-01"))
        # # no está completo
        # self.assertFalse(t.generacion_pendiente("C"))
        # # genero lo que falta
        # GeneracionDigitales.objects.create(trabajo=t, tipo='C', hasta=tz_parse("2015-12-05"))
        # # ahora sí
        # self.assertFalse(t.generacion_pendiente("C"))

        # # ejemplos de cobros parciales
        # self.assertEqual(t.saldo_pendiente(), 10)
        # t.cobros.create(importe=5)
        # t.save()
        # # self.assertEqual(t.saldo_pendiente(), 5)
        # t.cobros.create(importe=5)

    # def test_flow_completo_modified_impresos(self):

        # precios = ListaDePrecios.objects.create(fecha=tz_parse("2015-11-01"), hoja_operaciones=10, hoja_cobranzas=10, encabezados=11, contenidos=10, timbrado=10, recupero=10)
        # precios.save()

        # prod = Productor.objects.create(nombre="Productores Asociados", tipo_persona="J", matricula="22", companias_actualizadas_al_dia=datetime.date.today()+datetime.timedelta(days=-1))
        # prod.save()
        # t = TrabajoImpreso.objects.create(productor=prod, fecha=tz_parse("2015-12-01"))

        # t.save()

        # # faltan las companias
        # self.assertTrue(t.companias_faltantes())

        # # agregar companias
        # c1 = Compania.objects.create(nombre="A", descargable_impresos=True)
        # c2 = Compania.objects.create(nombre="B", descargable_impresos=True)
        # c3 = Compania.objects.create(nombre="C", descargable_impresos=False)
        # c4 = Compania.objects.create(nombre="D", descargable_impresos=True)
        # cod1 = prod.companias.create(compania=c1)
        # cod2 = prod.companias.create(compania=c2, desde="2016-01-01")
        # cod3 = prod.companias.create(compania=c3, hasta="2016-01-01")
        # cod4 = prod.companias.create(compania=c4, desde="2016-01-01", hasta="2016-01-31")

        # # no faltan más las companias
        # self.assertFalse(t.companias_faltantes())

        # # La fecha de actuzalización de companias esta desactualizada. (desde qe la creamos)
        # self.assertTrue(t.productor.companias_desactualizadas())

        # # "Actuaalizo" las companias
        # prod.companias_actualizadas_al_dia = datetime.date.today()
        # prod.save()

        # self.assertFalse(t.productor.companias_desactualizadas())

        # #asigno el precio a los recuperos
        # t.save()
        # self.assertEqual(t.importe_recupero,20)

        # # si es preferencial ya puedo empezar
        # prod.preferencial = True
        # prod.save()

        # # si es comun, debo cobrar el recupero primero
        # prod.preferencial = False
        # prod.save()

        # self.assertEqual(t.saldo_pendiente(), t.importe_recupero)

        # # cobro los recuperos
        # t.cobros.create(importe=10)
        # self.assertEqual(t.saldo_pendiente(), 10)
        # t.cobros.create(importe=10)
        # self.assertEqual(t.saldo_pendiente(), 0)

        # self.assertEqual(len(t.descargas_pendientes()), 4)
        # # una de las companias no tiene clave y la otra no se puede descargar
        # # POrque tenia 3? esto deveulve 0
        # self.assertEqual(len(t.descargas_pendientes(solo_descargables=True)), 0)
        # self.assertEqual(len(t.archivos_faltantes()), 1)
        # self.assertEqual(len(t.claves_faltantes()), 3)

        # cod1.descargas.create(trabajo=t, fecha=tz_parse("2016-02-01"), desde="2015-01-01", hasta="2015-12-01")
        # self.assertTrue(cod1.falta_recuperar())

        # self.assertEqual(len(t.descargas_pendientes()), 3)
        # self.assertEqual(len(t.recuperos_pendientes()), 4)

        # cod1.recuperos.create(trabajo=t, fecha=tz_parse("2016-03-01"))
        # self.assertFalse(cod1.falta_recuperar())
        # self.assertFalse(t.generacion_pendiente("O"))

        # self.assertEqual(len(t.descargas_pendientes()), 3)

        # cod3.descargas.create(trabajo=t, desde="2015-01-01", hasta="2015-12-01")

        # self.assertEqual(len(t.recuperos_pendientes()), 3)

        # cod3.recuperos.create(trabajo=t)

        # self.assertEqual(len(t.descargas_pendientes()), 2)
        # self.assertEqual(len(t.recuperos_pendientes()), 2)

        # t.cantidad_hojas_operaciones = 101
        # t.cantidad_hojas_cobranzas = 101
        # t.save()

        # t.calcular_presupuesto()

        # #debería ser 20, necesito 2 libros nuevos y cada timbrado sale 10 c/u
        # self.assertEqual(t.importe_timbrado, 20)

        # #debería ser 20, necesito 2 libros nuevos y los encabezados salen 11 c/u
        # self.assertEqual(t.importe_encabezados, 22)

        # # deberia ser hojas_operaciones + hojas cobranzas / 100 * importe (202/100)*10
        # self.assertEqual(t.importe_contenidos, 20.2)

        # # pero todavía no está la generación aprobada
        # self.assertFalse(t.generacion_pendiente("O"))

        # prod.ultimo_libro_operaciones = 3
        # prod.save()
        # # ya se puede imprimir las operaciones
        # # Estaba True pero deveulve False porque faltan las claves
        # self.assertFalse(t.generacion_pendiente("O"))

        # # Agregar las claves
        # # cod1.claves.create(usuario="a", password="c")
        # # self.assertEqual(len(t.claves_faltantes()), 0)

        # self.assertFalse(t.generacion_pendiente("C"))

        # # imprime más hojas
        # prod.ultimo_libro_cobranzas = 3
        # prod.save()
        # # ahora sí alcanza y sobra
        # self.assertFalse(t.generacion_pendiente("C"))

        # # genero los libros de operaciones
        # GeneracionImpresos.objects.create(trabajo=t, tipo='O', hasta=tz_parse("2015-12-02"))
        # # ya no hace falta generar
        # self.assertFalse(t.generacion_pendiente("O"))

        # # genero los libros de cobranzas
        # GeneracionImpresos.objects.create(trabajo=t, tipo='C', hasta=tz_parse("2015-11-01"))
        # # no está completo
        # self.assertFalse(t.generacion_pendiente("C"))
        # # genero lo que falta
        # GeneracionImpresos.objects.create(trabajo=t, tipo='C', hasta=tz_parse("2015-12-05"))
        # # ahora sí
        # self.assertFalse(t.generacion_pendiente("C"))

        # # ejemplos de cobros parciales
        # # estaba t.saldo_pendiente() yo coloque t.saldo() Carlos
        # self.assertEqual(t.saldo(), 62.2)
        # t.cobros.create(importe=32.2)
        # t.save()
        # self.assertEqual(t.saldo(), 30)
        # t.cobros.create(importe=0.0)

    # def test_otros_status(self):
        # #import ipdb;ipdb.set_trace()
        # precios = ListaDePrecios.objects.create(fecha=tz_parse("2015-11-01"), hoja_operaciones=10, hoja_cobranzas=10, encabezados=11, contenidos=10, timbrado=10, recupero=10)
        # precios.save()

        # prod = Productor.objects.create(nombre="Productores Asociados", tipo_persona="J", matricula="11", companias_actualizadas_al_dia=datetime.date.today())
        # t = TrabajoImpreso.objects.create(productor=prod,fecha=tz_parse("2015-12-01"))
        # c1 = Compania.objects.create(nombre="A", descargable_impresos=True)
        # cod1 = prod.companias.create(compania=c1)

        # # falta la clave
        # self.assertEqual(len(t.descargas_pendientes()),1)
        # self.assertEqual(len(t.claves_faltantes()), 1)

        # # viene la clave
        # cod1.claves.create(usuario="a", password="b")
        # self.assertEqual(len(t.claves_faltantes()), 0)

        # # clave incorrecta
        # descarga = t.tareas_pendientes()[0]
        # descarga.status = "ERR"
        # descarga.save()


        # self.assertEqual(len(t.descargas_pendientes()),1)
        # self.assertEqual(len(t.claves_faltantes()), 1)

        # # viene nueva clave
        # cod1.claves.create(usuario="a", password="c")
        # self.assertEqual(len(t.claves_faltantes()), 0)

        # # postergar
        # descarga = t.tareas_pendientes()[0]
        # descarga.status = "POS"
        # descarga.fecha = tz_parse("2016-01-01")
        # descarga.save()
        # descarga.refresh_from_db()
        # descarga2 = t.tareas_pendientes()[0]
        # self.assertEqual(descarga2.postergada_hasta(), descarga.fecha )  # + TIEMPO_POSTERGACION)

    # def test_descargas_seguidas(self):
        # precios = ListaDePrecios.objects.create(fecha=tz_parse("2015-11-01"), hoja_operaciones=10, hoja_cobranzas=10, encabezados=11, contenidos=10, timbrado=10, recupero=10)
        # precios.save()

        # prod = Productor.objects.create(nombre="Productores Asociados", tipo_persona="J", matricula="11", companias_actualizadas_al_dia=datetime.date.today())
        # t = TrabajoImpreso.objects.create(productor=prod,fecha=tz_parse("2015-12-01"))
        # c1 = Compania.objects.create(nombre="A", descargable_impresos=True)
        # cod1 = prod.companias.create(compania=c1)

        # # si es preferencial ya puedo empezar
        # prod.preferencial = True
        # prod.save()

        # # viene la clave
        # cod1.claves.create(usuario="a", password="b")
        # self.assertEqual(len(t.claves_faltantes()), 0)

        # cod1.descargas.create(trabajo=t, fecha=tz_parse("2016-02-01"), desde="2015-01-01", hasta="2015-12-01")
        # self.assertEqual(len(t.descargas_pendientes()), 0)

        # t.terminado=True
        # t.save()

        # t = TrabajoImpreso.objects.create(productor=prod,fecha=tz_parse("2015-12-01"))
        # c1 = Compania.objects.create(nombre="A", descargable_impresos=True)
        # cod1 = prod.companias.create(compania=c1)

        # # viene la clave
        # cod1.claves.create(usuario="a", password="b")
        # self.assertEqual(len(t.claves_faltantes()), 0)

        # self.assertEqual(len(t.descargas_pendientes()), 1)

    # """
    # def test_cuotas_pendiente(self):
        # '''.'''
        # precios = ListaDePrecios.objects.create(fecha=tz_parse("2015-11-01"), hoja_operaciones=10, hoja_cobranzas=10, encabezados=11, contenidos=10, timbrado=10, recupero=10)
        # precios.save()

        # prod = Productor(nombre="Productores Asociados", tipo_persona="J")
        # prod.libros_digitales = True
        # prod.mensual_digitales = True
        # prod.save()

        # # Si no tiene terabajos anteriores
        # self.assertEqual(prod.cuotas_pendiente(), None)

        # # Generar un Trabajo Digital
        # trabajo = TrabajoDigital.objects.create(productor=prod, fecha=tz_parse('2017-03-11'))

        # # Cobro el trabajo
        # trabajo.cobros.create(importe=5, fecha=tz_parse('2017-01-11'))

        # # Cierro el trabajo
        # trabajo.terminado = True
        # trabajo.save()

        # # 3 Meses
        # self.assertEqual(prod.cuotas_pendiente(), 3)
    # """

    # def test_ultimo_cobro(self):
        # '''.'''
        # precios = ListaDePrecios.objects.create(fecha=tz_parse("2015-11-01"), hoja_operaciones=10, hoja_cobranzas=10, encabezados=11, contenidos=10, timbrado=10, recupero=10)
        # precios.save()

        # prod = Productor(nombre="Productores Asociados", tipo_persona="J")
        # prod.libros_digitales = True
        # prod.mensual_digitales = True
        # prod.save()

        # # Si no tiene terabajos anteriores
        # self.assertEqual(prod.cuotas_pendiente(), 0)

        # # Generar un Trabajo Digital
        # trabajo = TrabajoDigital.objects.create(productor=prod, fecha=tz_parse('2017-03-11'))

        # # Cobro el trabajo
        # trabajo.cobros.create(importe=10, fecha=tz_parse('2017-01-11'))

        # # Cierro el trabajo
        # trabajo.terminado = True
        # trabajo.save()

        # # Fecha ultimo Cobro y monto
        # self.assertEqual(prod.ultimo_cobro(), 'ENERO 2017 /  10.0')

