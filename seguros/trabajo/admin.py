# import math
# from django.contrib import admin
# from django.db import models
# from .models import (
        # Trabajo, TrabajoImpreso, TrabajoDigital, Tarea, Cobro,
        # PresupuestoImpresos, ListaDePrecios,
        # Descarga, Recupero, GeneracionImpresos, GeneracionDigitales, Reclamo
        # )
# from polymorphic.admin import (
        # PolymorphicParentModelAdmin, PolymorphicChildModelAdmin,
        # PolymorphicChildModelFilter,
        # )
# from django_select2.forms import Select2Widget
# from .forms import CobroForm, TrabajoImpresoForm, ListaDePreciosForm


# class CobroInlineAdmin(admin.TabularInline):
    # '''.'''
    # fields = ('fecha', 'comprobante', 'importe')
    # model = Cobro
    # form = CobroForm
    # extra = 1


# class CobroAdmin(admin.ModelAdmin):
    # '''.'''
    # model = Cobro
    # form = CobroForm
    # fields = ('fecha', 'comprobante', 'importe', 'trabajo')
    # list_display = ('id', 'fecha', 'comprobante', 'importe',
                    # 'productor', 'cobrado', 'saldo_pendiente')
    # list_filter = (['fecha', 'comprobante', 'trabajo'])
    # date_hierarchy = 'fecha'

    # def productor(self, obj):
        # '''.'''
        # return obj.trabajo.productor.nombre

    # def cobrado(self, obj):
        # '''.'''
        # return obj.trabajo.cobrado

    # def saldo_pendiente(self, obj):
        # '''.'''
        # return obj.trabajo.saldo_pendiente()


# class TrabajoDigitalInline(admin.TabularInline):
    # '''.'''
    # model = TrabajoDigital


# @admin.register(TrabajoDigital)
# class TrabajoDigitalAdmin(admin.ModelAdmin):
    # '''.'''
    # inlines = [CobroInlineAdmin]
    # base_model = TrabajoDigital
    # fieldsets = [
            # (None, {'fields': ('productor', 'terminado', )}),
            # ('Datos extra', {'fields': ('observaciones', )}),
            # ]

    # list_display = ('fecha', 'productor', 'terminado')
    # search_fields = ('productor',)
    # list_filter = ('terminado', )
    # readonly_fields = ['terminado', ]

    # formfield_overrides = {
        # models.ForeignKey: {'widget': Select2Widget},
    # }


# class TrabajoImpresoInline(admin.TabularInline):
    # '''.'''
    # model = TrabajoImpreso


# @admin.register(TrabajoImpreso)
# class TrabajoImpresoAdmin(admin.ModelAdmin):
    # '''.'''
    # inlines = [CobroInlineAdmin]
    # base_model = TrabajoImpreso
    # form = TrabajoImpresoForm
    # fieldsets = [
            # (None, {'fields': ('productor', 'terminado', )}),
            # ('Datos extra', {'fields': ('observaciones', )}),
            # ('Presupuesto (no es necesario llenar ahora)',
                # {'fields': (
                    # ('cantidad_hojas_operaciones',),
                    # ('cantidad_hojas_cobranzas',), 'importe_recupero',
                    # ('importe_encabezados', 'calculo_importe_encabenzados'),
                    # ('importe_contenidos', 'calculo_importe_contenidos'),
                    # 'descuento', 'subtotal',
                    # ('importe_timbrado', 'calculo_importe_timbrado'),
                    # ('total_importe_mas_timbrado'),
                    # 'cobrado',
                    # )}),
    # ]
    # list_display = ('fecha', 'productor', 'terminado')
    # search_fields = ('productor', )
    # list_filter = ('terminado', )
    # readonly_fields = [
            # 'terminado', 'calculo_importe_timbrado',
            # 'calculo_importe_encabenzados', 'calculo_importe_contenidos',
            # 'total_importe_mas_timbrado',
            # ]
    # formfield_overrides = {models.ForeignKey: {'widget': Select2Widget}}

    # def calculo_importe_encabenzados(self, obj):
        # precios = obj.listadeprecios_relacionada()
        # diferencia_de_hojas_O = max(0, obj.cantidad_hojas_operaciones)
        # diferencia_de_hojas_C = max(0, obj.cantidad_hojas_cobranzas)
        # importe_encabezados = (math.ceil(float(diferencia_de_hojas_C) / 100) + math.ceil(float(diferencia_de_hojas_O) / 100)) * precios.encabezados
        # return importe_encabezados

    # def calculo_importe_contenidos(self, obj):
        # precios = obj.listadeprecios_relacionada()
        # diferencia_de_hojas_O = max(0, obj.cantidad_hojas_operaciones)
        # diferencia_de_hojas_C = max(0, obj.cantidad_hojas_cobranzas)
        # importe_contenidos = (math.ceil(float(diferencia_de_hojas_C) / 100) + math.ceil(float(diferencia_de_hojas_O) / 100)) * precios.contenidos
        # return importe_contenidos

    # def calculo_importe_timbrado(self, obj):
        # '''.'''
        # precios = obj.listadeprecios_relacionada()
        # diferencia_de_hojas_O = max(0, obj.cantidad_hojas_operaciones)
        # diferencia_de_hojas_C = max(0, obj.cantidad_hojas_cobranzas)
        # importe_timbrado = (math.ceil(float(diferencia_de_hojas_C) / 100) + math.ceil(float(diferencia_de_hojas_O) / 100)) * precios.timbrado
        # return importe_timbrado

    # def subtotal(self, obj):
        # '''.'''
        # descuento = 0 if not obj.descuento else obj.descuento
        # recupero = 0 if not obj.importe_recupero else obj.importe_recupero
        # encabezados = 0 if not obj.importe_encabezados else obj.importe_encabezados
        # contenidos = 0 if not obj.importe_contenidos else obj.importe_contenidos

        # return (recupero + encabezados + contenidos) - descuento

    # def total_importe_mas_timbrado(self, obj):
        # '''.'''
        # timbrado = obj.importe_timbrado if obj.importe_timbrado else self.calculo_importe_timbrado(obj)
        # subtotal = self.subtotal(obj)
        # return timbrado + subtotal

# @admin.register(Trabajo)
# class TrabajoAdmin(PolymorphicParentModelAdmin):
    # """ The parent model admin """
    # list_display = ['fecha', 'productor', 'pendiente_mejor', 'terminado', 'saldo_pend']
    # list_filter = ['user', 'terminado', 'productor',]
    # search_fields = ['productor__nombre']
    # polymorphic_list = True
    # date_hierarchy = 'fecha'
    # base_model = Trabajo
    # # child_models = [TrabajoDigital, TrabajoDigitalAdmin, TrabajoImpreso, TrabajoImpresoAdmin,]
    # child_models = [TrabajoDigital, TrabajoImpreso, ]
    # """
    # child_models = (
        # (TrabajoDigital, TrabajoDigitalAdmin),
        # (TrabajoImpreso, TrabajoImpresoAdmin),
    # )
    # """
    # def get_queryset(self, request):
        # '''.'''
        # qset = super(TrabajoAdmin, self).get_queryset(request)
        # return qset.all()

    # """def pendiente_mejorado(self, obj):
        # tareas_pendiente = trabajo.tareas_pendientes()
        # motivo_espera = [' '.join((\
                # tp.observaciones.strip() if tp.observaciones else '',\
                # tp.tipo_tarea().strip() if tp.tipo_tarea() else ''))\
                # for tp in tareas_pendiente]

        # return  ' '.join(motivo_espera).strip()"""

    # def saldo_pend(self, obj):
        # devolver_saldo = 0
        # obtener_saldo = obj.saldo()
        # if not obj.terminado:
            # if obtener_saldo > 0:
                # devolver_saldo = obtener_saldo
        # return devolver_saldo
    # saldo_pend.short_description = 'Saldo pendiente'

    # def pendiente_mejor(self, obj):
        # return obj.pendiente_mejorado()
    # pendiente_mejor.short_description = 'Estado'

    # def encargado(self, obj):
        # encargado = ''
        # if hasattr(obj.productor.encargado, 'username'):
            # encargado = obj.productor.encargado.username
        # return encargado

    # def pendiente(self, obj):
        # '''.'''
        # tarea_pend = obj.tareas_pendientes()
        # if tarea_pend:
            # # Se saca tipo de tarea de la descripcion
            # tarea_devolver = ''.join([' {1}'.format(
                # f.tipo_tarea(),
                # f.observaciones.replace('\n', '') \
                        # if f.observaciones else '') \
                        # for f in tarea_pend])
        # else:
            # tarea_devolver = 'Sin tarea pendiente'
        # return tarea_devolver

    # pendiente.short_description = 'Estado'
    # # user.short_description = 'Usuario'

# class TareaChildAdmin(PolymorphicChildModelAdmin):
    # """ Base admin class for all child models """
    # base_model = Tarea

    # base_fieldsets = (
        # (None, {
            # 'fields': (
                # 'status', 'observaciones'
            # )
        # }),
    # )
    # formfield_overrides = {
        # models.ForeignKey: {'widget': Select2Widget},
    # }


# class DescargaAdmin(TareaChildAdmin):
    # '''.'''
    # base_model = Descarga


# class RecuperoAdmin(TareaChildAdmin):
    # '''.'''
    # base_model = Recupero


# class GeneracionImpresosAdmin(TareaChildAdmin):
    # '''.'''
    # base_model = GeneracionImpresos


# class GeneracionDigitalesAdmin(TareaChildAdmin):
    # '''.'''
    # base_model = GeneracionDigitales

# class ReclamoAdmin(TareaChildAdmin):
    # '''.'''
    # base_model = Reclamo

# class PresupuestoImpresoAdmin(TareaChildAdmin):
    # '''.'''
    # base_model = PresupuestoImpresos

# class TareaAdmin(PolymorphicParentModelAdmin):
    # """ The parent model admin """
    # list_display = ['fecha', 'user', 'productor', 'compania',]
    # list_filter = ['user', PolymorphicChildModelFilter]
    # polymorphic_list = True
    # date_hierarchy = 'fecha'
    # base_model = Tarea
    # child_models = [
        # Descarga, DescargaAdmin,
        # Recupero, RecuperoAdmin,
        # GeneracionImpresos, GeneracionImpresosAdmin,
        # GeneracionDigitales, GeneracionDigitalesAdmin,
        # PresupuestoImpresos, PresupuestoImpresoAdmin,
        # Reclamo, ReclamoAdmin,
    # ]

    # def get_queryset(self, request):
        # '''.'''
        # qset = super(TareaAdmin, self).get_queryset(request)
        # qset.all()
        # #return qset.exclude(status='OK')
        # return qset

    # def productor(self, object):
        # '''.'''
        # if hasattr(object, "productor"):
            # return object.productor
        # if hasattr(object, "productor_en_compania"):
            # return object.productor_en_compania.productor
        # if hasattr(object, 'trabajo') and (not hasattr(object, 'productor_en_compania') or \
        # not hasattr(object, 'productor')):
            # return object.trabajo.productor.nombre


    # def compania(self, obj):
        # '''.'''
        # if hasattr(obj, "productor_en_compania"):
            # return obj.productor_en_compania.compania


    # def tarea(self, obj):
        # '''.'''
        # return obj.tipo_tarea()

# class ListaDePreciosAdmin(admin.ModelAdmin):
    # '''.'''
    # model = Cobro
    # form = ListaDePreciosForm
    # class Media(object):
        # '''.'''
        # css = {'all': ('no-more-warnings.css', )}


# admin.site.register(GeneracionImpresos)
# admin.site.register(Cobro, CobroAdmin)
# admin.site.register(ListaDePrecios, ListaDePreciosAdmin)
# admin.site.register(Tarea, TareaAdmin)
# # admin.site.register(Trabajo, TrabajoAdmin)
# #admin.site.register(TrabajoAdmin)
# #admin.site.register(TrabajoImpreso)
