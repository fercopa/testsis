# -*- coding: utf-8 -*-
# pylint: disable=missing-docstring, no-self-use, line-too-long

from django.db import models
from django.contrib.auth.models import User

from django.core.mail import EmailMessage, EmailMultiAlternatives, get_connection
from seguros.settings import MAIL_NOTIFICACIONES_VLIC_CONF
from seguros.settings import SENDGRID_API_KEY
import sendgrid
import os
from sendgrid.helpers.mail import *


class EmailNotification:
    def __init__(self, usuario_instancia):
        '''Envia un email a un usuario pasado como parametro'''
        # MAIL_NOTIFICACIONES_VLIC_CONF

        self.usuario = usuario_instancia

    def send_email_single(self, asunto, cuerpo, para=''):

        asunto = asunto
        cuerpo = cuerpo
        para_email = para

        correo = []
        correo.append(self.usuario.email)
        email = EmailMessage(asunto, cuerpo , '',  correo)

        mail_connection = get_connection(**MAIL_ALERTAS_CONF)
        mail_connection.open()
        mail_connection.send_messages([email])


    def send_email_with_sendgrid(self, data):
        sg = sendgrid.SendGridAPIClient(apikey=SENDGRID_API_KEY)

        email_ctx = Context(data)
        html_template = get_template('new_email_nota.html')
        html_content = html_template.render(email_ctx)

        subject = u'{} - {} - {}'.format(
                data['nombre_medio'],
                data['titulo'],
                data['fecha_hora'],
            )

        from_email = Email("info@lis.com")
        to_email = Email(self.usuario.email)

        content = Content("text/html", html_content)
        mail = Mail(from_email, subject, to_email, content)
        response = sg.client.mail.send.post(request_body=mail.get())

        if response.status_code == 202:
            return {'enviado': True, 'msg': 'Enviado con éxito'}
        else:
            return {'enviado': False, 'msg': 'Enviado con éxito'}

    def test(self, to_email, subject, body):
        import sendgrid
        import os
        from sendgrid.helpers.mail import *

        sg = sendgrid.SendGridAPIClient(apikey=os.environ.get('SENDGRID_API_KEY'))
        from_email = Email("notificaciones@lis.com")
        to_email = Email(to_email)
        subject = subject
        content = Content("text/plain", body)
        mail = Mail(from_email, subject, to_email, content)
        response = sg.client.mail.send.post(request_body=mail.get())
        print(response.status_code)
        print(response.body)
        print(response.headers)

        if response.status_code == 202:
            return {'enviado': True, 'msg': 'Enviado con éxito'}
        else:
            return {'enviado': False, 'msg': 'Enviado con éxito'}
