from django.views.generic.edit import UpdateView
from . import models


def tarjeta(request, trabajo_id, tarea_id):
    pass


def proxima_tarjeta(request, trabajo_id=None):
    pass


def get_trabajo_sin_encargado(request):
    pass


def pasar_pendiente(request, trabajo_id, tarea_id):
    pass


def pasar_a_pendiente(trabajo):
    pass


def dashboard(request):
    pass


class TrabajoDigitalUpdate(UpdateView):
    model = models.TrabajoDigital
    fields = '__all__'
    template_name = 'trabajo_digital_update.html'
