import datetime
from trabajo.models import Propuesta, TrabajoDigital, Tarea, Trabajo, Recupero

from mixer.backend.django import mixer
import pytest
pytestmark = pytest.mark.django_db


class TestTrabajo:
    def test_model(self):
        desde = datetime.date(2014, 12, 23)
        hasta = datetime.date(2015, 12, 23)
        work = mixer.blend(Trabajo, fecha_desde=desde, fecha_hasta=hasta)
        assert work.pk == 1

    def test_model_save(self):
        desde = datetime.date(2016, 12, 23)
        hasta = datetime.date(2015, 12, 23)
        mixer.blend(Trabajo, fecha_desde=desde, fecha_hasta=hasta)
        amount_works = Trabajo.objects.all().count()
        assert amount_works == 0

    def test_finalizar(self):
        desde = datetime.date(2016, 12, 23)
        hasta = datetime.date(2015, 12, 23)
        work = mixer.blend(Trabajo, fecha_desde=desde, fecha_hasta=hasta)
        work.finalizar()
        assert work.terminado is True

    def test_finalizar_with_task(self):
        desde = datetime.date(2014, 12, 23)
        hasta = datetime.date(2015, 12, 23)
        work = mixer.blend(Trabajo, fecha_desde=desde, fecha_hasta=hasta)
        work.proponer()
        work.finalizar()
        assert work.terminado is False


class TestTrabajoDigital:
    def test_model(self):
        desde = datetime.date(2014, 12, 23)
        hasta = datetime.date(2015, 12, 23)
        work = mixer.blend(TrabajoDigital, fecha_desde=desde,
                           fecha_hasta=hasta)
        assert work.pk == 1

    def test_create_digital_work(self):
        desde = datetime.date(2014, 12, 23)
        hasta = datetime.date(2015, 12, 23)
        productor = mixer.blend('productor.Productor')
        productor.create_digital_work(desde, hasta)
        works = TrabajoDigital.objects.filter(productor=productor,
                                              terminado=False)
        assert len(works) == 1
        work = works[0]
        tasks = Tarea.objects.filter(trabajo=work)
        assert len(tasks) == 1

    def test_cobrar_work(self):
        pass

    def test_reclamar_claves(self):
        pass

    def test_reclamar_archivos(self):
        pass

    def test_finalizar_descarga(self):
        pass

    def test_finalizar_toma(self):
        pass

    def test_con_controles_gruesos(self):
        pass

    def test_con_controles_finos(self):
        pass

    def test_subir_libro_digital(self):
        pass

    def test_retirar_pdf(self):
        pass

    def test_enviar_pdf(self):
        pass

    def test_finalizar_work_with_incomplete_tasks(self):
        productor = mixer.blend('productor.Productor')
        mixer.cycle(5).blend('productor.ProductorEnCompania',
                             productor=productor)
        desde = datetime.date(2014, 12, 23)
        hasta = datetime.date(2015, 12, 23)
        productor.create_digital_work(desde, hasta)
        work = productor.get_active_work()
        work.accept_propuesta()
        work.finalizar()
        assert work.terminado is False


class TestTareaPropuesta:
    def test_model(self):
        desde = datetime.date(2014, 12, 23)
        hasta = datetime.date(2015, 12, 23)
        task = mixer.blend(Propuesta, trabajo__fecha_desde=desde,
                           trabajo__fecha_hasta=hasta)
        assert task.pk == 1

    def test_accept_propuesta_without_companies(self):
        desde = datetime.date(2014, 12, 23)
        hasta = datetime.date(2015, 12, 23)
        task = mixer.blend(Propuesta, trabajo__fecha_desde=desde,
                           trabajo__fecha_hasta=hasta)
        task.accept()
        work = task.trabajo
        amount_cias = work.get_amount_active_companies()
        amount_descarga = work.get_amount_descargas()
        assert amount_cias == amount_descarga
        assert work.get_amount_cobros() == 1
        assert task.accepted is True
        assert task.finalized is True

    def test_accept_propuesta_with_some_companies(self):
        productor = mixer.blend('productor.Productor')
        mixer.cycle(5).blend('productor.ProductorEnCompania',
                             productor=productor, active=True)
        desde = datetime.date(2014, 12, 23)
        hasta = datetime.date(2015, 12, 23)
        work = mixer.blend(TrabajoDigital, productor=productor,
                           fecha_desde=desde, fecha_hasta=hasta)
        work.proponer()
        work.accept_propuesta()
        propuesta = work.get_finalized_propuesta()
        amount_cias = work.get_amount_active_companies()
        amount_descarga = work.get_amount_descargas()
        assert amount_cias == 5
        assert amount_cias == amount_descarga
        assert work.get_amount_cobros() == 1
        assert propuesta.accepted is True
        assert propuesta.finalized is True

    def test_reject_propuesta(self):
        desde = datetime.date(2014, 12, 23)
        hasta = datetime.date(2015, 12, 23)
        task = mixer.blend(Propuesta, trabajo__fecha_desde=desde,
                           trabajo__fecha_hasta=hasta)
        task.reject()
        assert task.accepted is False
        assert task.finalized is True


class TestTareaDescarga:
    def test_get_company_name(self):
        pec = mixer.blend('productor.ProductorEnCompania',
                          compania__codigo=292)
        desde = datetime.date(2014, 12, 23)
        hasta = datetime.date(2015, 12, 23)
        descarga = mixer.blend('trabajo.Descarga',
                               trabajo__fecha_desde=desde,
                               trabajo__fecha_hasta=hasta,
                               productor_en_compania=pec)
        company_name = descarga.get_company_name()
        assert '9 DE JULIO' in company_name

    def test_finalizar(self):
        desde = datetime.date(2014, 12, 23)
        hasta = datetime.date(2015, 12, 23)
        work = mixer.blend('trabajo.Trabajo', fecha_desde=desde,
                           fecha_hasta=hasta)
        descarga = mixer.blend('trabajo.Descarga', trabajo=work)
        descarga.finalizar()
        amount_recuperos = Recupero.objects.all().count()
        assert amount_recuperos > 0
