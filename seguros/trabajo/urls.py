# pylint: disable-all
from django.conf.urls import url
from trabajo import views

urlpatterns = [
    url(r'^digital/new/$', views.TrabajoDigitalUpdate.as_view(),
        name='new-digital-work'),
    url(r'^tarjeta/(?P<trabajo_id>\d+)/(?P<tarea_id>\d+)/', views.tarjeta,
        name='tarjeta'),
    url(r'^tarjeta/(?P<trabajo_id>\d+)/', views.proxima_tarjeta),
    url(r'^tarjeta/', views.proxima_tarjeta),
    url(r'^get_trabajo_sin_encargado/', views.get_trabajo_sin_encargado),
    url(r'^dashboard/', views.dashboard, name='dashboard'),
    url(r'^pasar_pendiente/(?P<trabajo_id>\d+)/(?P<tarea_id>\d+)/',
        views.pasar_pendiente, name='pasar_pendiente'),
]
