from django.conf.urls import url
import compania.views as views

urlpatterns = [
    url(r'^$', views.Home.as_view(), name="compania-home"),
    url(r'^all/$', views.CompaniaList.as_view(), name='compania-list'),
    url(r'^add/$', views.CompaniaCreate.as_view(), name='compania-create'),
    url(r'^add/all/$', views.add_all_companies, name='compania-add-all'),
    url(r'^(?P<pk>\d+)/update/$', views.CompaniaUpdate.as_view(),
        name="compania-update"),
    url(r'^(?P<pk>\d+)/$', views.CompaniaDetail.as_view(),
        name="compania-detail"),
]
