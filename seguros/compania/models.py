import json
from django.urls import reverse_lazy
from django.db import models


class Compania(models.Model):
    codigo = models.IntegerField()
    portal_web = models.URLField(blank=True, null=True)
    portal_web_alternativo = models.URLField(blank=True, null=True)
    es_descargable = models.BooleanField(default=False)
    descarga_automatizada = models.BooleanField(default=False)
    instructivo_descarga = models.TextField(blank=True)
    instructivo_recupero = models.TextField(blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.get_nombre()

    class Meta:
        ordering = ['codigo']

    def get_absolute_url(self):
        return reverse_lazy('compania-detail', kwargs={'pk': self.pk})

    def get_nombre(self):
        try:
            with open('compania/companies.json') as f:
                data = json.load(f)
            nombre = data[str(self.codigo)]['nombre']
        except (KeyError, FileNotFoundError):
            nombre = ''
        return nombre

    def get_nombre_corto(self):
        try:
            with open('compania/companies.json') as f:
                data = json.load(f)
            nombre_corto = data[str(self.codigo)]['nombre_corto']
        except (KeyError, FileNotFoundError):
            nombre_corto = ''
        return nombre_corto
