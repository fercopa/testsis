# pylint: disable-all
import json
import time
from celery import task
from django.utils import timezone
import logging
import logging.config
from django.conf import settings as django_settings
# scrapy
from twisted.internet import reactor
from scrapy.crawler import Crawler, CrawlerProcess
from recolistor.spiders import mercantil, mapfre, nacion, \
                                norte, allianz, laholando, qbe, \
                                rivadavia, sancristobal, riouruguay
from celery.signals import after_setup_task_logger
from celery.utils.log import get_task_logger
log = get_task_logger(__name__)


spiders = {
    'mercantil': mercantil.MercantilSpider,
    'mapfre': mapfre.MapfreSpider,
    'nacion': nacion.NacionSpider,
    'norte': norte.NorteSpider,
    'allianz': allianz.AllianzSpider,
    'laholando': laholando.LaHolandoSpider,
    'rivadavia': rivadavia.RivadaviaSpider,
    'sancristobal': sancristobal.SanCristobalSpider,
    'qbe': qbe.QbeSpider,
    'riouruguay': riouruguay.RiouruguaySpider,
}


# Ejecitar desde consola Linux
# scrapy crawl mercantil -a id_download=1 -a user=fox -a password=test
# -a date_from=2017-01-01 -a date_to=2017-03-01 -a show=True
@task()
def download(data):
    LOGGING = django_settings.LOGGING
    LOGGING['handlers']['file']['filename'] = '/tmp/%s.log' % data['id']
    logging.config.dictConfig(LOGGING)

    # run spider
    spider = spiders[data['compania']]
    crawler = Crawler(spider)
    crawler.crawl(
        id_download=data['id'],
        user=data['user'],
        password=data['password'],
        password2=data['password2'],
        date_from=data['date_from'],
        date_to=data['date_to'],
        show="False",
        logger=log)
    reactor.run()


# Ejecitar desde consola Linux
# scrapy crawl mercantil -a id_download=1 -a user=fox -a password=test
# -a just_login=True -a show=True
@task()
def check_login(clave_id):
    from productores.models import Clave
    logging.error(clave_id)

    # por alguna razon no existe la instancia todavia
    time.sleep(5)

    clave = Clave.objects.get(id=clave_id)
    spider = spiders[clave.productor_en_compania.compania.nombre_script]

    process = CrawlerProcess()
    process.crawl(
        spider,
        id_download=clave.id,
        user=clave.usuario,
        password=clave.password,
        password2=clave.identificador,
        just_login="True",
        show="False",
        logger=log)
    process.start()

    with open("/tmp/claves/{}/result.json".format(clave_id), "r") as file:
        data = json.loads(file.read())

    if "result" in data and data['result'] is not None:
        clave.vigente = data['result']
        clave.fecha_validacion = timezone.now()
        clave.save()


@after_setup_task_logger.connect
def configure_task_logger(**kwargs):
    return


mercantil.MercantilSpider
