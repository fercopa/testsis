# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from productores.models import *
import csv


class Command(BaseCommand):
    #add_arguments define los argumentos válidos para parsear
    def add_arguments(self, parser):
        parser.add_argument('csv_filename', nargs='+', type=str)

    #handle ejecuta el codigo correspondiente al argumento
    def handle(self, *args, **options):
        for csv_filename in options['csv_filename']:
            with open(csv_filename, 'rb') as csvfile:
                companies_reader = csv.reader(csvfile, delimiter=',')  # delimiter='\t', quotechar='|')
                first_row = True
                i = 0
                for row in companies_reader:
                    print i
                    i += 1
                    name = row[0]
                    di = row[1]
                    dd = row [2]
                    if first_row: #la primera vuelta corresponde a los nombres de las columnas
                        first_row = False
                    elif Compania.objects.filter(nombre__iexact=name).count() == 0: #¿Existe ya la compania en la base?
                        print('Creando compania: {0}'.format(name))
                        compania = Compania.objects.create(nombre=name, descargable_impresos=parseSN(di), descargable_digital=parseSN(dd))
                        compania.save()
                    else:   #Si ya existe, actualizar la existente
                        print('Ya existe: {0} , se actualiza con el archivo'.format(name))
                        compania = Compania.objects.get(nombre__iexact=name)
                        compania.descargable_impresos = parseSN(di)
                        compania.descargable_digital = parseSN(dd)
                        compania.save()
                        #TODO: evaluar luego si se manejarán las excepciones de get (para cuando .count() >= 2)

def parseSN(si_o_no):
    if si_o_no == 'S':
        return True
    else:
        return False
