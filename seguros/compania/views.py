from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from .models import Compania
from .forms import CompaniaUpdateForm


@method_decorator(login_required, 'dispatch')
class Home(ListView):
    model = Compania
    template_name = 'compania_home.html'
    paginate_by = 10


@method_decorator(login_required, 'dispatch')
class CompaniaUpdate(UpdateView):
    model = Compania
    form_class = CompaniaUpdateForm
    success_url = reverse_lazy('compania-list')
    template_name = 'compania_update.html'


@method_decorator(login_required, 'dispatch')
class CompaniaList(ListView):
    model = Compania
    template_name = 'compania_list.html'


@method_decorator(login_required, 'dispatch')
class CompaniaCreate(CreateView):
    model = Compania
    form_class = CompaniaUpdateForm
    template_name = 'compania_update.html'
    success_url = reverse_lazy('compania-list')


@method_decorator(login_required, 'dispatch')
class CompaniaDetail(DetailView):
    model = Compania
    template_name = 'compania_detail.html'


@method_decorator(login_required, 'dispatch')
class CompaniaDelete(DeleteView):
    model = Compania
    template_name = 'compania_confirm_delete.html'
    success_url = reverse_lazy('compania-list')


@login_required
def add_all_companies(request):
    return redirect('compania-home')
