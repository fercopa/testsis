from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory
from mixer.backend.django import mixer
from .. import views
from ..models import Compania
import pytest
pytestmark = pytest.mark.django_db


def anonymous_get(view, *args):
    req = RequestFactory().get('/')
    req.user = AnonymousUser()
    pk = args[0] if args else None
    return view(req, pk=pk)


def superuser_get(view, *args):
    user = mixer.blend('auth.User', is_superuser=True)
    req = RequestFactory().get('/')
    req.user = user
    pk = args[0] if args else None
    return view(req, pk=pk)


class TestCompaniaHome:
    def test_anonymous_get(self):
        resp = anonymous_get(views.Home.as_view())
        assert 'login' in resp.url

    def test_superuser_get(self):
        resp = superuser_get(views.Home.as_view())
        assert resp.status_code == 200


class TestCompaniaList:
    def test_anonymous_get(self):
        resp = anonymous_get(views.CompaniaCreate.as_view())
        assert 'login' in resp.url

    def test_superuser_get(self):
        resp = superuser_get(views.CompaniaCreate.as_view())
        assert resp.status_code == 200


class TestCompaniaCreate:
    def test_anonymous_get(self):
        resp = anonymous_get(views.CompaniaCreate.as_view())
        assert 'login' in resp.url

    def test_superuser_get(self):
        resp = superuser_get(views.CompaniaCreate.as_view())
        assert resp.status_code == 200

    def test_anonymous_post(self):
        data = {'codigo': 292}
        req = RequestFactory().post('/', data=data)
        req.user = AnonymousUser()
        resp = views.CompaniaCreate.as_view()(req)
        assert 'login' in resp.url

    def test_superuser_post(self):
        user = mixer.blend('auth.User', is_superuser=True)
        data = {'codigo': 292}
        req = RequestFactory().post('/', data=data)
        req.user = user
        resp = views.CompaniaCreate.as_view()(req)
        assert resp.status_code == 302
        assert Compania.objects.all().count() == 1


class TestCompaniaUpdate:
    def test_anonymous_get(self):
        cia = mixer.blend(Compania)
        resp = anonymous_get(views.CompaniaUpdate.as_view(), cia.pk)
        assert 'login' in resp.url

    def test_superuser_get(self):
        cia = mixer.blend(Compania)
        resp = superuser_get(views.CompaniaUpdate.as_view(), cia.pk)
        assert resp.status_code == 200

    def test_anonymous_update(self):
        cia = mixer.blend(Compania, codigo=292)
        data = {'codigo': 567}
        req = RequestFactory().post('/', data=data)
        req.user = AnonymousUser()
        resp = views.CompaniaUpdate.as_view()(req, pk=cia.pk)
        cia.refresh_from_db()
        assert 'login' in resp.url
        assert cia.codigo == 292

    def test_superuser_update(self):
        user = mixer.blend('auth.User', is_superuser=True)
        cia = mixer.blend(Compania, codigo=292)
        data = {'codigo': 567}
        req = RequestFactory().post('/', data=data)
        req.user = user
        resp = views.CompaniaUpdate.as_view()(req, pk=cia.pk)
        cia.refresh_from_db()
        assert cia.codigo != 292
        assert resp.status_code == 302


class TestCompaniaDetail:
    def test_anonymous_get(self):
        cia = mixer.blend(Compania)
        resp = anonymous_get(views.CompaniaDetail.as_view(), cia.pk)
        assert 'login' in resp.url

    def test_superuser_get(self):
        cia = mixer.blend(Compania)
        resp = superuser_get(views.CompaniaDetail.as_view(), cia.pk)
        assert resp.status_code == 200


class TestCompaniaDelete:
    def test_anonymous_get(self):
        cia = mixer.blend(Compania)
        resp = anonymous_get(views.CompaniaDelete.as_view(), cia.pk)
        assert 'login' in resp.url

    def test_superuser_get(self):
        cia = mixer.blend(Compania)
        resp = superuser_get(views.CompaniaDelete.as_view(), cia.pk)
        assert resp.status_code == 200

    def test_anonymous_post(self):
        cia = mixer.blend(Compania)
        req = RequestFactory().post('/', args=(cia.pk, ))
        req.user = AnonymousUser()
        resp = views.CompaniaDelete.as_view()(req, pk=cia.pk)
        assert 'login' in resp.url

    def test_superuser_post(self):
        user = mixer.blend('auth.User')
        cia = mixer.blend(Compania)
        req = RequestFactory().post('/', args=(cia.pk, ))
        req.user = user
        resp = views.CompaniaDelete.as_view()(req, pk=cia.pk)
        assert resp.status_code == 302
        assert Compania.objects.all().count() == 0
