from mixer.backend.django import mixer
import pytest
pytestmark = pytest.mark.django_db


class TestCompania:
    def test_model(self):
        cia = mixer.blend('compania.Compania')
        assert cia.pk == 1

    def test_get_absolute_url(self):
        cia = mixer.blend('compania.Compania')
        url = cia.get_absolute_url()
        assert str(cia.pk) in url

    def test_get_nombre(self):
        cia = mixer.blend('compania.Compania', codigo=292)
        nombre = cia.get_nombre()
        assert '9 DE JULIO' in nombre

    def test_get_nonexistent_name(self):
        cia = mixer.blend('compania.Compania', codigo=99999)
        nombre = cia.get_nombre()
        assert nombre == ''

    def test_get_nombre_corto(self):
        cia = mixer.blend('compania.Compania', codigo=292)
        nombre_corto = cia.get_nombre_corto()
        assert 'SOC COOP' in nombre_corto

    def test_get_nonexistent_name_corto(self):
        cia = mixer.blend('compania.Compania', codigo=99999)
        nombre_corto = cia.get_nombre_corto()
        assert nombre_corto == ''
