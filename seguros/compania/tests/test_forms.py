import pytest
from .. import forms

pytestmark = pytest.mark.django_db


class TestCreateForm:
    def test_post_form_empty(self):
        form = forms.CompaniaUpdateForm(data={})
        valid = form.is_valid()
        assert valid is False

    def test_post_form_with_data(self):
        data = {'codigo': 292}
        form = forms.CompaniaUpdateForm(data=data)
        valid = form.is_valid()
        assert valid is True
