from django import forms
from . import models


class CompaniaUpdateForm(forms.ModelForm):
    class Meta:
        model = models.Compania
        fields = '__all__'
        widgets = {'instructivo_descarga': forms.Textarea(
            attrs={'class': 'materialize-textarea'}),
            'instructivo_recupero': forms.Textarea(
                attrs={'class': 'materialize-textarea'})
        }
