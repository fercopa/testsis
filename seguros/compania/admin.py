from django.contrib import admin

from .models import Compania


class CompaniaAdmin(admin.ModelAdmin):
    list_display = ['codigo', 'get_nombre', 'es_descargable',
                    'descarga_automatizada', 'portal_web']
    search_fields = ('codigo',)


admin.site.register(Compania, CompaniaAdmin)
