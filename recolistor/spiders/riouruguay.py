# -*- coding: utf-8 -*-
import scrapy
import json
import requests
import codecs


from recolistor.spiders.compania import *


def format_allianz_fecha(fecha):
    date = (str(fecha).split(" ")[0].split("-"))
    date.reverse()
    return "/".join(date)


class RiouruguaySpider(CompaniaSpider):
    allowed_host = "http://ru5.com.ar"
    name = 'riouruguay'
    start_urls = ["http://ru5.com.ar/servlet/index.jsp"]

    def parse(self, response):
        self.get(start_urls[0])



    def get_polizas_and_cobranzas(self):
        # informes
        time.sleep(5)
        self.get("https://mb.allianznet.com.ar/allianz/extranet/REG_PRODUCTORES_OPERACIONES.consultas_operaciones_par")
        self.find(By.NAME, "P_FECHA_DESDE", wait=True, time=100).clear()
        self.find(By.NAME, "P_FECHA_DESDE", wait=True).send_keys(format_allianz_fecha(self.date_from))
        self.find(By.NAME, "P_FECHA_HASTA", wait=True).clear()
        self.find(By.NAME, "P_FECHA_HASTA", wait=True).send_keys(format_allianz_fecha(self.date_to))
        elem = self.find(By.NAME, "procesar", wait=True)
        self.click(elem)

        self.switch_to_window(self.driver.window_handles[1])
        self.wait(By.XPATH,  "//img[@src='/iconos/txt.gif']", time=10)
        self.click(self.find_all(By.XPATH, "//img[@src='/iconos/txt.gif']")[1])
        time.sleep(5)

        self.get("https://mb.allianznet.com.ar/allianz/extranet/REG_PRODUCTORES_COBRANZAS.consultas_operaciones_par")
        self.find(By.NAME, "P_FECHA_DESDE", wait=True, time=100).clear()
        self.find(By.NAME, "P_FECHA_DESDE", wait=True).send_keys(format_allianz_fecha(self.date_from))
        self.find(By.NAME, "P_FECHA_HASTA", wait=True).clear()
        self.find(By.NAME, "P_FECHA_HASTA", wait=True).send_keys(format_allianz_fecha(self.date_to))
        elem = self.find(By.NAME, "procesar", wait=True)
        self.click(elem)
        self.switch_to_window(self.driver.window_handles[1])
        self.wait(By.XPATH,  "//img[@src='/iconos/txt.gif']", time=10)
        self.click(self.find_all(By.XPATH, "//img[@src='/iconos/txt.gif']")[1])
        time.sleep(5)
