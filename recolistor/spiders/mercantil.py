# -*- coding: utf-8 -*-
# pylint: disable-all
import scrapy


from recolistor.spiders.compania import *

def format_mercantil_fecha(fecha):
    date = (str(fecha).split(" ")[0].split("-"))
    return ".".join(date)


LIBROS = {
    #'impresos_operaciones': "A",
    #'impresos_cobranzas': "B",
    'digitales_operaciones': "C",
    'digitales_cobranzas': "D",
}


class MercantilSpider(CompaniaSpider):
    name = 'mercantil'
    allowed_host = "https://gestionpas.mercantilandina.com.ar/"
    login_url = "https://gestionpas.mercantilandina.com.ar/sigma/entry"
    start_urls = [login_url]

    def is_logged(self):
        self.wait(By.CLASS_NAME, "salir")
        return self.find(By.CLASS_NAME, "salir") is not None

    def login(self):
        self.get(self.login_url)
        # user
        self.find(By.NAME, "in_522_10", wait=True).send_keys(self.user)
        # password
        self.find(By.NAME, "in_602_10").send_keys(self.password)
        self.find(By.XPATH, "//body").send_keys(Keys.RETURN)

    def download(self, response):
        return scrapy.FormRequest.from_response(
            response,
            formdata={"SESSIONNUMBER":"", "PERF_TIMESTAMP":"0",
            "COMMAND":"[enter]", "CURSORPOSITION":"607",
            "KeyboardToggle":"1", "SESSIONID":"INVALID","CARETPOS":"607",
            'in_522_10': self.user, 'in_602_10': self.password,
            "LINESIZE":"2", "ISCURSORDECR":"false"},
            callback=self.verifica_login
        )

    def verifica_login(self, response):
        '''.'''
        # Verifica el login antes de continuar
        if "nombre del usuario" in response.body and "son incorrectos" in response.body:
            self.logger.error("Usuario o contrasena incorrecta")

        else:
            # self.logger.error("Contrasena Exitosa")
            self.logger.error('Verificación de usuario y contrasena exitoso')
            # Por ahora solo me interesa saber si inicio sesion bien
            # y devuelvo True nada mas cuando se implemente el resto
            # se saca el pass y se deja continuar
            pass

            # self.login()
            # self.get_polizas_and_cobranzas()
            # time.sleep(5)
            # self.zip()

    def get_polizas_and_cobranzas(self):
        self.find(By.LINK_TEXT, "Informes", wait=True).click()
        # informes por cuenta
        elem = self.find_all(By.CLASS_NAME, "HATSDEFRENDER", wait=True)[0]
        self.click(elem)

        for tipo_libro, codigo_libro in LIBROS.items():
            self.select_options(codigo_libro)

    def select_options(self, codigo_libro):
        tipo_informe = Select(self.find(By.NAME, "in_482_1", wait=True))
        tipo_informe.select_by_value(codigo_libro)

        self.find(By.NAME, "in5E_724_11", wait=True).click()
        self.find(By.NAME, "in_895_10", wait=True).send_keys(format_mercantil_fecha(self.date_from))
        self.find(By.NAME, "in_932_10").send_keys(format_mercantil_fecha(self.date_to))

        # generar
        elem = self.find_all(By.CLASS_NAME, "ApplicationButton", wait=True)[0]
        self.click(elem)
        # descargar
        elem = self.find_all(By.CLASS_NAME, "verde", wait=True)[1]
        self.click(elem)
        # volver
        elem = self.find(By.LINK_TEXT, "Volver", wait=True)
        self.click(elem)
