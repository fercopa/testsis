# -*- coding: utf-8 -*-
import datetime
import calendar
import time

from recolistor.spiders.compania import *


class RivadaviaSpider(CompaniaSpider):
    name = 'rivadavia'
    allowed_host = "http://www.segurosrivadavia.com/institucional/"
    login_url = "http://www.segurosrivadavia.com/institucional/login-usuarios.php?tipo_usu=P"
    start_urls = [login_url]
    downloads = 0

    def download(self, response):
        self.find(By.NAME, "Aceptar").click()
        self.get_polizas()
        self.zip()

    def login(self):
        self.get(self.login_url)
        # user
        self.find(By.NAME, "usuario").send_keys(self.user)
        # id
        id1, id2, id3 = self.password2.split('-')
        self.find(By.NAME, "cuit").send_keys(id1)
        self.find(By.NAME, "cuit2").send_keys(id2)
        self.find(By.NAME, "cuit3").send_keys(id3)
        # password
        self.find(By.NAME, "password").send_keys(self.password)
        # confirmar
        self.click(self.find(By.ID, "ingresar"))
        time.sleep(3)
        self.main_frame()

    def is_logged(self):
        text_ok = "Seleccione Número de Productor"
        return self.find(By.XPATH, self.xpath_contains(text_ok)) is not None

    def get_polizas(self):
        # herramientas
        i = 0
        cant_meses = len(list(self.iter_months()))
        for month,year in self.iter_months():
            self.get("https://www.sistemas.segurosrivadavia.com/Libros_Rubricados_Digital.php")
            # fechas
            if i == 0:
                fecha = datetime.date(year, month, int(self.date_from.strftime("%d/%m/%Y").split("/")[0]))
            else:
                fecha = datetime.date(year, month, 1)
            time.sleep(3)
            self.set_input(By.NAME, "fecha_desde", fecha.strftime("%d/%m/%Y"))
            if i == cant_meses:
                fecha = datetime.date(year, month, int(self.date_to.strftime("%d/%m/%Y").split(" ")[0]))
            else:
                fecha = datetime.date(year, month, calendar.monthrange(year, month)[1])
            self.set_input(By.NAME, "fecha_hasta", fecha.strftime("%d/%m/%Y"))
            self.find(By.NAME,"radio").send_keys(Keys.DOWN)
            self.find(By.NAME, "orden_inicial").send_keys("1")
            time.sleep(1)
            self.click(self.find(By.NAME, "aceptar"))
            time.sleep(10)
            self.wait(By.ID, "Layer1")
            links = self.find_all(By.XPATH, "//a[@class='text']")
            for link in links:
                self.click(link)
                time.sleep(10)
