# -*- coding: utf-8 -*-
import scrapy
import json
import requests
import codecs
import datetime
import calendar
import os

from recolistor.spiders.compania import *

class SanCristobalSpider(CompaniaSpider):
    name = 'sancristobal'
    allowed_host = "https://www2.sancristobal.com.ar"
    login_url = "http://www2.sancristobal.com.ar/PortalSCNET/Productores/Login/Loguin.aspx?AspxAutoDetectCookieSupport=1"
    start_urls = [login_url]

    def download(self, response):
        self.get_polizas_cobranzas()
        self.zip()

    def login(self):
        self.get(self.login_url)
        # 20224158530 -  oliasoc523
        # user
        self.find(By.ID, "AccPanelLogin_content_TxtUsuario").send_keys(self.user)
        # password
        self.find(By.ID, "AccPanelLogin_content_TxtContraseña").send_keys(self.password)
        # confirmar
        elem = self.find(By.ID, "AccPanelLogin_content_BtnIngresar")
        self.click(elem)

    def is_logged(self):
        text_ok = "Bienvenido!"
        self.wait(By.XPATH, self.xpath_contains(text_ok))
        return self.find(By.XPATH, self.xpath_contains(text_ok)) is not None

    def get_polizas_cobranzas(self):
        #libros
        elem = (self.find(By.XPATH, "//a[@id='MenuBotomLeft1_HlkLibros']"))
        self.click(elem)
        time.sleep(2)

        self.driver.switch_to_window(self.driver.window_handles[1])
        time.sleep(2)
        select_productores =  Select(self.find(By.ID, 'ctl00_ContentPlaceHolder1_ctl01_OrganizadorProductor1_CboProductores'))

        for productor in range(len(select_productores.options)):
            select_productores.select_by_index(productor)

            #Saco Cobranzas
            for month,year in self.iter_months():
                select_meses =  Select(self.find(By.ID, 'ctl00_ContentPlaceHolder1_ctl01_DdlFechas'))
                value = str(year).zfill(4) + str(month).zfill(2)
                select_meses.select_by_value(value)
                time.sleep(1)
                self.wait(by=By.XPATH, value="//div[@id='ctl00_UpdateProgress1'][@aria-hidden='true']", time=60)
                elem = (self.find(By.XPATH, "//a[@id='ctl00_ContentPlaceHolder1_ctl01_HypLibros']"))
                self.click(elem)
                time.sleep(2)
                self.driver.switch_to_window(self.driver.window_handles[1])
            #Saco Operaciones
            for month,year in self.iter_months():
                select_meses =  Select(self.driver.find(By.ID, 'ctl00_ContentPlaceHolder1_ctl01_DdlFechasOperaciones'))
                value = str(year).zfill(4) + str(month).zfill(2)
                select_meses.select_by_value(value)
                time.sleep(1)
                self.wait(by=By.XPATH, value="//div[@id='ctl00_UpdateProgress1'][@aria-hidden='true']", time=60)
                elem = (self.find(By.XPATH, "//a[@id='ctl00_ContentPlaceHolder1_ctl01_HlkOperaciones']"))
                self.click(elem)
                time.sleep(2)
                self.driver.switch_to_window(self.driver.window_handles[1])

            cant_meses = len(list(self.iter_months()))
            i = 0
            #itero a traves de los meses
            for month,year in self.iter_months():
                #si es el primer mes , pongo el día que corresponde, en caso contrario, tomo el mes desde el principio
                if i == 0:
                    fecha = datetime.date(year, month, int(self.date_from.strftime("%d/%m/%Y").split("/")[0]))
                else:
                    fecha = datetime.date(year, month, 1)
                time.sleep(3)
                self.find(By.ID, "ctl00_ContentPlaceHolder1_ctl01_TxtFechaDesdeCobranzas").clear()
                self.set_input(By.ID, "ctl00_ContentPlaceHolder1_ctl01_TxtFechaDesdeCobranzas", fecha.strftime("%d%m%Y"))
                if i == cant_meses - 1:
                    fecha = datetime.date(year, month, int(self.date_to.strftime("%d/%m/%Y").split("/")[0]))
                else:
                    fecha = datetime.date(year, month, calendar.monthrange(year, month)[1])
                self.find(By.ID, "ctl00_ContentPlaceHolder1_ctl01_TxtFechaHastaCobranzas").clear()
                self.set_input(By.ID, "ctl00_ContentPlaceHolder1_ctl01_TxtFechaHastaCobranzas", fecha.strftime("%d%m%Y"))
                # exportar en excel y xml
                self.click(self.find(By.ID, "ctl00_ContentPlaceHolder1_ctl01_LinkLibrosCobranzasXML"))
                time.sleep(10)
                i += 1

            i = 0
            #itero a traves de los meses
            for month,year in self.iter_months():
                #si es el primer mes , pongo el día que corresponde, en caso contrario, tomo el mes desde el principio
                if i == 0:
                    fecha = datetime.date(year, month, int(self.date_from.strftime("%d/%m/%Y").split("/")[0]))
                else:
                    fecha = datetime.date(year, month, 1)
                time.sleep(3)
                self.find(By.ID, "ctl00_ContentPlaceHolder1_ctl01_TxtFechaDesdeOperaciones").clear()
                self.set_input(By.ID, "ctl00_ContentPlaceHolder1_ctl01_TxtFechaDesdeOperaciones", fecha.strftime("%d%m%Y"))
                if i == cant_meses - 1:
                    fecha = datetime.date(year, month, int(self.date_to.strftime("%d/%m/%Y").split("/")[0]))
                else:
                    fecha = datetime.date(year, month, calendar.monthrange(year, month)[1])
                self.find(By.ID, "ctl00_ContentPlaceHolder1_ctl01_TxtFechaHastaOperaciones").clear()
                self.set_input(By.ID, "ctl00_ContentPlaceHolder1_ctl01_TxtFechaHastaOperaciones", fecha.strftime("%d%m%Y"))
                # exportar en excel y xml
                self.click(self.find(By.ID, "ctl00_ContentPlaceHolder1_ctl01_LinkLibrosOperacionesXML"))
                time.sleep(10)
                i += 1
