# -*- coding: utf-8 -*-
import scrapy
import json
import requests
import codecs


from recolistor.spiders.compania import *


UN_DIA = 2
UNA_PAGINA = 1
TODOS = 0
TOTAL_ATTEMPS = 3
DEBUG_LEVEL = 1
STATE_ACTION = ['inicializando', 'obteniendo_urls', 'obteniendo_poliza', 'obteniendo_riesgos', 'escribiendo_a_csv']
NO = 0
SI = 1


class NacionSpider(CompaniaSpider):
    name = 'nacion'
    allowed_host = "https://nsd.nacion-seguros.com.ar/"
    login_url = "https://nsd.nacion-seguros.com.ar/productor/"
    start_urls = [login_url]

    def login(self):
        self.get(self.login_url)

        # maximizamos la ventana, tira un problema si la ventana no esta maximizada.
        self.driver.maximize_window()
        # Le damos 1 segundo de espera para que puede llegar a demorar.
        time.sleep(1)

        # 29255010  -  lozano2028
        # user
        self.find(by=By.NAME, value="_LOGIN", wait=True).send_keys(self.user)
        # password
        self.find(by=By.NAME, value="_PASSWEB", wait=True).send_keys(self.password)
        # confirmar
        self.click(self.find(by=By.NAME,value="BTNCONFIRM", wait=True))

        # cerrar publicidades
        for ad in self.driver.find_elements_by_id("sbox-btn-close"):
            self.click(ad)

    def is_logged(self):
        self.wait(By.ID, "username")
        return self.find(By.ID, "username") is not None

    def download(self, response):
        codes = True
        self.polizas = []
        self.riesgos = []
        self.cobranzas = []
        self.state['compania'] = self.name
        self.state['termino'] = NO
        # codigos de visbroker
#        for codigo in [52580, 47864, 61313, 58478, 55150, 52048]:
        self.codigos_procesados = []
        if codes:
            for codigo in  [47864, 52580, 61313, 58478, 55150, 52048]:
                self.login()

                self.driver.get("https://nsd.nacion-seguros.com.ar/comunicaciones.html")
                self.state['codigo'] = codigo

                try:
                    self.get_polizas(codigo)
                    self.polizas_t = self.polizas
                    self.riesgos_t = self.riesgos
                    self.codigos_procesados.append(codigo)
                    error = 0
                except:
                    #log.error("Excepcion ")
                    #log.error("El programa se cerrara, con los siguientes códigos procesados: %s", str(self.codigos_procesados))
                    #log.error("Se guardaran los codigos procesados hasta ahora...")
                    error = 1

            if error:
                self.logger.error("Error, se corto la descarga")
                self.save_to_csv("polizas.csv", self.polizas_t)
                self.save_to_csv("riesgos.csv", self.riesgos_t)
        else:
            self.login()

            self.driver.get("https://nsd.nacion-seguros.com.ar/comunicaciones.html")

            try:
                self.get_polizas()
                self.polizas_t = self.polizas
                self.riesgos_t = self.riesgos
                error = 0
            except:
                #log.error("Excepcion ")
                #log.error("El programa se cerrara, con los siguientes códigos procesados: %s", str(self.codigos_procesados))
                #log.error("Se guardaran los codigos procesados hasta ahora...")
                error = 1

        if error:
            self.logger.error("Error, se corto la descarga")
            self.save_to_csv("polizas.csv", self.polizas_t)
            self.save_to_csv("riesgos.csv", self.riesgos_t)
        self.state['action'] = STATE_ACTION[4]
        # save files
        self.save_to_csv("polizas.csv", self.polizas)
        self.save_to_csv("riesgos.csv", self.riesgos)
        self.save_to_csv("cobranzas.csv", self.cobranzas)

        # zip files
        self.zip()
        self.state['termino'] = SI

    def get_polizas(self, codigo=None):
        # pólizas
        self.state['action'] = STATE_ACTION[1]

        btn_polizas = self.find(By.LINK_TEXT, "Pólizas", wait=True)
        if not btn_polizas:
            return
        self.click(btn_polizas)
        self.frame("")
        # productor
        if codigo:
            self.set_input(By.NAME, "formf:sproductor", codigo)

        self.click(self.find(By.ID, "formf:fperiodos_label"))
        self.click(self.find(By.CSS_SELECTOR, "#formf\:fperiodos_panel * li:nth-child(2)"))

        if DEBUG_LEVEL >= UN_DIA:
            self.date_to = self.date_from

        iteration_polizas = []
        self.frame("")

        # fechas
        self.set_input(By.ID, "formf:cldesde", self.date_from.strftime("%d/%m/%Y"))
        self.set_input(By.ID, "formf:clhasta", self.date_to.strftime("%d/%m/%Y"))
        # buscar
        self.click(self.find(By.ID, "formf:botonBuscar"))

        #Espero que desaparezca el "buscando". La tabla "nueva" siempre se carga antes de que desaparezca.
        self.wait(time=10,function=EC.presence_of_element_located, by=By.XPATH, value="//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-shadow ui-overlay-hidden']")

        # recorro todas las paginas
        last = False
        flag = False
        no_hay_datos = "No se encontraron datos"
        if no_hay_datos in self.driver.page_source:
            last = True
        #import ipdb; ipdb.set_trace()
        while not last:

            #Si no estoy en la ultima pagina
            if flag:
                actual_page += 1
                string_page = '(' + str(actual_page) + " of " + str(last_page) + ')'
                self.wait(time=100, function=EC.text_to_be_present_in_element, by=By.XPATH, value="//span[@class='ui-paginator-current']", value2=string_page)

            detalle_polizas = self.find_all(By.XPATH, "//img[@title='Ver Detalle']", wait=True, time=40, ignore_error=True)

            if not detalle_polizas:
                break

            #import ipdb; ipdb.set_trace()
            for detalle in detalle_polizas:
               #import ipdb; ipdb.set_trace()
                # me voy al padre del detalle, para ver el href
                url = detalle.find_element_by_xpath("..").get_attribute("href")
                # voy al renglon de la tabla para buscar mas datos
                tr = detalle.find_element_by_xpath("../../..") #.find_element_by_xpath("..").find_element_by_xpath("..")
                # nro de poliza
                nro = tr.find_element_by_xpath("td[2]/span").text
                # rama
                rama = tr.find_element_by_xpath("td[4]/span").text
                fecha = tr.find_element_by_xpath("td[7]/span").text
                poliza = {'url': url, 'poliza_nro': nro, 'rama': rama, 'fecha': fecha}

                # guardo la poliza en el conjunto total de polizas.
                self.polizas.append(poliza)

                # guardo la poliza en el conjunto de polizas de ESTA iteración sobre el codigo.
                # (Se podría ver de que lo que se guarde en self.polizas sea un par  (poliza,es_de_esta_iteracion)
                # pero es añadirle mas complejidad)
                iteration_polizas.append(poliza)

          #tomo el texto de la forma (x of y) y veo si estoy en la ultima página
            paginator_text = self.find(By.XPATH, "//span[@class='ui-paginator-current']", wait=True).text.replace('(','').replace(')','')
            #obtengo la pagina actual y la ultima
            actual_page, last_page = int(paginator_text.split('of')[0]),int(paginator_text.split('of')[1])
            if actual_page >= last_page:
                #Estoy en la ultima pagina
                last = True
            else:
                #bandera de "Tengo que pasar de página"
                flag = True

            # pasar a pagina siguiente
            if not last:
                self.click(self.find(By.XPATH, "//span[@class='ui-paginator-next ui-state-default ui-corner-all']", wait=True))

#                import ipdb; ipdb.set_trace()
            #if DEBUG_LEVEL >= UNA_PAGINA:
            #    last = True

        if self.error:
            try:
                self.logger.error("Hubo un error obteniendo las polizas del codigo" + str(codigo) + "el día " + str(date))
            except:
                self.logger.error("Hubo un error obteniendo las polizas del día " + str(date))

            self.error = 0

        for poliza in iteration_polizas:
            success = False
            while not success:
                try:
                    self.open_poliza(poliza)
                    self.state['fecha'] = poliza['fecha']
                    success = True
                except PopupBlocked:
                    self.close()
                    self.login()
        self.driver.close()


    def open_poliza(self, poliza):
        # abro un nuevo tab con la poliza
        self.main_frame()
        self.state['action'] = STATE_ACTION[1]

        self.find(By.TAG_NAME, 'body').send_keys(Keys.CONTROL + 't')
        self.poliza_window = self.driver.current_window_handle
        self.driver.switch_to_window(self.poliza_window)
        with wait_for_page_load(self.driver):
            self.driver.get(poliza['url'])

        # guardo los datos de la poliza
        poliza.update(self.get_table2(["Label"]))
        self.state['action'] = STATE_ACTION[3]

        self.get_riesgos(poliza)
        #self.get_cobranzas(poliza)

        # cerrar el tab y volver al anterior
        self.find(By.TAG_NAME, 'html').send_keys(Keys.CONTROL + 'w')
        self.main_frame()
        self.frame("")


    def get_riesgos(self, poliza):
        # RIESGOS
        self.click(self.find(By.XPATH, "//a[@id='Tab_TAB1Containerpanel3']", wait=True))
        self.wait(time=100, function=EC.element_to_be_clickable, by=By.ID, value="W0051vGR1VERRIESGO_0001")
        # get riesgos
        i = 1
        for item in self.find_all(By.XPATH, "//img[@title='Ver Riesgo']", wait=True, time=20):
            time.sleep(3)
            # me voy al padre del detalle y hago click para abrir el riesgo
            # guardar los datos del riesgo
            self.click(self.find(By.XPATH, "//img[@id='W0051vGR1VERRIESGO_00"+ str(i).zfill(2) + "']"))

            #tarda un rato en "estar" disponible
            time.sleep(3)
            #aunque solo la primera vez que hace eso en el navegador- extraño - REVISAR.
            try:
                self.click(self.find(By.XPATH, "//img[@id='W0051vGR1VERRIESGO_00"+ str(i).zfill(2) + "']"))
            except AttributeError:
                pass
            #Espero hasta que aparezca la lupa celeste, lo que significa que la poliza ya esta cargada
            self.wait(time=100, function=EC.presence_of_element_located, by=By.ID, value="vGR1MOVIM_0001")

            #obtengo todos los datos
            #Este lista_attrs tiene los "class" que tienen los <span> que son "labels" de un dato que queremos obtener
            #Puede que sea una lista incompleta.
            lista_attrs = ["NSD_TXT_ATRIBUTOESTANDARLISTATRN","NSD_TXT_ATRIBUTOESTANDAR"]
            item = self.get_table(lista_attrs)
            item["poliza_nro"] = poliza["poliza_nro"]
            self.riesgos.append(item)
            self.driver.back()

            i += 1

        # volver a la poliza
        self.driver.back()


    """def get_cobranzas(self, poliza):
        # COBRANZAS

        #cobranzas_button = self.find(By.CSS_SELECTOR, "#W0004LBLCOBRANZAS a")
        self.click(self.find(By.XPATH, "//a[@id='Tab_TAB1Containerpanel4']"))

        # get cobranzas
        for item in self.find_all(By.XPATH, "//table[@id='W0059CONTENT_TABLE_GR1PRINCIPAL']/tbody/tr", wait=True):
            # si no es el header

            pago = {}
            pago["poliza_nro"] = poliza["poliza_nro"]
            pago["comprobante"] = item.find_element_by_xpath("td[1]/span").text
            pago["fecha"] = item.find_element_by_xpath("td[2]/span").text
            pago["importe"] = item.find_element_by_xpath("td[4]/span").text
            pago["endoso"] = item.find_element_by_xpath("td[5]/span").text
            pago["forma_de_pago"] = item.find_element_by_xpath("td[6]/span").text
            self.cobranzas.append(pago)
            # volver a la poliza
        self.driver.back()
"""

    """ parsear una tabla en la que los nombres de los campos tienen la/s clase/s que esta en list_of_keywords  """

    def get_table(self, list_of_keywords):
        data = {}
        key = None
        for span in self.find_all(By.TAG_NAME, "span", wait=True):
            if span.get_attribute("class") in list_of_keywords:
                key = (u"%s" % (span.text))
                if not key in data:
                    data[key] = ""
            elif key != None and key.strip() != "":
                if data[key] != "":
                    data[key] += " "
                data[key] += (u"%s" % (span.text))
                key = None
        return data

        """ la idea es poder generalizar la llamadas a los Explicit_Wait para
        poder hacer un buen manejo de errores sin añadir tantas lineas de código"""

    def get_table2(self, list_of_keywords):
        data = {}
        key = None
        tab = self.find(By.XPATH, "//div[@id='TABPAGE1TABLE']")
        for row in tab.find_elements_by_xpath("//div[@class='form-group gx-form-group']"):
            key = None
            value = None
            try:
                key = row.find_element_by_xpath(".//label[@class='gx-label col-sm-3 NSD_ATT_ESTANDARLabel control-label Label']")
            except:
                try:
                    key = row.find_element_by_xpath(".//label[@class='gx-label col-sm-3 Label Label Label Label Label Label NSD_ATT_ESTANDARLabel control-label Label']")
                except:
                    pass
            try:
                value = row.find_element_by_xpath(".//span[@class='ReadonlyNSD_ATT_ESTANDAR']")
            except:
                pass

            if key and value:
                key = (u"%s" % (key.text))
                value = (u"%s" % (value.text))
                data[key] = value
        return data
