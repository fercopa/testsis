# -*- coding: utf-8 -*-
import scrapy
import json
import requests
import codecs
import logging


from recolistor.spiders.compania import *

STATE_ACTION = ['inicializando', 'obteniendo_urls', 'obteniendo_poliza', 'obteniendo_riesgos', 'escribiendo_a_csv']
categorias = [12, 4, 18, 16, 12, 3, 79, 80, 23, 2, 9, 20, 21, 5, 8, 10, 22, 19, 27, 11, 33, 8, 15, 6, 26, 13, 15, 24, 1, 17, 14, 7]
CATEGORIA_AUTO = 4

def format_mapfre_fecha(fecha):
    date = reversed(str(fecha).split(" ")[0].split("-"))
    return "".join(date)

class NoDataException(Exception):
    pass


class MapfreSpider(CompaniaSpider):
    name = 'mapfre'
    allowed_host = "https://web2.mapfre.com.ar/mapfrenet/"
    login_url = "https://web2.mapfre.com.ar/mapfrenet/login.aspx"
    start_urls = [login_url]

    def login(self):
        self.get(self.login_url)

        # user
        self.find(By.NAME, "ctl00$body$usuario", wait=True).send_keys(self.user)
        time.sleep(2)
        # password
        self.find(By.NAME, "ctl00$body$clave", wait=True).send_keys(self.password)
        # login
        self.find(By.NAME, "ctl00$body$btSend").click()
        self.frame("MM")

    def is_logged(self):
        text_ok = "tituloMenu"
        return self.find(By.ID, text_ok) is not None

    def download(self, response):
        # polizas
        time.sleep(2)
        self.find(By.PARTIAL_LINK_TEXT, "PÓLIZAS", wait=True).click()
        # libros
        self.find(By.PARTIAL_LINK_TEXT, "Copia PAS / Organizador", wait=True).click()
        self.main_frame()
        self.frame("MC")


        # Usuario "5806"
        # Clave " 2277 " o si la debemos cambiar es "7722"
        self.polizas = []
        self.riesgos = []
        self.cobros = []
        # user

        for categoria in categorias:
            try:
                self.download_polizas(categoria)
            except NoDataException:
                self.driver.back()
                continue
            self.init(response)

        self.save_to_csv("polizas.csv", self.polizas)
        self.save_to_csv("riesgos.csv", self.riesgos)
        self.save_to_csv("cobros.csv", self.cobros)

        self.zip()

    def download_polizas(self, categoria):

        list_of_keywords = []
        try:
            self.frame("MC")
        except NameError:
            pass

        tipo_libro = Select(self.find(By.ID, "ctl00_body_SelectorSec", wait=True))
        tipo_libro.select_by_value(str(categoria))
        #Introduzco la fecha
        self.set_input(By.NAME, "ctl00$body$FecDesde$tbx", format_mapfre_fecha(self.date_from))
        self.set_input(By.NAME, "ctl00$body$FecHasta$tbx", format_mapfre_fecha(self.date_to))

        self.click(self.find(By.ID, "ctl00_body_btContinuar"))
        #Espero obtener las fechas.
        time.sleep(2)
        if self.find_all(By.XPATH, "//td[@class='mpMsg']") != []:
            raise NoDataException

        table = self.find(By.XPATH, "//div[@id='MAD']")
        #Obtengo los encabezados de los datos generales de la poliza.
        for key in table.find_elements_by_xpath("//tr[@class='HeaderStyle']/th[@scope='col']"):
            if key.text == u"P\xf3liza":
                list_of_keywords.append("poliza")
                continue
            list_of_keywords.append(key.text)

        number_of_pages = len(self.find_all(By.XPATH, "//tr[@class='PagerStyle']/td/table/tbody/tr/td"))
        #Reviso todas las paginas en busco de los datos generales de las polizas.
        if number_of_pages:
            for i in range(number_of_pages):
                if i == 0:
                    self.get_polizas_data(list_of_keywords, categoria)
                else:
                    number_of_page = self.find_all(By.XPATH, "//tr[@class='PagerStyle']/td/table/tbody/tr/td")
                    number_of_page = number_of_page[i]
                    link_to_number_of_page = number_of_page.find_element_by_xpath(".//a")
                    self.click(link_to_number_of_page)
                    self.get_polizas_data(list_of_keywords, categoria)
        else:
            self.get_polizas_data(list_of_keywords, categoria)
        
        self.get_polizas_content(categoria)
        time.sleep(2)

    def get_polizas_data(self, list_of_keywords, categoria):
        self.logger.error("Obteniendo pólizas: " + str(categoria))
        
        table = self.find(By.XPATH, "//div[@id='MAD']")
        #La table tiene 2 tipos diferentes de fila RowStyle y AlternetingRowStyle
        for row in table.find_elements_by_xpath("//tr[@class='RowStyle']"):
            data_col = row.find_elements_by_xpath(".//td")
            data = {}
            for i in range(len(data_col)):
                data[list_of_keywords[i]] = data_col[i].text
            url = row.find_element_by_xpath(".//td/a[@title='Pinche aquí para ver el detalle']").get_attribute("href")
            data['url'] = url
            data['categoria'] = categoria
            self.polizas.append(data)

        for row in table.find_elements_by_xpath("//tr[@class='AlternatingRowStyle']"):
            data_col = row.find_elements_by_xpath(".//td")
            data = {}
            for i in range(len(data_col)):
                data[list_of_keywords[i]] = data_col[i].text
            url = row.find_element_by_xpath(".//td/a[@title='Pinche aquí para ver el detalle']").get_attribute("href")
            data['url'] = url
            data['categoria'] = categoria
            self.polizas.append(data)

    def get_polizas_content(self, categoria):
        self.logger.error("Abriendo Polizas: " + str(categoria))
        i = 1
        for poliza in self.polizas:
            success = False
            while not success:
                try:
                    time.sleep(1)
                    self.open_poliza(poliza, categoria)
                    self.logger("Poliza " + str(i) + " de " + str(len(self.polizas)))
                    success = True
                    i += 1
                except PopupBlocked:
                    self.close()
                    self.init()
                    self.login()
        self.driver.close()

    def open_poliza(self, poliza, categoria):
        # abro un nuevo tab con la poliza
        self.driver.find_element_by_id("TC").send_keys(Keys.CONTROL + 't')

        self.state['action'] = STATE_ACTION[1]
        #pongo datos generales de la poliza.
        self.poliza_window = self.driver.current_window_handle
        self.driver.switch_to_window(self.poliza_window)
        with wait_for_page_load(self.driver):
            self.driver.get(poliza['url'])

        tabla = self.find(By.XPATH, "//div[@class='Agrupacion']")
        
        i = 0
        #sacar datos especificos de la poliza:
        for tr in (tabla.find_elements_by_xpath(".//tr")):
            if not i in [1,2]:
                poliza.update(self.get_table_poliza(tr))
            i += 1

        #sacar datos especificos de los riesgos:
        self.click(self.find(By.ID, 'ctl00_body_BtRiesgos'))
        time.sleep(1)
        lista_riesgos = self.find_all(By.XPATH, "//select[@name='ctl00$body$RiesgoVista$DDLNumeroRiesgo']/option")
        self.wait(by=By.NAME, value="ctl00$body$RiesgoVista$DDLNumeroRiesgo")
        list_of_values = [i.get_attribute('value') for i in lista_riesgos]
        for i in list_of_values:
            riesgos = Select(self.find(By.NAME, "ctl00$body$RiesgoVista$DDLNumeroRiesgo", wait=True))
            riesgo = riesgos.select_by_value(i)
            time.sleep(3)
            try:
                riesgo = self.get_riesgo_table()
            except:
                time.sleep(3)
                riesgo = self.get_riesgo_table()
            riesgo['poliza'] = poliza['poliza']
            
            #Sacar patente del auto.
            if categoria == CATEGORIA_AUTO:
                self.click(self.find(By.ID, "ctl00_body_RiesgoVista_TCMasDatos_TPDatosVariables_tab"))
                table = self.find(By.XPATH, "//table[@id='ctl00_body_RiesgoVista_TCMasDatos_TPDatosVariables_TabDatosVariables']")
                patente = table.find_elements_by_xpath(".//td")[19]
                a = patente.find_element_by_xpath(".//span").text
                riesgo['patente'] = a

            self.riesgos.append(riesgo)

        #sacar datos especificos de las cobranzas
        self.click(self.find(By.ID, 'ctl00_body_BtCuotas'))
        time.sleep(1)

        table = self.find(By.XPATH, "//div[@class='Agrupacion']")
        list_of_keywords = []
        for key in table.find_elements_by_xpath("//tr[@class='HeaderStyle']/th"):
            list_of_keywords.append(key.text)
 
        table = self.find(By.XPATH, "//div[@class='Agrupacion']")    
        for row in table.find_elements_by_xpath("//tr[@class='RowStyle']"):
            data_col = row.find_elements_by_xpath(".//td")
            data_col = [i.text for i in data_col]
            data = {}
            for i in range(len(data_col)-1):
                data[list_of_keywords[i+1]] = data_col[i+1]
            data['poliza'] = poliza['poliza']
            try:
                if data['Referencia'] is not u'':
                    self.cobros.append(data)
            except KeyError:
                continue

        for row in table.find_elements_by_xpath("//tr[@class='AlternatingRowStyle']"):
            data_col = row.find_elements_by_xpath(".//td")
            data_col = [i.text for i in data_col]
            data = {}
            for i in range(len(data_col)-1):
                data[list_of_keywords[i+1]] = data_col[i+1] 
            data['poliza'] = poliza['poliza']
            self.cobros.append(data)
            try:
                if data['Referencia'] is not u'':
                    self.cobros.append(data)
            except KeyError:
                continue

        
        # cerrar el tab y volver al anterior
        self.find(By.TAG_NAME, 'html').send_keys(Keys.CONTROL + 'w')
        self.main_frame()
        self.frame("MC")

    def get_table_poliza(self, tr):
        data = {}
        elems = tr.find_elements_by_xpath(".//td")
        for i in range(len(elems)/2):
            data[elems[2*i].text] = elems[2*i + 1].text
        return data

    def get_riesgo_table(self):
        data = {}
        tabla = self.find(By.XPATH, "//div[@class='Agrupacion']")
        elems = tabla.find_elements_by_xpath(".//td")
        for i in range(len(elems)/2):
            data[elems[2*i].text] = elems[2*i + 1].text
            tabla = self.find(By.XPATH, "//div[@class='Agrupacion']")
            elems = tabla.find_elements_by_xpath(".//td")

        tabla = self.find(By.XPATH, "//div[@class='MAD Mt10']")
        elems = tabla.find_elements_by_xpath(".//td")

        for i in range(len(elems)/2):
            data[elems[2*i].text] = elems[2*i + 1].text
            tabla = self.find(By.XPATH, "//div[@class='MAD Mt10']")
            elems = tabla.find_elements_by_xpath(".//td")
        return data