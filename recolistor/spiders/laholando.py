# -*- coding: utf-8 -*-
import scrapy
import json
import requests
import codecs


from recolistor.spiders.compania import *


STATE_ACTION = ['inicializando', 'obteniendo_urls', 'obteniendo_poliza', 'obteniendo_riesgos', 'escribiendo_a_csv']

def format_holando_fecha(fecha):
    date = (str(fecha).split(" ")[0].split("-"))
    date.reverse()
    return "/".join(date)


class LaHolandoSpider(CompaniaSpider):
    name = 'laholando'
    allowed_host = "http://ru5.com.ar/"
    login_url = "http://holandonet.laholando.com/holandonet/index.html"
    start_urls = [login_url]

    def download(self, response):
        self.get_polizas_and_cobranzas()
        self.zip()

    def login(self):
        self.get(self.login_url)
        self.find(By.ID, "p_usuario", wait=True).send_keys(self.user)
        # password
        self.find(By.ID, "p_password", wait=True).send_keys(self.password)
        # login
        elem = self.find(By.ID, "btn_conectar")
        self.click(elem)

    def is_logged(self):
        text_ok = "Mi Cuenta"
        self.wait(By.XPATH, self.xpath_contains(text_ok))
        return self.find(By.XPATH, self.xpath_contains(text_ok)) is not None

    def get_polizas_and_cobranzas(self):
        try:
            self.wait(by=By.XPATH, value="//div[@class='widget-big-int plugin-clock']", time=60)
        except:
            self.refresh()
            self.wait(by=By.XPATH, value="//div[@class='widget-big-int plugin-clock']", time=60)

        elem = self.find_all(By.XPATH, "//li[@class='xn-openable level-1']")[0]
        self.click(elem)

        elem = self.find_all(By.XPATH, "//li[@class='xn-openable level-2']")[1]
        self.click(elem)

        elem = self.find_all(By.XPATH, "//li[@hnt_label='Nuevo Sistema de Rúbrica Dig.']", wait=True)[0]
        self.click(elem)
        time.sleep(5)

        self.frame("myIFrame")
        self.find(By.XPATH, "//input[@name='p_periodo_desde']", wait=True).send_keys(format_holando_fecha(self.date_from))
        self.find(By.XPATH, "//input[@name='p_periodo_hasta']", wait=True).send_keys(format_holando_fecha(self.date_to))
        elem = self.find(By.XPATH, "//input[@class='btn btn-primary btn-sm']", wait=True)
        self.click(elem)

        link_cuotas = self.find_all(By.XPATH, "//td/a")[0].get_attribute("href")
        link_coberturas = self.find_all(By.XPATH, "//td/a")[1].get_attribute("href")

        self.find(By.TAG_NAME, 'body').send_keys(Keys.CONTROL + 't')
        operaciones_window = self.get_current_window_handle()
        self.switch_to_window(operaciones_window)
        self.get(link_cuotas)
        text_cuotas = self.find(By.TAG_NAME, "body").text
        time.sleep(3)
        self.find(By.TAG_NAME, 'html').send_keys(Keys.CONTROL + 'w')
        self.main_frame()

        self.find(By.TAG_NAME, 'body').send_keys(Keys.CONTROL + 't')
        operaciones_window = self.get_current_window_handle()
        self.switch_to_window(operaciones_window)
        self.get(link_coberturas)
        text_coberturas = self.find(By.TAG_NAME, "body").text
        self.find(By.TAG_NAME, 'html').send_keys(Keys.CONTROL + 'w')
        self.main_frame()
