# -*- coding: utf-8 -*-
# pylint: disable-all
import scrapy
from scrapy import signals

import time
import json
import datetime
import os
import zipfile
import logging
from scrapy.utils.log import configure_logging
import codecs

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import TimeoutException, UnexpectedAlertPresentException

from pyvirtualdisplay import Display

from celery.utils.log import get_task_logger


class ElementNotFound(Exception):
    pass


class LoginFailed(Exception):
    pass


class PopupBlocked(Exception):
    pass

class CustomAdapter(logging.LoggerAdapter):
    """
    This example adapter expects the passed in dict-like object to have a
    'connid' key, whose value in brackets is prepended to the log message.
    """
    def process(self, msg, kwargs):
        try:
            return 'Compania [%s], codigo: [%s], recurso: [%s] %s' % (self.extra['compania'], self.extra['codigo'], self.extra['value'], msg), kwargs
        except:
            if self.extra and 'compania' in self.extra:
                return 'Compania [%s], %s' % (self.extra['compania'], msg), kwargs
            return 'Compania [%s], %s' % ('', msg), kwargs

def wait_for(condition_function):
    start_time = time.time()
    while time.time() < start_time + 30:
        if condition_function():
            return True
        else:
            time.sleep(0.1)
    raise Exception(
        'Timeout waiting for {}'.format(condition_function.__name__)
    )


class wait_for_page_load(object):

    def __init__(self, browser):
        self.browser = browser

    def __enter__(self):
        try:
            self.old_page = self.browser.find_element_by_tag_name('html')
        except:
            self.old_page = None

    def page_has_loaded(self):
        try:
            new_page = self.browser.find_element_by_tag_name('html')
        except:
            return False
        # por alguna razon no anda el if cortocircuitado
        if new_page != None:
            if self.old_page != None:
                return new_page.id != self.old_page.id
            else:
                return True
        else:
            return False

    def __exit__(self, *_):
        wait_for(self.page_has_loaded)


class Navigator():

    FILE_TYPES = [
        "application/octet-stream", "application/vnd.ms-excel", "application/xhtml+xml", "application/xml","appl/text","application/text","text/html"
    ]

    def __init__(self):

        self._profile = webdriver.FirefoxProfile()

    def download_directly_to(self, folder):
        self._profile.set_preference("browser.download.folderList", 2)
        self._profile.set_preference("browser.download.manager.showWhenStarting", False)
        self._profile.set_preference("browser.download.dir", folder)
        self._profile.set_preference("browser.helperApps.neverAsk.saveToDisk", ",".join(self.FILE_TYPES))
        self._profile.set_preference("browser.helperApps.alwaysAsk.force", False)

    def get_driver(self):
        caps = webdriver.DesiredCapabilities.FIREFOX
        caps["marionette"] = True
        #caps['binary'] = '/usr/bin/geckodriver'

        return webdriver.Firefox(
            capabilities=caps,
            firefox_profile=self._profile,
            log_path="/home/brokerdigital/projects/librosobligatorios/logs/webdriver.log")


class CompaniaSpider(scrapy.Spider):

    def __init__(self, id_download=None, user=None, password=None, password2=None, date_from=None, date_to=None, show=False, log_id=None, *args, **kwargs):
        super(CompaniaSpider, self).__init__(*args, **kwargs)

        self.user = user
        self.password = password
        self.password2 = password2
        if date_from:
            self.date_from = datetime.datetime.strptime(date_from, "%Y-%m-%d")
        else:
            self.date_from = ""
        if date_to:
            self.date_to = datetime.datetime.strptime(date_to, "%Y-%m-%d")
        else:
            self.date_to = ""
        # dispatcher.connect(self.spider_closed, signals.spider_closed)

        self.show = show == "True"
        self.debug = False

        if self.just_login:
            self.tmp_folder = "/tmp/claves/{}/".format(id_download)
        else:
            self.tmp_folder = "/tmp/{}/".format(id_download)

        os.makedirs(self.tmp_folder, exist_ok=True)
        self.popups = 0
        self.driver = None
        self.error = 0
        #Guardar el estado
        self.state = {}
        #
        self.polizas_t = []
        self.riesgos_t = []
        self.conf_navigator()

    def conf_navigator(self):
        nav = Navigator()
        nav.download_directly_to(self.tmp_folder)
        self.set_navigator(nav)

    def zip(self):
        zf = zipfile.ZipFile(self.tmp_folder + 'zipfile.zip', mode='w')
        for f in os.listdir(self.tmp_folder):
            if f != "zipfile.zip":
                zf.write(self.tmp_folder + f)
        zf.close()

    def xpath_contains(self, value):
        return "//*[contains(text(), '{}')]".format(value)

    def find(self, by, value, wait=False, time=3, ignore_error=False, tries=1):
        elem = None
        for t in range(tries):
            try:
                if wait:
                    self.wait(by, value, ignore_error=ignore_error)
                elem = self.driver.find_element(by, value)
                break
            except:
                pass
        if t >= tries and not ignore_error:
            if self.debug:
                pass
                #import ipdb;ipdb.set_trace()
            raise ElementNotFound
        return elem

    def find_all(self, by, value, wait=False, time=3, ignore_error=False, tries=1):
        elems = None
        for t in range(tries):
            try:
                if wait:
                    self.wait(by, value, ignore_error=ignore_error)
                elems = self.driver.find_elements(by, value)
                break
            except:
                pass
        if t >= tries and not ignore_error:
            if self.debug:
                pass
                #import ipdb;ipdb.set_trace()
            raise ElementNotFound
        return elems

    def main_frame(self):
        self.driver.switch_to_default_content()

    def frame(self, name, time=3):
        try:
            WebDriverWait(self.driver, time).until(
                EC.frame_to_be_available_and_switch_to_it(name)
            )
        except:
            if name != "":
                self.logger.error("Error al tratar de cambiar al frame %s" % name)

    def accept_alert(self):
        try:
            alert = self.driver.switch_to.alert
            if self.debug:
                print(alert.text)
            alert.dismiss()
        except:
            pass

    def popup_click(self, elem):
        tries = 0
        self.popups += 1
        if self.debug:
            print(self.popups)
        elem.click()
        self.accept_alert()
        while self.driver.window_handles[-1] == self.driver.current_window_handle:
            time.sleep(0.1)
            elem.click()
            self.accept_alert()
            tries += 1
            if tries > 2:
                raise PopupBlocked
        self.driver.switch_to_window(self.driver.window_handles[-1])


    def popup(self, other_windows):
        popup_window = None
        while popup_window == None:
            for handle in self.driver.window_handles:
                if handle not in other_windows:
                    popup_window = handle
                    break

        self.driver.switch_to_window(popup_window)

    def perform(self,  fun, *args ):
        fun( *args)

    def wait(self, by, value, value2=None, time=3, function=EC.presence_of_element_located, ignore_error=False, retry=True):
        #La variable retry la definimos True por defecto. Solo la vamos a usar en caso de que haya un "UnexpectAlertePresentException"
        #En el debugueo la mayoría de esas excepciones se solucionaron haciendo un refresh a la pagina.
        self.state['value'] = value
        extra = self.state

        #get the logger
        logg = get_task_logger(__name__)

        #configure out of the logger
        logger = CustomAdapter(logg, extra)

        #
        if not value2:
            try:
                self.perform(WebDriverWait(self.driver, time).until, function((by, value)))
            except TimeoutException:
                if not ignore_error:
                    logger.warning('Timeout - Recurso no Encontrado')
            except UnexpectedAlertPresentException:
                if not ignore_error:
                    if retry:
                        logger.warning(" Unexpected Alert Present Exception, Intento obtenerlo una vez más ")
                        self.driver.refresh()
                        self.wait(by, value, value2, time, function, ignore_error=False, retry=false)
                    else:
                        logger.warning(" Unexpected Alert Present Exception - Recurso no encontrado ")

        else:
            try:
                self.perform(WebDriverWait(self.driver, time).until, function((by, value),value2))
            except TimeoutException:
                if not ignore_error:
                    self.state['value'] = value
                    logger.error('Timeout - Recurso no Encontrado ')
            except UnexpectedAlertPresentException:
                if not ignore_error:
                    if retry:
                        logger.warning(" Unexpected Alert Present Exception, Intento obtenerlo una vez más ")
                        self.driver.refresh()
                        self.perform(WebDriverWait(self.driver, time).until, function((by, value),value2))
                    else:
                        logger.warning(" Unexpected Alert Present Exception - Recurso no encontrado ")

    def switch_to_window(self, window):
        self.driver.switch_to_window(window)

    def get_window_current_handle(self):
        return self.driver.current_window_handle

    def refresh(self):
        self.driver.refresh()

    def close(self):
        if self.display:
            self.display.stop()
        try:
            self.driver.quit()
        except:
            pass

    def spider_closed(self, spider):
        logg = get_task_logger(__name__)
        extra = self.state
        logger = CustomAdapter(logg, extra)
        if 'value' in self.state: del self.state['value']

        if self.state:
            if self.state['termino']:
                logger.warning("Termino Exitosamente La Recolección de Datos")
            else:
                logger.warning("Termino con errores - Ver el Logg")
        #configure out of the logger
        # mark download as terminated
        open(self.tmp_folder + "terminated", 'a').close()

    def iter_months(self):
        for year in range(2005, 2020):
            for month in range(1, 13):
                if year < self.date_from.year or year == self.date_from.year and month < self.date_from.month:
                    continue
                if year > self.date_to.year or year == self.date_to.year and month > self.date_to.month:
                    continue
                yield (month, year)

    def iter_days(self):
        d = self.date_from
        while d <= self.date_to:
            yield d
            d = d + datetime.timedelta(days=1)

    def set_navigator(self, navigator):
        if not self.show:
            self.display = Display(visible=0, size=(800, 600))
            self.display.start()
        else:
            self.display = None

        self.driver = navigator.get_driver()

    def get(self, url):
        self.driver.get(url)

    def save_to_csv(self, file, data):
        import csv
        from sets import Set
        keys = Set([])
        for item in data:
            keys = keys | Set(item.keys())
        keys = list(keys)

        with codecs.open(self.tmp_folder + file, 'wb', 'utf-8') as output_file:
            output_file.write(";".join(keys) + "\n")
            for item in data:
                row = []
                for key in keys:
                    if key in item.keys():
                        row.append(item[key])
                    else:
                        row.append("")

                output_file.write(";".join(row) + "\n")

            #dict_writer = csv.DictWriter(output_file, keys)
            #dict_writer.writeheader()
            #dict_writer.writerows(data)

    def control_click(self, element):
        ActionChains(self.driver) \
            .key_down(Keys.CONTROL) \
            .click(element) \
            .key_up(Keys.CONTROL) \
            .perform()
        self.driver.switch_to_window(self.driver.window_handles[-1])

    def set_input(self, by, where, value):
        # prueba hasta 5 veces
        # si siempre da error, lo propaga
        for i in range(5):
            try:
                input = self.find(by, where)
                input.clear()
                input.send_keys(value)
                break
            except:
                pass

    def click(self, elem):
        self.driver.execute_script("arguments[0].click();", elem)
        # para chrome
        # ActionChains(self.driver).move_to_element_with_offset(elem, 0, 20).click().perform()

    def parse(self, response):
        self.login()
        if self.just_login:
            file = self.tmp_folder + "result.json"
            with open(file, 'w') as output_file:
                data = {'result': self.is_logged()}
                output_file.write(json.dumps(data) + "\n")
        else:
            self.download()
